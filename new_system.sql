--
-- Table structure for table `cobalt_reporter`
--

CREATE TABLE `cobalt_reporter` (
  `module_name` varchar(255) NOT NULL,
  `report_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `show_field` blob NOT NULL,
  `hidden_field` blob NOT NULL,
  `operator` blob NOT NULL,
  `text_field` blob NOT NULL,
  `sum_field` blob NOT NULL,
  `count_field` blob NOT NULL,
  `group_field1` blob NOT NULL,
  `group_field2` blob NOT NULL,
  `group_field3` blob NOT NULL,
  PRIMARY KEY (`module_name`,`report_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `cobalt_sst`
--

CREATE TABLE `cobalt_sst` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `config_file` varchar(255) NOT NULL,
  PRIMARY KEY (`auto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `person_id` int(11) NOT NULL auto_increment,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  PRIMARY KEY  (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`person_id`, `first_name`, `middle_name`, `last_name`, `gender`) VALUES
(1, 'Super User', 'X', 'Root', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `system_log`
--

CREATE TABLE `system_log` (
  `entry_id` bigint(20) NOT NULL auto_increment,
  `ip_address` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  `action` varchar(50000) NOT NULL,
  `module` varchar(255) NOT NULL,
  PRIMARY KEY  (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `system_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `setting` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY  (`setting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`setting`, `value`) VALUES
('Security Level', 'HIGH'),
('Max Attachment Size (MB)', '0'),
('Max Attachment Width', '0'),
('Max Attachment Height', '0');
-- --------------------------------------------------------

--
-- Table structure for table `system_skins`
--

CREATE TABLE `system_skins` (
  `skin_id` int(11) NOT NULL auto_increment,
  `skin_name` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `footer` varchar(255) NOT NULL,
  `master_css` varchar(255) NOT NULL,
  `colors_css` varchar(255) NOT NULL,
  `fonts_css` varchar(255) NOT NULL,
  `override_css` varchar(255) NOT NULL,
  `icon_set` varchar(255) NOT NULL,
  PRIMARY KEY  (`skin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `system_skins`
--

INSERT INTO `system_skins` (`skin_id`, `skin_name`, `header`, `footer`, `master_css`, `colors_css`, `fonts_css`, `override_css`, `icon_set`) VALUES
(1, 'Cobalt Default', 'skins/default_header.php', 'skins/default_footer.php', 'cobalt_master.css', 'cobalt_colors.css', 'cobalt_fonts.css', 'cobalt_override.css','cobalt'),
(2, 'Cobalt Minimal', 'skins/minimal_header.php', 'skins/minimal_footer.php', 'cobalt_minimal.css', 'cobalt_minimal.css', 'cobalt_minimal.css', 'cobalt_minimal.css','cobalt'),
(3, 'After Sunset', 'skins/default_header.php', 'skins/default_footer.php', 'after_sunset_master.css', 'after_sunset_colors.css', 'after_sunset_fonts.css', 'after_sunset_override.css','cobalt'),
(4, 'Hello There', 'skins/default_header.php', 'skins/default_footer.php', 'hello_there_master.css', 'hello_there_colors.css', 'hello_there_fonts.css', 'hello_there_override.css','cobalt'),
(5, 'Gold Titanium', 'skins/default_header.php', 'skins/default_footer.php', 'gold_titanium_master.css', 'gold_titanium_colors.css', 'gold_titanium_fonts.css', 'gold_titanium_override.css','cobalt'),
(6, 'Summer Rain', 'skins/default_header.php', 'skins/default_footer.php', 'summer_rain_master.css', 'summer_rain_colors.css', 'summer_rain_fonts.css', 'summer_rain_override.css','cobalt'),
(7, 'Salmon Impression', 'skins/default_header.php', 'skins/default_footer.php', 'salmon_impression_master.css', 'salmon_impression_colors.css', 'salmon_impression_fonts.css', 'salmon_impression_override.css','cobalt'),
(8, 'Royal Amethyst', 'skins/default_header.php', 'skins/default_footer.php', 'royal_amethyst_master.css', 'royal_amethyst_colors.css', 'royal_amethyst_fonts.css', 'royal_amethyst_override.css','cobalt'),
(9, 'Red Decadence', 'skins/default_header.php', 'skins/default_footer.php', 'red_decadence_master.css', 'red_decadence_colors.css', 'red_decadence_fonts.css', 'red_decadence_override.css','cobalt'),
(10, 'Modern Eden', 'skins/default_header.php', 'skins/default_footer.php', 'modern_eden_master.css', 'modern_eden_colors.css', 'modern_eden_fonts.css', 'modern_eden_override.css','cobalt'),
(11, 'Warm Teal', 'skins/default_header.php', 'skins/default_footer.php', 'warm_teal_master.css', 'warm_teal_colors.css', 'warm_teal_fonts.css', 'warm_teal_override.css', 'cobalt'),
(12, 'Purple Rain', 'skins/default_header.php', 'skins/default_footer.php', 'purple_rain_master.css', 'purple_rain_colors.css', 'purple_rain_fonts.css', 'purple_rain_override.css', 'cobalt');


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `iteration` int(11) NOT NULL,
  `method` varchar(255) NOT NULL,
  `person_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `skin_id` int(11) NOT NULL,
  `user_level` tinyint(4) NOT NULL,
  PRIMARY KEY  (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_links`
--

CREATE TABLE `user_links` (
  `link_id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `target` varchar(255) NOT NULL,
  `descriptive_title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `passport_group_id` int(11) NOT NULL,
  `show_in_tasklist` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_links`
--

INSERT INTO `user_links` (`link_id`, `name`, `target`, `descriptive_title`, `description`, `passport_group_id`, `show_in_tasklist`, `status`, `icon`,`priority`) VALUES
(1, 'Module Control', 'sysadmin/module_control.php', 'Module Control', 'Enable or disable system modules', 2, 'Yes', 'On', 'modulecontrol.png', ''),
(2, 'Set User Passports', 'sysadmin/set_user_passports.php', 'Set User Passports', 'Change the passport settings of system users', 2, 'Yes', 'On', 'passport.png', ''),
(3, 'Security Monitor', 'sysadmin/security_monitor.php', 'Security Monitor', 'Examine the system log', 2, 'Yes', 'On', 'security3.png', ''),
(4, 'Add person', 'sysadmin/add_person.php', 'Add Person', '', 2, 'No', 'On', 'form.png', ''),
(5, 'Edit person', 'sysadmin/edit_person.php', 'Edit Person', '', 2, 'No', 'On', 'form.png', ''),
(6, 'View person', 'sysadmin/listview_person.php', 'Person', '', 2, 'Yes', 'On', 'persons.png', ''),
(7, 'Delete person', 'sysadmin/delete_person.php', 'Delete Person', '', 2, 'No', 'On', 'form.png', ''),
(8, 'Add user', 'sysadmin/add_user.php', 'Add User', '', 2, 'No', 'On', 'form.png', ''),
(9, 'Edit user', 'sysadmin/edit_user.php', 'Edit User', '', 2, 'No', 'On', 'form.png', ''),
(10, 'View user', 'sysadmin/listview_user.php', 'User', '', 2, 'Yes', 'On', 'card.png', ''),
(11, 'Delete user', 'sysadmin/delete_user.php', 'Delete User', '', 2, 'No', 'On', 'form.png', ''),
(12, 'Add user role', 'sysadmin/add_user_role.php', 'Add User Role', '', 2, 'No', 'On', 'form.png', ''),
(13, 'Edit user role', 'sysadmin/edit_user_role.php', 'Edit User Role', '', 2, 'No', 'On', 'form.png', ''),
(14, 'View user role', 'sysadmin/listview_user_role.php', 'User Roles', '', 2, 'Yes', 'On', 'roles.png', ''),
(15, 'Delete user role', 'sysadmin/delete_user_role.php', 'Delete User Role', '', 2, 'No', 'On', 'form.png', ''),
(16, 'Add system settings', 'sysadmin/add_system_settings.php', 'Add System Settings', '', 2, 'No', 'On', 'form.png', ''),
(17, 'Edit system settings', 'sysadmin/edit_system_settings.php', 'Edit System Settings', '', 2, 'No', 'On', 'form.png', ''),
(18, 'View system settings', 'sysadmin/listview_system_settings.php', 'System Settings', '', 2, 'Yes', 'On', 'system_settings.png', ''),
(19, 'Delete system settings', 'sysadmin/delete_system_settings.php', 'Delete System Settings', '', 2, 'No', 'On', 'form.png', ''),
(20, 'Add user links', 'sysadmin/add_user_links.php', 'Add User Links', '', 2, 'No', 'On', 'form.png', ''),
(21, 'Edit user links', 'sysadmin/edit_user_links.php', 'Edit User Links', '', 2, 'No', 'On', 'form.png', ''),
(22, 'View user links', 'sysadmin/listview_user_links.php', 'User Links', '', 2, 'Yes', 'On', 'links.png', ''),
(23, 'Delete user links', 'sysadmin/delete_user_links.php', 'Delete User Links', '', 2, 'No', 'On', 'form.png', ''),
(24, 'Add user passport groups', 'sysadmin/add_user_passport_groups.php', 'Add User Passport Groups', '', 2, 'No', 'On', 'form.png', ''),
(25, 'Edit user passport groups', 'sysadmin/edit_user_passport_groups.php', 'Edit User Passport Groups', '', 2, 'No', 'On', 'form.png', ''),
(26, 'View user passport groups', 'sysadmin/listview_user_passport_groups.php', 'User Passport Groups', '', 2, 'Yes', 'On', 'passportgroup.png', ''),
(27, 'Delete user passport groups', 'sysadmin/delete_user_passport_groups.php', 'Delete User Passport Groups', '', 2, 'No', 'On', 'form.png', ''),
(28, 'Add system skins', 'sysadmin/add_system_skins.php', 'Add System Skins', '', 2, 'No', 'On', 'form.png', ''),
(29, 'Edit system skins', 'sysadmin/edit_system_skins.php', 'Edit System Skins', '', 2, 'No', 'On', 'form.png', ''),
(30, 'View system skins', 'sysadmin/listview_system_skins.php', 'System Skins', '', 2, 'Yes', 'On', 'system_skins.png', ''),
(31, 'Delete system skins', 'sysadmin/delete_system_skins.php', 'Delete System Skins', '', 2, 'No', 'On', 'form.png', ''),
(32, 'Reset Password', 'sysadmin/reset_password.php', 'Reset Password', '', 2, 'Yes', 'On', 'lock_big.png', ''),
(33, 'Add cobalt sst', 'sst/add_cobalt_sst.php', 'Add Cobalt SST', '', 2, 'No', 'On', 'form3.png', 0),
(34, 'Edit cobalt sst', 'sst/edit_cobalt_sst.php', 'Edit Cobalt SST', '', 2, 'No', 'On', 'form3.png', 0),
(35, 'View cobalt sst', 'sst/listview_cobalt_sst.php', 'Cobalt SST', '', 2, 'Yes', 'On', 'form3.png', 0),
(36, 'Delete cobalt sst', 'sst/delete_cobalt_sst.php', 'Delete Cobalt SST', '', 2, 'No', 'On', 'form3.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_passport`
--

CREATE TABLE `user_passport` (
  `username` varchar(255) NOT NULL,
  `link_id` int(11) NOT NULL,
  PRIMARY KEY  (`username`,`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_passport`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_passport_groups`
--

CREATE TABLE `user_passport_groups` (
  `passport_group_id` int(11) NOT NULL auto_increment,
  `passport_group` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL,
  PRIMARY KEY  (`passport_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_passport_groups`
--

INSERT INTO `user_passport_groups` (`passport_group_id`, `passport_group`,`icon`) VALUES
(1, 'Default','blue_folder3.png'),
(2, 'Admin','preferences-system.png');


-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

 CREATE TABLE `user_role` (
`role_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`role` VARCHAR( 255 ) NOT NULL ,
`description` VARCHAR( 255 ) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`role_id`, `role`, `description`) VALUES
(1, 'Super Admin', 'Super admin role with 100% system privileges'),
(2, 'System Admin', 'System admin role with all sysadmin permissions');


-- --------------------------------------------------------

--
-- Table structure for table `user_role_links`
--

CREATE TABLE `user_role_links` (
`role_id` INT NOT NULL ,
`link_id` INT NOT NULL ,
PRIMARY KEY ( `role_id` , `link_id` )
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role_links`
--

INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36);
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add action notice', 'modules/action_notice/add_action_notice.php', 'Add Action Notice','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit action notice', 'modules/action_notice/edit_action_notice.php', 'Edit Action Notice','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View action notice', 'modules/action_notice/listview_action_notice.php', 'Action Notice','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete action notice', 'modules/action_notice/delete_action_notice.php', 'Delete Action Notice','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add applicant', 'modules/applicant/applicant/add_applicant.php', 'Add Applicant','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit applicant', 'modules/applicant/applicant/edit_applicant.php', 'Edit Applicant','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View applicant', 'modules/applicant/applicant/listview_applicant.php', 'Applicant','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete applicant', 'modules/applicant/applicant/delete_applicant.php', 'Delete Applicant','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add applicant exam', 'modules/applicant/applicant_exam/add_applicant_exam.php', 'Add Applicant Exam','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit applicant exam', 'modules/applicant/applicant_exam/edit_applicant_exam.php', 'Edit Applicant Exam','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View applicant exam', 'modules/applicant/applicant_exam/listview_applicant_exam.php', 'Applicant Exam','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete applicant exam', 'modules/applicant/applicant_exam/delete_applicant_exam.php', 'Delete Applicant Exam','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add applicant family members', 'modules/applicant/applicant_family_members/add_applicant_family_members.php', 'Add Applicant Family Members','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit applicant family members', 'modules/applicant/applicant_family_members/edit_applicant_family_members.php', 'Edit Applicant Family Members','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View applicant family members', 'modules/applicant/applicant_family_members/listview_applicant_family_members.php', 'Applicant Family Members','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete applicant family members', 'modules/applicant/applicant_family_members/delete_applicant_family_members.php', 'Delete Applicant Family Members','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add applicant interview', 'modules/applicant/applicant_interview/add_applicant_interview.php', 'Add Applicant Interview','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit applicant interview', 'modules/applicant/applicant_interview/edit_applicant_interview.php', 'Edit Applicant Interview','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View applicant interview', 'modules/applicant/applicant_interview/listview_applicant_interview.php', 'Applicant Interview','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete applicant interview', 'modules/applicant/applicant_interview/delete_applicant_interview.php', 'Delete Applicant Interview','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add applicant languages proficiency', 'modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php', 'Add Applicant Languages Proficiency','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit applicant languages proficiency', 'modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php', 'Edit Applicant Languages Proficiency','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View applicant languages proficiency', 'modules/applicant/applicant_languages_proficiency/listview_applicant_languages_proficiency.php', 'Applicant Languages Proficiency','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete applicant languages proficiency', 'modules/applicant/applicant_languages_proficiency/delete_applicant_languages_proficiency.php', 'Delete Applicant Languages Proficiency','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add applicant license', 'modules/applicant/applicant_license/add_applicant_license.php', 'Add Applicant License','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit applicant license', 'modules/applicant/applicant_license/edit_applicant_license.php', 'Edit Applicant License','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View applicant license', 'modules/applicant/applicant_license/listview_applicant_license.php', 'Applicant License','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete applicant license', 'modules/applicant/applicant_license/delete_applicant_license.php', 'Delete Applicant License','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add applicant previous employers', 'modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php', 'Add Applicant Previous Employers','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit applicant previous employers', 'modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php', 'Edit Applicant Previous Employers','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View applicant previous employers', 'modules/applicant/applicant_previous_employers/listview_applicant_previous_employers.php', 'Applicant Previous Employers','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete applicant previous employers', 'modules/applicant/applicant_previous_employers/delete_applicant_previous_employers.php', 'Delete Applicant Previous Employers','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add applicant reference', 'modules/applicant/applicant_reference/add_applicant_reference.php', 'Add Applicant Reference','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit applicant reference', 'modules/applicant/applicant_reference/edit_applicant_reference.php', 'Edit Applicant Reference','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View applicant reference', 'modules/applicant/applicant_reference/listview_applicant_reference.php', 'Applicant Reference','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete applicant reference', 'modules/applicant/applicant_reference/delete_applicant_reference.php', 'Delete Applicant Reference','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add applicant school attended', 'modules/applicant/applicant_school_attended/add_applicant_school_attended.php', 'Add Applicant School Attended','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit applicant school attended', 'modules/applicant/applicant_school_attended/edit_applicant_school_attended.php', 'Edit Applicant School Attended','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View applicant school attended', 'modules/applicant/applicant_school_attended/listview_applicant_school_attended.php', 'Applicant School Attended','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete applicant school attended', 'modules/applicant/applicant_school_attended/delete_applicant_school_attended.php', 'Delete Applicant School Attended','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add applicant skills', 'modules/applicant/applicant_skills/add_applicant_skills.php', 'Add Applicant Skills','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit applicant skills', 'modules/applicant/applicant_skills/edit_applicant_skills.php', 'Edit Applicant Skills','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View applicant skills', 'modules/applicant/applicant_skills/listview_applicant_skills.php', 'Applicant Skills','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete applicant skills', 'modules/applicant/applicant_skills/delete_applicant_skills.php', 'Delete Applicant Skills','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add branch', 'modules/organization/branch/add_branch.php', 'Add Branch','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit branch', 'modules/organization/branch/edit_branch.php', 'Edit Branch','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View branch', 'modules/organization/branch/listview_branch.php', 'Branch','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete branch', 'modules/organization/branch/delete_branch.php', 'Delete Branch','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add branch type', 'modules/organization/branch_type/add_branch_type.php', 'Add Branch Type','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit branch type', 'modules/organization/branch_type/edit_branch_type.php', 'Edit Branch Type','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View branch type', 'modules/organization/branch_type/listview_branch_type.php', 'Branch Type','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete branch type', 'modules/organization/branch_type/delete_branch_type.php', 'Delete Branch Type','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add building', 'modules/organization/building/add_building.php', 'Add Building','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit building', 'modules/organization/building/edit_building.php', 'Edit Building','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View building', 'modules/organization/building/listview_building.php', 'Building','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete building', 'modules/organization/building/delete_building.php', 'Delete Building','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add calendar event', 'modules/calendar_event/add_calendar_event.php', 'Add Calendar Event','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit calendar event', 'modules/calendar_event/edit_calendar_event.php', 'Edit Calendar Event','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View calendar event', 'modules/calendar_event/listview_calendar_event.php', 'Calendar Event','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete calendar event', 'modules/calendar_event/delete_calendar_event.php', 'Delete Calendar Event','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add calendar event type', 'modules/calendar_event_type/add_calendar_event_type.php', 'Add Calendar Event Type','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit calendar event type', 'modules/calendar_event_type/edit_calendar_event_type.php', 'Edit Calendar Event Type','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View calendar event type', 'modules/calendar_event_type/listview_calendar_event_type.php', 'Calendar Event Type','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete calendar event type', 'modules/calendar_event_type/delete_calendar_event_type.php', 'Delete Calendar Event Type','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add clearance', 'modules/clearance/clearance/add_clearance.php', 'Add Clearance','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit clearance', 'modules/clearance/clearance/edit_clearance.php', 'Edit Clearance','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View clearance', 'modules/clearance/clearance/listview_clearance.php', 'Clearance','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete clearance', 'modules/clearance/clearance/delete_clearance.php', 'Delete Clearance','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add clearance approval', 'modules/clearance/clearance_approval/add_clearance_approval.php', 'Add Clearance Approval','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit clearance approval', 'modules/clearance/clearance_approval/edit_clearance_approval.php', 'Edit Clearance Approval','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View clearance approval', 'modules/clearance/clearance_approval/listview_clearance_approval.php', 'Clearance Approval','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete clearance approval', 'modules/clearance/clearance_approval/delete_clearance_approval.php', 'Delete Clearance Approval','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add clearance approver', 'modules/clearance/clearance_approver/add_clearance_approver.php', 'Add Clearance Approver','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit clearance approver', 'modules/clearance/clearance_approver/edit_clearance_approver.php', 'Edit Clearance Approver','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View clearance approver', 'modules/clearance/clearance_approver/listview_clearance_approver.php', 'Clearance Approver','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete clearance approver', 'modules/clearance/clearance_approver/delete_clearance_approver.php', 'Delete Clearance Approver','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add company', 'modules/organization/company/add_company.php', 'Add Company','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit company', 'modules/organization/company/edit_company.php', 'Edit Company','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View company', 'modules/organization/company/listview_company.php', 'Company','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete company', 'modules/organization/company/delete_company.php', 'Delete Company','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add company policy', 'modules/organization/company_policy/add_company_policy.php', 'Add Company Policy','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit company policy', 'modules/organization/company_policy/edit_company_policy.php', 'Edit Company Policy','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View company policy', 'modules/organization/company_policy/listview_company_policy.php', 'Company Policy','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete company policy', 'modules/organization/company_policy/delete_company_policy.php', 'Delete Company Policy','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add company violation', 'modules/organization/company_violation/add_company_violation.php', 'Add Company Violation','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit company violation', 'modules/organization/company_violation/edit_company_violation.php', 'Edit Company Violation','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View company violation', 'modules/organization/company_violation/listview_company_violation.php', 'Company Violation','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete company violation', 'modules/organization/company_violation/delete_company_violation.php', 'Delete Company Violation','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add department', 'modules/organization/department/add_department.php', 'Add Department','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit department', 'modules/organization/department/edit_department.php', 'Edit Department','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View department', 'modules/organization/department/listview_department.php', 'Department','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete department', 'modules/organization/department/delete_department.php', 'Delete Department','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add employee', 'modules/employee/employee/add_employee.php', 'Add Employee','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit employee', 'modules/employee/employee/edit_employee.php', 'Edit Employee','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View employee', 'modules/employee/employee/listview_employee.php', 'Employee','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete employee', 'modules/employee/employee/delete_employee.php', 'Delete Employee','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add employee education', 'modules/employee/employee_education/add_employee_education.php', 'Add Employee Education','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit employee education', 'modules/employee/employee_education/edit_employee_education.php', 'Edit Employee Education','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View employee education', 'modules/employee/employee_education/listview_employee_education.php', 'Employee Education','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete employee education', 'modules/employee/employee_education/delete_employee_education.php', 'Delete Employee Education','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add employee evaluation', 'modules/employee/employee_evaluation/add_employee_evaluation.php', 'Add Employee Evaluation','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit employee evaluation', 'modules/employee/employee_evaluation/edit_employee_evaluation.php', 'Edit Employee Evaluation','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View employee evaluation', 'modules/employee/employee_evaluation/listview_employee_evaluation.php', 'Employee Evaluation','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete employee evaluation', 'modules/employee/employee_evaluation/delete_employee_evaluation.php', 'Delete Employee Evaluation','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add employee evaluation answer', 'modules/employee/employee_evaluation_answer/add_employee_evaluation_answer.php', 'Add Employee Evaluation Answer','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit employee evaluation answer', 'modules/employee/employee_evaluation_answer/edit_employee_evaluation_answer.php', 'Edit Employee Evaluation Answer','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View employee evaluation answer', 'modules/employee/employee_evaluation_answer/listview_employee_evaluation_answer.php', 'Employee Evaluation Answer','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete employee evaluation answer', 'modules/employee/employee_evaluation_answer/delete_employee_evaluation_answer.php', 'Delete Employee Evaluation Answer','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add employee evaluation template', 'modules/employee/employee_evaluation_template/add_employee_evaluation_template.php', 'Add Employee Evaluation Template','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit employee evaluation template', 'modules/employee/employee_evaluation_template/edit_employee_evaluation_template.php', 'Edit Employee Evaluation Template','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View employee evaluation template', 'modules/employee/employee_evaluation_template/listview_employee_evaluation_template.php', 'Employee Evaluation Template','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete employee evaluation template', 'modules/employee/employee_evaluation_template/delete_employee_evaluation_template.php', 'Delete Employee Evaluation Template','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add employee evaluation template criteria', 'modules/employee/employee_evaluation_template_criteria/add_employee_evaluation_template_criteria.php', 'Add Employee Evaluation Template Criteria','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit employee evaluation template criteria', 'modules/employee/employee_evaluation_template_criteria/edit_employee_evaluation_template_criteria.php', 'Edit Employee Evaluation Template Criteria','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View employee evaluation template criteria', 'modules/employee/employee_evaluation_template_criteria/listview_employee_evaluation_template_criteria.php', 'Employee Evaluation Template Criteria','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete employee evaluation template criteria', 'modules/employee/employee_evaluation_template_criteria/delete_employee_evaluation_template_criteria.php', 'Delete Employee Evaluation Template Criteria','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add floor', 'modules/organization/floor/add_floor.php', 'Add Floor','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit floor', 'modules/organization/floor/edit_floor.php', 'Edit Floor','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View floor', 'modules/organization/floor/listview_floor.php', 'Floor','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete floor', 'modules/organization/floor/delete_floor.php', 'Delete Floor','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add hr program', 'modules/hr_program/hr_program/add_hr_program.php', 'Add Hr Program','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit hr program', 'modules/hr_program/hr_program/edit_hr_program.php', 'Edit Hr Program','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View hr program', 'modules/hr_program/hr_program/listview_hr_program.php', 'Hr Program','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete hr program', 'modules/hr_program/hr_program/delete_hr_program.php', 'Delete Hr Program','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add hr program attendees', 'modules/hr_program/hr_program_attendees/add_hr_program_attendees.php', 'Add Hr Program Attendees','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit hr program attendees', 'modules/hr_program/hr_program_attendees/edit_hr_program_attendees.php', 'Edit Hr Program Attendees','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View hr program attendees', 'modules/hr_program/hr_program_attendees/listview_hr_program_attendees.php', 'Hr Program Attendees','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete hr program attendees', 'modules/hr_program/hr_program_attendees/delete_hr_program_attendees.php', 'Delete Hr Program Attendees','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add performance appraisal', 'modules/performance_appraisal/add_performance_appraisal.php', 'Add Performance Appraisal','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit performance appraisal', 'modules/performance_appraisal/edit_performance_appraisal.php', 'Edit Performance Appraisal','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View performance appraisal', 'modules/performance_appraisal/listview_performance_appraisal.php', 'Performance Appraisal','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete performance appraisal', 'modules/performance_appraisal/delete_performance_appraisal.php', 'Delete Performance Appraisal','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add performance appraisal goal', 'modules/performance_appraisal_goal/add_performance_appraisal_goal.php', 'Add Performance Appraisal Goal','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit performance appraisal goal', 'modules/performance_appraisal_goal/edit_performance_appraisal_goal.php', 'Edit Performance Appraisal Goal','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View performance appraisal goal', 'modules/performance_appraisal_goal/listview_performance_appraisal_goal.php', 'Performance Appraisal Goal','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete performance appraisal goal', 'modules/performance_appraisal_goal/delete_performance_appraisal_goal.php', 'Delete Performance Appraisal Goal','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add personnel requisition', 'modules/personnel_requisition/add_personnel_requisition.php', 'Add Personnel Requisition','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit personnel requisition', 'modules/personnel_requisition/edit_personnel_requisition.php', 'Edit Personnel Requisition','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View personnel requisition', 'modules/personnel_requisition/listview_personnel_requisition.php', 'Personnel Requisition','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete personnel requisition', 'modules/personnel_requisition/delete_personnel_requisition.php', 'Delete Personnel Requisition','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add plantilla', 'modules/plantilla/add_plantilla.php', 'Add Plantilla','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit plantilla', 'modules/plantilla/edit_plantilla.php', 'Edit Plantilla','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View plantilla', 'modules/plantilla/listview_plantilla.php', 'Plantilla','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete plantilla', 'modules/plantilla/delete_plantilla.php', 'Delete Plantilla','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add resignation', 'modules/resignation/add_resignation.php', 'Add Resignation','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit resignation', 'modules/resignation/edit_resignation.php', 'Edit Resignation','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View resignation', 'modules/resignation/listview_resignation.php', 'Resignation','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete resignation', 'modules/resignation/delete_resignation.php', 'Delete Resignation','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add school', 'modules/organization/school/add_school.php', 'Add School','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit school', 'modules/organization/school/edit_school.php', 'Edit School','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View school', 'modules/organization/school/listview_school.php', 'School','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete school', 'modules/organization/school/delete_school.php', 'Delete School','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add site', 'modules/organization/site/add_site.php', 'Add Site','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit site', 'modules/organization/site/edit_site.php', 'Edit Site','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View site', 'modules/organization/site/listview_site.php', 'Site','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete site', 'modules/organization/site/delete_site.php', 'Delete Site','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Add training', 'modules/training/add_training.php', 'Add Training','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Edit training', 'modules/training/edit_training.php', 'Edit Training','','1','No','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'View training', 'modules/training/listview_training.php', 'Training','','1','Yes','On','form3.png');
INSERT INTO `user_links`(link_id, name, target, descriptive_title, description, passport_group_id, show_in_tasklist, `status`, icon) VALUES(null,'Delete training', 'modules/training/delete_training.php', 'Delete Training','','1','No','On','form3.png');

INSERT INTO `user` (`username`, `password`, `salt`, `iteration`, `method`, `person_id`, `role_id`, `skin_id`, `user_level`) VALUES
('root', '$2y$12$u4q6Mwkf5Iw2KtHYRdfKZuXzXibBEbk08oGl1R0z2MQKXN3HxP/6e', 'u4q6Mwkf5Iw2KtHYRdfKZw', '12', 'bcrypt', '1', '1', '1', '5');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '37');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '38');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '39');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '40');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '41');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '42');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '43');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '44');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '45');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '46');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '47');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '48');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '49');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '50');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '51');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '52');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '53');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '54');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '55');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '56');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '57');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '58');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '59');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '60');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '61');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '62');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '63');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '64');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '65');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '66');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '67');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '68');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '69');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '70');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '71');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '72');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '73');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '74');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '75');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '76');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '77');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '78');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '79');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '80');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '81');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '82');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '83');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '84');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '85');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '86');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '87');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '88');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '89');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '90');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '91');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '92');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '93');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '94');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '95');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '96');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '97');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '98');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '99');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '100');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '101');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '102');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '103');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '104');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '105');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '106');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '107');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '108');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '109');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '110');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '111');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '112');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '113');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '114');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '115');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '116');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '117');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '118');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '119');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '120');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '121');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '122');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '123');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '124');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '125');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '126');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '127');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '128');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '129');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '130');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '131');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '132');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '133');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '134');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '135');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '136');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '137');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '138');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '139');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '140');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '141');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '142');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '143');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '144');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '145');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '146');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '147');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '148');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '149');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '150');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '151');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '152');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '153');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '154');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '155');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '156');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '157');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '158');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '159');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '160');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '161');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '162');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '163');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '164');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '165');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '166');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '167');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '168');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '169');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '170');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '171');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '172');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '173');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '174');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '175');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '176');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '177');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '178');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '179');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '180');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '181');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '182');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '183');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '184');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '185');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '186');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '187');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '188');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '189');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '190');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '191');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '192');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '193');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '194');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '195');
INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES ('1', '196');
INSERT INTO `user_passport` SELECT 'root', `link_id` FROM user_role_links WHERE role_id='1'