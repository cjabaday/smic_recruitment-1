<?php
require_once 'path.php';
init_cobalt('ALLOW_ALL',FALSE);

// debug($_SESSION);
$html = new html;
$html->draw_header('Welcome to your Control Center', $message, $message_type, FALSE);

if(ENABLE_SIDEBAR)
{
    echo '
    <script>
    if (top.location == location)
    {
        window.location.replace("start.php");
    }
    </script>
    ';
}

if(DEBUG_MODE)
{
    $html->display_error('System is running in DEBUG MODE. Please contact the system administrator ASAP.');
}


menuGroupWindowFooter();


function menuGroupWindowHeader($group, $icon)
{
    echo '<fieldset class="top">';
    echo "<img src='images/" . $_SESSION['icon_set'] . "/$icon'> $group";
    echo '</fieldset>';
    echo '<fieldset class="middle">';
}

function menuGroupWindowFooter()
{
    echo '</fieldset>';
}
$html->draw_footer();
