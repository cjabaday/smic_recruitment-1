<?php
require 'components/get_listview_referrer.php';

require 'subclasses/hr_program.php';
$dbh_hr_program = new hr_program;
$dbh_hr_program->set_where("hr_program_id = ?");
$dbh_hr_program->stmt_bind_param($hr_program_id);

$data = $dbh_hr_program->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$start_date);
    if(count($data) == 3)
    {
        $start_date_year = $data[0];
        $start_date_month = $data[1];
        $start_date_day = $data[2];
    }
    $data = explode('-',$end_date);
    if(count($data) == 3)
    {
        $end_date_year = $data[0];
        $end_date_month = $data[1];
        $end_date_day = $data[2];
    }
}

