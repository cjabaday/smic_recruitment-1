<?php
require 'components/get_listview_referrer.php';

require 'subclasses/hr_program_attendees.php';
$dbh_hr_program_attendees = new hr_program_attendees;
$dbh_hr_program_attendees->set_where("hr_program_attendees_id = ?");
$dbh_hr_program_attendees->stmt_bind_param($hr_program_attendees_id);

$data = $dbh_hr_program_attendees->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

