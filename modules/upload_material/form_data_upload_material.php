<?php
require 'components/get_listview_referrer.php';

require 'subclasses/upload_material.php';
$dbh_upload_material = new upload_material;
$dbh_upload_material->set_where("upload_material_id = ?");
$dbh_upload_material->stmt_bind_param($upload_material_id);

$data = $dbh_upload_material->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$date_uploaded);
    if(count($data) == 3)
    {
        $date_uploaded_year = $data[0];
        $date_uploaded_month = $data[1];
        $date_uploaded_day = $data[2];
    }
}

