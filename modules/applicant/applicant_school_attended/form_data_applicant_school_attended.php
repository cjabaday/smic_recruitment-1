<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_school_attended.php';
$dbh_applicant_school_attended = new applicant_school_attended;
$dbh_applicant_school_attended->set_where("applicant_school_attended_id = ?");
$dbh_applicant_school_attended->stmt_bind_param($applicant_school_attended_id);

$data = $dbh_applicant_school_attended->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $temp = explode('-',$date_from);
    if(count($temp) == 3)
    {
        $date_from_year = $temp[0];
        $date_from_month = $temp[1];
        $date_from_day = $temp[2];
    }

    $temp = explode('-',$date_to);
    if(count($temp) == 3)
    {
        $date_to_year = $temp[0];
        $date_to_month = $temp[1];
        $date_to_day = $temp[2];
    }
}
