<?php

require_once "path.php";

init_cobalt();

$applicant_id = $_GET['applicant_id'];

$d = cobalt_load_class('applicant');
$d->set_table('applicant a LEFT JOIN user b on a.applicant_id = b.applicant_id');
$d->set_where('a.applicant_id = ?');
$d->stmt_bind_param($applicant_id);
$d->stmt_prepare();
$d->stmt_fetch('single');

$recepient_name  = "{$d->dump['first_name']} {$d->dump['middle_name']} {$d->dump['last_name']}";
$email = $d->dump['username'];

$email_subject = "SMIC Job Interview Invitation";
$email_body ="<p>Good Day $recepient_name,<br><br>
                \t You are invited for a Job Interview here in SM Investment Corporation  Harbor Drive Mall of Asia Complex, San Antonio Avenue, Pasay City, 1300 Metro Manila, Please reply here when you are available so we can accomodate you. <br><br><br>
                Regards,<br>
                Recruitment Team  </p>";

require_once 'components/emailer.php';

redirect('listview_applicant.php?email=success');
