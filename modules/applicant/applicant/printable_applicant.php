<?php

require_once "path.php";

init_cobalt();

if(isset($_GET['applicant_id']))
{
    $applicant_id = rawurldecode($_GET['applicant_id']);
    $iprf_get_id = rawurldecode($_GET['iprf_get_id']);
    require 'form_data_applicant.php';

    //set up tables list
    // icitizenship
    // icivilstatus
    // icourse
    // ieducationallevel
    // iemployeestatus
    // igender
    // ilanguage
    // iprf_staffrequest
    // iprf_staffrequest_applicants
    // iproficiency
    // irelationship
    //end

    $arr_set_up_table = array(
        'gender' => 'iGender',
        'citizenship' => 'iCitizenship',
        'civil_status' => 'iCivilStatus',

    );

    require 'components/fetch_data_from_setup_tables.php';

    //fetch position applying for

    $d = cobalt_load_class('iprf_staffrequest');
    $d->set_table('iprf_staffrequest a LEFT JOIN position b ON a.position_id = b.position_id
                                       LEFT JOIN iprf_staffrequest_applicants c on a.iprf_staffrequest_id = c.iprf_staffrequest_id');
    $d->set_fields('title, date_applied');
    $d->set_where('a.iprf_staffrequest_id = ?');
    $d->stmt_bind_param($iprf_get_id);
    $d->stmt_fetch('single');
    $position_title = $d->dump['title'];
    $date_of_application = $d->dump['date_applied'];



    //format data
    $present_address = "$present_address_line_1, $present_address_line_2, $present_address_barangay, $present_city, $present_province";
    $present_address_contact_no = $contact_number;

    $provincial_address = "$provincial_address_line_1, $provincial_address_line_2, $provincial_address_barangay, $provincial_city, $provincial_province";
    $provincial_address_contact_no = $provincial_address_contact_number;

    $string_date = date('F d, Y',strtotime($date_of_birth));
    require_once 'components/compute_age.php';

    $date_of_birth = $string_date;
}


init_var($cf_applicant_family_members_name,array());
init_var($cf_applicant_school_attended_school_id,array());
init_var($cf_applicant_license_license,array());
init_var($cf_applicant_previous_employers_previous_employer_name,array());
require_once "components/format_date.php";
require_once "components/draw_star.php";
require_once 'components/fetch_data_from_setup_tables_mf.php';
// debug($data);
$html = cobalt_load_class('applicant_html');
$html->draw_header_printable();
?>
<style>
/* .container {
    width: 80%;
    margin-left: auto;
    margin-right: auto;
}

.input_form td {
    padding-left: 10px;
    padding-right: 10px;
} */

/* td {
    vertical-align: super;
} */

.section_header {
    font-weight: bold;
    background-color:gray;
}

.application_container {
    margin:20px;
}

.disclaimer_content {
    margin:20px;
}

.general_info td {
    width:25%;
}


.for_print {
    visibility: hidden;
}

.hide_from_printer {
    margin:20px;
}

@media print {
    .hide_from_printer{  display:none; }

    * {
        margin:0px;
    }

    strong {
        font-size:8px;
    }

    .for_print {
        visibility: visible;
    }

    .for_print div {
        border: 1px solid;
        border-bottom:0px;
        border-left:0px;
        border-right:0px;
        padding-left:80px;
        padding-right:80px;
        margin:40px;

    }

}



</style>
<?php
    echo '<div class="container">';
?>


<div class ="hide_from_printer" style="clear:both">
    <a href="#" onclick="window.print()"> Print </a>
</div>

<div class = "application_container">
    <table width="100%" border = "1">
        <tr>
            <td colspan = "2">
                <div style="padding:15px;float:left">
                    <img src="/<?php echo BASE_DIRECTORY?>/tmp/SM_logo.jpg">
                </div>
                <div style="font-size:24px;float:left;padding-top:30px;font-weight:bold">
                    APPLICATION FOR EMPLOYMENT
                </div>

            </td>
            <td style="width:8%" rowspan="3">
                <img src="/<?php echo BASE_DIRECTORY?>/tmp/<?php echo $image?>" style="height:192px;width:192px">

            </td>
        </tr>
        <tr>
            <td rowspan = "2">
                <table class = "general_info">
                    <tr>
                        <td style="width:70%">
                            <strong>POSITION APPLIED FOR</strong>
                        </td>
                        <td>
                            <strong>DATE OF APPLICATION</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $position_title?>
                        </td>
                        <td>
                            <?php
                            $arr_date = explode('-',$date_of_application);
                            echo format_date($arr_date[0],$arr_date[1],$arr_date[2]);
                            ?>
                        </td>
                    </tr>


                </table>
            </td>

        </tr>
        <tr>

        </tr>

        <tr>
            <td class="section_header" colspan = "3">
                I. GENERAL INFORMATION
            </td>
        </tr>
        <tr>
            <td colspan = "3">
                <table class="general_info" border = "1" width="100%">
                    <tr>
                        <td>
                            <strong>SURNAME</strong><br>
                            <?php echo cobalt_htmlentities($last_name)?>
        &nbsp;                </td>
                        <td>
                            <strong>FIRST NAME</strong><br>
                            <?php echo cobalt_htmlentities($first_name)?>

    &nbsp;                    </td>
                        <td>
                            <strong>MIDDLE NAME</strong><br>
                            <?php echo cobalt_htmlentities($middle_name)?>
        &nbsp;                </td>
                        <td>
                            <strong>NICKNAME</strong><br>
                            <?php echo cobalt_htmlentities($nickname)  ?>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan = "3">
                            <strong>PRESENT CITY ADDRESS</strong><br>
                            <?php echo cobalt_htmlentities($present_address)  ?>&nbsp;
                        </td>
                        <td>
                            <strong>PRESENT CONTACT NO.</strong><br>
                            <?php echo cobalt_htmlentities($present_address_contact_no)  ?>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan = "3">
                            <strong>PERMANENT CITY ADDRESS</strong><br>
                            <?php echo cobalt_htmlentities($provincial_address)  ?>&nbsp;
                        </td>
                        <td>
                            <strong>PERMANENT CONTACT NO.</strong><br>
                            <?php echo cobalt_htmlentities($provincial_address_contact_no)  ?>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>GENDER</strong><br>
                            <?php echo cobalt_htmlentities($gender)  ?>&nbsp;
                        </td>
                        <td>
                            <strong>CIVIL STATUS</strong><br>
                            <?php echo cobalt_htmlentities($civil_status)  ?>&nbsp;

                        </td>
                        <td>
                            <strong>RELIGION</strong><br>
                            <?php echo cobalt_htmlentities($religion)  ?>&nbsp;
                        </td>
                        <td>
                            <strong>CITIZENSHIP</strong><br>
                            <?php echo cobalt_htmlentities($citizenship)  ?>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan = "2">
                            <strong>PLACE OF BIRTH</strong><br>
                            <?php echo cobalt_htmlentities($birth_place)  ?>&nbsp;
                        </td>
                        <td>
                            <strong>DATE OF BIRTH</strong><br>
                            <?php echo cobalt_htmlentities($date_of_birth)  ?>&nbsp;
                        </td>
                        <td>
                            <strong>AGE</strong><br>
                            <?php echo cobalt_htmlentities($age)  ?>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan = "2">
                            <strong>APPLICATION SOURCE</strong><br>
                            Online Application&nbsp;
                        </td>
                        <td colspan = "2">
                            <strong>EMAIL ADDRESS</strong><br>

                            <?php
                            echo cobalt_htmlentities($personal_email)  ?>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>SSS NUMBER</strong><br>
                            <?php echo cobalt_htmlentities($sss_number)  ?>&nbsp;
                        </td>
                        <td>
                            <strong>HDMF NUMBER</strong><br>
                            <?php echo cobalt_htmlentities($hdmf_number)  ?>&nbsp;

                        </td>
                        <td>
                            <strong>PHILHEALTH NUMBER</strong><br>
                            <?php echo cobalt_htmlentities($philhealth_id_number)  ?>&nbsp;
                        </td>
                        <td>
                            <strong>TAX IDENTIFICATION NUMBER</strong><br>
                            <?php echo cobalt_htmlentities($tin)  ?>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="section_header" colspan = "3">
                II. FAMILY BACKGROUND
            </td>
        </tr>
        <tr>
            <td colspan = "3">
                <table class="family_info" border = "1" width="100%">
                    <tr>
                        <th width="25%">
                            <strong>NAME</strong><br>
                        </th>
                        <th width="20%">
                            <strong>RELATIONSHIP</strong><br>

                        </th>
                        <th width="20%">
                            <strong>BIRTH DATE</strong><br>
                        </th>
                        <th width="10%">
                            <strong>AGE</strong><br>
                        </th>
                        <th width="25%">
                            <strong>DEPENDENT</strong><br>
                        </th>
                    </tr>
                        <!-- loop family background through here -->
                    <?php

                    if(count($cf_applicant_family_members_name) > 0)
                    {
                        for($a = 0; $a < count($cf_applicant_family_members_name); ++$a)
                        {
                            echo "
                            <tr>
                                <td>
                                    ".cobalt_htmlentities($cf_applicant_family_members_name[$a])." &nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities(fetch_desc_title('iRelationship',$cf_applicant_family_members_relationship[$a]))." &nbsp;
                                </td>
                                <td>
                                    ".format_date($cf_applicant_family_members_birthday_year[$a],$cf_applicant_family_members_birthday_month[$a],$cf_applicant_family_members_birthday_day[$a])."&nbsp;
                                </td>
                                <td align = 'center'>
                                    ".cobalt_htmlentities($cf_applicant_family_members_age[$a])."&nbsp;
                                </td>

                                <td>
                                    ".cobalt_htmlentities($cf_applicant_family_members_is_dependent[$a])."&nbsp;
                                </td>
                            </tr>
                            ";
                        }
                    }
                    else
                    {
                        echo "
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>

                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        ";
                    }
                    ?>


                </table>
            </td>
        </tr>
        <tr>
            <td class="section_header" colspan = "3">
                III. EDUCATIONAL BACKGROUND
            </td>
        </tr>
        <tr>
            <td colspan = "3">
                <table class="educational_info" border = "1" width="100%">
                    <tr>
                        <th width="25%">
                            <strong>NAME OF SCHOOL</strong><br>

                        </th>
                        <th width="15%">
                            <strong>LEVEL</strong><br>
                        </th>
                        <th width="25%">
                            <strong>COURSE</strong><br>
                        </th>
                        <th width="5%">
                            <strong>FROM</strong><br>
                        </th>
                        <th width="5%">
                            <strong>TO</strong><br>
                        </th>
                        <th width="25%">
                            <strong>HONORS/AWARDS and SCHOLARSHIPS RECEIVED</strong><br>
                        </th>
                    </tr>

                    <!-- loop educational info through here -->
                    <?php

                    if(count($cf_applicant_school_attended_school_id) > 0)
                    {
                        for($a = 0; $a < count($cf_applicant_school_attended_school_id); ++$a)
                        {
                            echo "
                            <tr>
                                <td>
                                    ".cobalt_htmlentities($cf_applicant_school_attended_name[$a]) ."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities(fetch_desc_title('iEducationalLevel',$cf_applicant_school_attended_educational_attainment[$a])) ."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities(fetch_desc_title('iCourse',$cf_applicant_school_attended_course[$a])) ."&nbsp;
                                </td>
                                <td align = 'center'>
                                    ".cobalt_htmlentities($cf_applicant_school_attended_date_from_year[$a]) ."&nbsp;
                                </td>

                                <td align = 'center'>
                                    ".cobalt_htmlentities($cf_applicant_school_attended_date_to_year[$a])."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_applicant_school_attended_awards[$a]) ."&nbsp;
                                </td>
                            </tr>
                            ";
                        }
                    }

                    else
                    {
                        echo "
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>

                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        ";
                    }
                    ?>

                </table>
            </td>
        </tr>
        <tr>
            <td class="section_header" colspan = "3">
                IV. LICENSES
            </td>
        </tr>
        <tr>
            <td colspan = "3">
                <table class="licenses_info" border = "1" width="100%">
                    <tr>
                        <th width="40%">
                            <strong>TYPE</strong><br>
                        </th>
                        <th width="35%">
                            <strong>LICENSE NO.</strong><br>

                        </th>
                        <th width="25%">
                            <strong>DATE EXPIRY</strong><br>
                        </th>
                    </tr>

                    <!-- loop educational info through here -->

                    <?php
                    if(count($cf_applicant_license_license) > 0)
                    {
                        for($a = 0; $a < count($cf_applicant_license_license); ++$a)
                        {
                            $formatted_date = format_date($cf_applicant_license_license_expiry_year[$a],$cf_applicant_license_license_expiry_month[$a],$cf_applicant_license_license_expiry_day[$a]);
                            echo "
                                <tr>
                                    <td>
                                        ".cobalt_htmlentities($cf_applicant_license_license[$a]) ."&nbsp;
                                    </td>
                                    <td>
                                        ".cobalt_htmlentities($cf_applicant_license_license_number[$a]) ."&nbsp;
                                    </td>
                                    <td align='center'>
                                        ".cobalt_htmlentities($formatted_date)."&nbsp;
                                    </td>
                                </tr>
                            ";

                        }

                    }
                    else
                    {
                        echo "
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        ";
                    }


                    ?>

                </table>
            </td>
        </tr>
        <tr>
            <td class="section_header" colspan = "3">
                V. WORK EXPERIENCE/S
            </td>
        </tr>
        <tr>
            <td colspan = "3">
                <table class="work_info" border = "1" width="100%">
                    <tr>
                        <th width="35%">
                            <strong>COMPANY / ADDRESS</strong><br>
                        </th>
                        <th width="10%">
                            <strong>FROM</strong><br>
                        </th>
                        <th width="10%">
                            <strong>TO</strong><br>
                        </th>
                        <th width="15%">
                            <strong>POSITION</strong><br>
                        </th>
                        <th width="5%">
                            <strong>LAST SALARY</strong><br>
                        </th>
                        <th width="25%">
                            <strong>REASON FOR LEAVING</strong><br>
                        </th>
                    </tr>

                    <!-- loop family background through here -->
                    <?php
                    if(count($cf_applicant_previous_employers_previous_employer_name) > 0)
                    {
                        for($a = 0; $a < count($cf_applicant_previous_employers_previous_employer_name); ++$a)
                        {

                            $from_date = format_date($cf_applicant_previous_employers_previous_employer_date_from_year[$a],$cf_applicant_previous_employers_previous_employer_date_from_month[$a],$cf_applicant_previous_employers_previous_employer_date_from_day[$a]);
                            $to_date = format_date($cf_applicant_previous_employers_previous_employer_date_to_year[$a],$cf_applicant_previous_employers_previous_employer_date_to_month[$a],$cf_applicant_previous_employers_previous_employer_date_to_day[$a]);
                            echo "
                            <tr>
                                <td>
                                    ".cobalt_htmlentities($cf_applicant_previous_employers_previous_employer_name[$a])."&nbsp;
                                </td>
                                <td align = 'center'>
                                    $from_date&nbsp;
                                </td>
                                <td align = 'center'>
                                    $to_date&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_applicant_previous_employers_previous_employer_position[$a])."&nbsp;
                                </td>

                                <td align = 'right'>
                                    ".cobalt_htmlentities(number_format($cf_applicant_previous_employers_previous_employer_basic_salary[$a],2))."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_applicant_previous_employers_previous_employer_reason_for_leaving[$a])."&nbsp;
                                </td>
                            </tr>
                            ";
                        }
                    }
                    else
                    {
                        echo "
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>

                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        ";
                    }
                    ?>

                </table>
            </td>
        </tr>
        <tr>
            <td class="section_header" colspan = "3">
                VI. REFERENCES
            </td>
        </tr>
        <tr>
            <td colspan = "3">
                <table class="references_info" border = "1" width="100%">
                    <tr>
                        <th width="30%">
                            <strong>NAME</strong><br>
                        </th>
                        <th width="30%">
                            <strong>ADDRESS / TELEPHONE NO.</strong><br>

                        </th>
                        <th width="20%">
                            <strong>OCCUPATION</strong><br>
                        </th>
                        <th width="20%">
                            <strong>HOW LONG KNOWN</strong><br>
                        </th>
                    </tr>

                    <!-- loop family background through here -->
                    <?php
                    init_var($cf_applicant_reference_reference_name,array());
                    if(count($cf_applicant_reference_reference_name) > 0)
                    {
                        for($a = 0 ; $a < count($cf_applicant_reference_reference_name); ++$a)
                        {
                            echo "
                            <tr>
                                <td>
                                    ".cobalt_htmlentities($cf_applicant_reference_reference_name[$a])."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_applicant_reference_reference_address[$a])." / ".cobalt_htmlentities($cf_applicant_reference_reference_contact_number[$a])."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_applicant_reference_reference_occupation[$a])."&nbsp;
                                </td>
                                <td align='center'>
                                    ".cobalt_htmlentities($cf_applicant_reference_years_known[$a])."&nbsp;
                                </td>
                            </tr>
                            ";
                        }

                    }
                    else
                    {
                        echo "
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        ";
                    }

                    ?>

                </table>
            </td>
        </tr>
        <tr>
            <td class="section_header" colspan = "3">
                VII. OTHER INFORMATION
            </td>
        </tr>
        <tr>
            <td colspan = "3">
                <table class="skills_info" border = "1" width="100%">
                    <tr>
                        <th width="34%">
                            <strong>SKILLS</strong><br>
                        </th>
                        <th width="33%">
                            <strong>YEARS OF EXPERIENCE</strong><br>

                        </th>
                        <th width="33%">
                            <strong>PROFICIENCY LEVEL</strong><br>
                        </th>
                    </tr>

                    <!-- loop family background through here -->
                    <?php
                    init_var($cf_applicant_skills_skill,array());
                    if(count($cf_applicant_skills_skill) > 0)
                    {
                        for($a = 0; $a < count($cf_applicant_skills_skill); ++$a)
                        {
                            echo "
                            <tr>
                                <td>
                                    ".cobalt_htmlentities($cf_applicant_skills_skill[$a])."&nbsp;
                                </td>
                                <td align='center'>
                                    ".cobalt_htmlentities($cf_applicant_skills_years_of_experience[$a])."&nbsp;
                                </td>
                                <td >
                                    ".cobalt_htmlentities(fetch_desc_title('iProficiency',$cf_applicant_skills_proficiency[$a]))."&nbsp;
                                </td>
                            </tr>
                            ";
                        }

                    }
                    else
                    {
                        echo "
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        ";
                    }
                    ?>

                </table>
            </td>
        </tr>
        <tr>
            <td colspan = "3">
                <table class="languages_info" border = "1" width="100%">
                    <tr>
                        <th width="34%">
                            <strong>LANGUAGES DESCRIPTION</strong><br>
                        </th>
                        <th width="33%">
                            <strong>SPOKEN PROFICIENCY</strong><br>

                        </th>
                        <th width="33%">
                            <strong>WRITTEN PROFICIENCY</strong><br>
                        </th>
                    </tr>

                    <!-- loop family background through here -->
                    <?php
                    init_var($cf_applicant_languages_proficiency_language, array());
                    if(count($cf_applicant_languages_proficiency_language) > 0)
                    {
                        for($a = 0; $a < count($cf_applicant_languages_proficiency_language); ++$a)
                        {
                            echo "
                            <tr>
                                <td>
                                    ".$cf_applicant_languages_proficiency_language[$a]."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities(fetch_desc_title('iProficiency',$cf_applicant_languages_proficiency_speaking_proficiency[$a]))."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities(fetch_desc_title('iProficiency',$cf_applicant_languages_proficiency_writing_proficiency[$a]))."&nbsp;
                                </td>
                            </tr>
                            ";
                        }
                    }
                    else
                    {
                        echo "
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        ";
                    }
                    ?>


                </table>
            </td>
        </tr>
        <tr>
            <td class="section_header" colspan = "3">
                VIII. NAME OF RELATIVE/S IN OUR EMPLOY
            </td>
        </tr>
        <tr>
            <td colspan = "3">
                <table class="relatives_info" border = "1" width="100%">
                    <tr>
                        <th width="20%">
                            <strong>FULL NAME</strong><br>
                        </th>
                        <th width="20%">
                            <strong>RELATIONSHIP</strong><br>
                        </th>
                        <th width="20%">
                            <strong>POSITION</strong><br>
                        </th>
                        <th width="20%">
                            <strong>COMPANY DEPARTMENT</strong><br>
                        </th>
                    </tr>

                    <!-- loop family background through here -->
                    <?php
                    init_var($cf_other_declarations_name_smic_employ_name,array());
                    if(count($cf_other_declarations_name_smic_employ_name) > 0)
                    {
                        for($a = 0; $a < count($cf_other_declarations_name_smic_employ_name); ++$a)
                        {
                            echo "
                            <tr>
                                <td>
                                    ".cobalt_htmlentities($cf_other_declarations_name_smic_employ_name[$a])."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_other_declarations_name_smic_employ_relationship[$a])."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_other_declarations_name_smic_employ_position[$a])."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_other_declarations_name_smic_employ_department_company[$a])."&nbsp;
                                </td>
                            </tr>
                            ";
                        }
                    }
                    else
                    {
                        echo "
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        ";
                    }
                    ?>

                </table>
            </td>
        </tr>
        <tr>
            <td class="section_header" colspan = "3">
                IX. NAME OF RELATIVE/S OR FRIEND/S EMPLOYED WITH ANY OF THE SM GROUP OF COMPANIES
            </td>
        </tr>
        <tr>
            <td colspan = "3">
                <table class="relatives_info" border = "1" width="100%">
                    <tr>
                        <th width="20%">
                            <strong>FULL NAME</strong><br>
                        </th>
                        <th width="20%">
                            <strong>RELATIONSHIP</strong><br>
                        </th>
                        <th width="20%">
                            <strong>POSITION</strong><br>
                        </th>
                        <th width="20%">
                            <strong>COMPANY DEPARTMENT</strong><br>
                        </th>
                    </tr>

                    <?php
                    init_var($cf_other_declarations_name_smgroup_name,array());
                    if(count($cf_other_declarations_name_smgroup_name) > 0)
                    {
                        for($a = 0; $a < count($cf_other_declarations_name_smgroup_name); ++$a)
                        {
                            echo "
                            <tr>
                                <td>
                                    ".cobalt_htmlentities($cf_other_declarations_name_smgroup_name[$a])."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_other_declarations_name_smgroup_relationship[$a])."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_other_declarations_name_smgroup_position[$a])."&nbsp;
                                </td>
                                <td>
                                    ".cobalt_htmlentities($cf_other_declarations_name_smgroup_company[$a])."&nbsp;
                                </td>
                            </tr>
                            ";
                        }
                    }
                    else
                    {
                        echo "
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        ";
                    }
                    ?>
                </table>
            </td>
        </tr>
    </table>
</div>

<div class = "disclaimer_content">
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;I hereby affirm to the best of my knowledge and belief that all the answers to the foregoing are true and correct. I acknowledge that the filing of this application does not entitle me to any acquired right to employment and SM Investments Corporation may dispose of this application in any manner it so desires. I also authorize SM Investments Corporation to inquire as to my record from any or all of my former employers / the schools I attended, and release the company from any liability arising from such inquiry.
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;I further acknowledge that any misrepresentation in the foregoing answers and data given shall sufficient grounds for my dismissal from SM Investments Corporation.
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;I further grant my express, unconditional, voluntary and informed consent to, and hereby authorize SM Investments Corporation and other internal parties involved in processing the information so disclosed, to collect, store, access, use, verify, process and/or dispose my Information necessary for the purpose of and in the course of my application, or termination or cessation of such application, hereinafter collectively referred to as “Purposes,” including but not limited to carrying out data analytics, management, profiling, manual or automated decision-making, and any activity in the furtherance of the Purposes.
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;I knowingly acknowledge and confirm that I have been informed of my rights under the law with respect to my information, and that I am solely responsible for the consequences of such disclosure, transfer and sharing of such, including all requirements, records and documents, which shall be submitted to SM Investments Corporation for similar Purposes and of the same nature, and which are generally made known or disclosed by me or any other third party through social media and other non-private means. Thus, I hereby hold the Company free and harmless from suits in connection with or arising from privacy breach of my personal data.
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Finally, I hereby voluntarily confirm the due execution, validity and effectivity of this consent clause, which I have executed out of my own volition and free will, for as long as necessary within the period allowed by the applicable laws and regulations.

    </p>
</div>

<div class = "for_print" width = "100%">
    <div style="float:right">
        Date
    </div>
    <div style="float:right">
        Applicant's Signature
    </div>
</div>
