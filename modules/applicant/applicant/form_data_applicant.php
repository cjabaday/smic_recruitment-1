<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant.php';
$dbh_applicant = new applicant;
$dbh_applicant->set_where("applicant_id = ?");
$dbh_applicant->stmt_bind_param($applicant_id);

//get email
$dbh = cobalt_load_class('user');
$dbh->set_fields('username');
$dbh->set_where('applicant_id = ?');
$dbh->stmt_bind_param($applicant_id);
$personal_email = $dbh->stmt_fetch('single')->dump['username'];

// debug($email_address);
$data = $dbh_applicant->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$application_date);
    if(count($data) == 3)
    {
        $application_date_year = $data[0];
        $application_date_month = $data[1];
        $application_date_day = $data[2];
    }

    $data = explode('-',$date_of_birth);
    if(count($data) == 3)
    {
        $date_of_birth_year = $data[0];
        $date_of_birth_month = $data[1];
        $date_of_birth_day = $data[2];
    }
}

// fetch city and province
$dbh = cobalt_load_class('city');
$dbh->get_city($present_address_city_municipality_id);
$present_city = ucwords(strtolower($dbh->stmt_fetch('single')->dump['city_name']));

$dbh = cobalt_load_class('city');
$dbh->get_city($provincial_address_city_municipality_id);
$provincial_city = ucwords(strtolower($dbh->stmt_fetch('single')->dump['city_name']));

$dbh = cobalt_load_class('province');
$dbh->get_province($present_address_province_id);
$present_province = ucwords(strtolower($dbh->stmt_fetch('single')->dump['province_name']));

$dbh = cobalt_load_class('province');
$dbh->get_province($provincial_address_province_id);
$provincial_province = ucwords(strtolower($dbh->stmt_fetch('single')->dump['province_name']));


// debug($email_address);
require_once 'subclasses/applicant_family_members.php';
$dbh_applicant_family_members = new applicant_family_members;
$dbh_applicant_family_members->set_fields('name, birthday, age, relationship, is_dependent');
$dbh_applicant_family_members->set_where("applicant_id = ?");
$dbh_applicant_family_members->stmt_bind_param($applicant_id);

$data = $dbh_applicant_family_members->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_applicant_family_members = count($data);
$a = 0;

foreach($data as $column)
{
        $cf_applicant_family_members_name[$a] = $column['name'];
        $cf_applicant_family_members_birthday[$a] = $column['birthday'];
        $data_temp_cf_date = explode('-',$column['birthday']);
        $cf_applicant_family_members_birthday_year[$a]  = $data_temp_cf_date[0];
        $cf_applicant_family_members_birthday_month[$a] = $data_temp_cf_date[1];
        $cf_applicant_family_members_birthday_day[$a]   = $data_temp_cf_date[2];
        $cf_applicant_family_members_age[$a] = $column['age'];
        $cf_applicant_family_members_relationship[$a] = $column['relationship'];
        $cf_applicant_family_members_is_dependent[$a] = $column['is_dependent'];
++$a;
}

require_once 'subclasses/applicant_interview.php';
$dbh_applicant_interview = new applicant_interview;
$dbh_applicant_interview->set_fields('interviewer_employee_id, interview_date, remarks');
$dbh_applicant_interview->set_where("applicant_id = ?");
$dbh_applicant_interview->stmt_bind_param($applicant_id);

$data = $dbh_applicant_interview->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_applicant_interview = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_applicant_interview_interviewer_employee_id[$a] = $column['interviewer_employee_id'];
        $data_temp_cf_date = explode('-',$column['interview_date']);
        $cf_applicant_interview_interview_date_year[$a]  = $data_temp_cf_date[0];
        $cf_applicant_interview_interview_date_month[$a] = $data_temp_cf_date[1];
        $cf_applicant_interview_interview_date_day[$a]   = $data_temp_cf_date[2];
        $cf_applicant_interview_remarks[$a] = $column['remarks'];
++$a;
}

require_once 'subclasses/applicant_languages_proficiency.php';
$dbh_applicant_languages_proficiency = new applicant_languages_proficiency;
$dbh_applicant_languages_proficiency->set_table('applicant_languages_proficiency a LEFT JOIN ilanguage b ON a.language = b.LanguageID');
$dbh_applicant_languages_proficiency->set_fields('b.LanguageDesc, speaking_proficiency, writing_proficiency');
$dbh_applicant_languages_proficiency->set_where("applicant_id = ?");
$dbh_applicant_languages_proficiency->stmt_bind_param($applicant_id);

$data = $dbh_applicant_languages_proficiency->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_applicant_languages_proficiency = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_applicant_languages_proficiency_language[$a] = $column['LanguageDesc'];
        $cf_applicant_languages_proficiency_speaking_proficiency[$a] = $column['speaking_proficiency'];
        $cf_applicant_languages_proficiency_writing_proficiency[$a] = $column['writing_proficiency'];
++$a;
}

require_once 'subclasses/applicant_license.php';
$dbh_applicant_license = new applicant_license;
$dbh_applicant_license->set_fields('license, license_number, license_expiry');
$dbh_applicant_license->set_where("applicant_id = ?");
$dbh_applicant_license->stmt_bind_param($applicant_id);

$data = $dbh_applicant_license->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_applicant_license = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_applicant_license_license[$a] = $column['license'];
        $cf_applicant_license_license_number[$a] = $column['license_number'];
        $cf_applicant_license_license_expiry[$a] = $column['license_expiry'];

        $data_temp_cf_date = explode('-',$column['license_expiry']);
        $cf_applicant_license_license_expiry_year[$a]  = $data_temp_cf_date[0];
        $cf_applicant_license_license_expiry_month[$a] = $data_temp_cf_date[1];
        $cf_applicant_license_license_expiry_day[$a]   = $data_temp_cf_date[2];
++$a;
}

require_once 'subclasses/applicant_previous_employers.php';
$dbh_applicant_previous_employers = new applicant_previous_employers;
$dbh_applicant_previous_employers->set_fields('previous_employer_name, previous_employer_position, previous_employer_date_from, previous_employer_date_to, reason_for_leaving, basic_salary');
$dbh_applicant_previous_employers->set_where("applicant_id = ?");
$dbh_applicant_previous_employers->stmt_bind_param($applicant_id);

$data = $dbh_applicant_previous_employers->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_applicant_previous_employers = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_applicant_previous_employers_previous_employer_name[$a] = $column['previous_employer_name'];
        $cf_applicant_previous_employers_previous_employer_position[$a] = $column['previous_employer_position'];
        $cf_applicant_previous_employers_previous_employer_reason_for_leaving[$a] = $column['reason_for_leaving'];
        $cf_applicant_previous_employers_previous_employer_basic_salary[$a] = $column['basic_salary'];
        $cf_applicant_previous_employers_previous_employer_date_from[$a] = $column['previous_employer_date_from'];
        $data_temp_cf_date = explode('-',$column['previous_employer_date_from']);
        $cf_applicant_previous_employers_previous_employer_date_from_year[$a]  = $data_temp_cf_date[0];
        $cf_applicant_previous_employers_previous_employer_date_from_month[$a] = $data_temp_cf_date[1];
        $cf_applicant_previous_employers_previous_employer_date_from_day[$a]   = $data_temp_cf_date[2];
        $cf_applicant_previous_employers_previous_employer_date_to[$a] = $column['previous_employer_date_to'];
        $data_temp_cf_date = explode('-',$column['previous_employer_date_to']);
        $cf_applicant_previous_employers_previous_employer_date_to_year[$a]  = $data_temp_cf_date[0];
        $cf_applicant_previous_employers_previous_employer_date_to_month[$a] = $data_temp_cf_date[1];
        $cf_applicant_previous_employers_previous_employer_date_to_day[$a]   = $data_temp_cf_date[2];
++$a;
}

require_once 'subclasses/applicant_reference.php';
$dbh_applicant_reference = new applicant_reference;
$dbh_applicant_reference->set_fields('reference_name, reference_occupation, reference_relationship, reference_additional_information, reference_address, reference_contact_number, years_known');
$dbh_applicant_reference->set_where("applicant_id = ?");
$dbh_applicant_reference->stmt_bind_param($applicant_id);

$data = $dbh_applicant_reference->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_applicant_reference = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_applicant_reference_reference_address[$a] = $column['reference_address'];
        $cf_applicant_reference_reference_name[$a] = $column['reference_name'];
        $cf_applicant_reference_reference_occupation[$a] = $column['reference_occupation'];
        $cf_applicant_reference_reference_relationship[$a] = $column['reference_relationship'];
        $cf_applicant_reference_reference_additional_information[$a] = $column['reference_additional_information'];
        $cf_applicant_reference_reference_contact_number[$a] = $column['reference_contact_number'];
        $cf_applicant_reference_years_known[$a] = $column['years_known'];
++$a;
}

require_once 'subclasses/applicant_school_attended.php';
$dbh_applicant_school_attended = new applicant_school_attended;
$dbh_applicant_school_attended->set_table('applicant_school_attended LEFT JOIN school on applicant_school_attended.school_id = school.school_id');
$dbh_applicant_school_attended->set_fields('school.school_id, school.name, address, date_from, date_to, educational_attainment, course, awards');
$dbh_applicant_school_attended->set_where("applicant_id = ?");
$dbh_applicant_school_attended->stmt_bind_param($applicant_id);
$data = $dbh_applicant_school_attended->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_applicant_school_attended = count($data);
$a = 0;
foreach($data as $column)
{
    // debug($column);
        $cf_applicant_school_attended_name[$a] = $column['name'];
        $cf_applicant_school_attended_school_id[$a] = $column['school_id'];
        $cf_applicant_school_attended_educational_attainment[$a] = $column['educational_attainment'];
        $cf_applicant_school_attended_course[$a] = $column['course'];
        $cf_applicant_school_attended_awards[$a] = $column['awards'];
        $cf_applicant_school_attended_address[$a] = $column['address'];
        $cf_applicant_school_attended_date_from[$a] = $column['date_from'];

        $data_temp_cf_date = explode('-',$column['date_from']);
        $cf_applicant_school_attended_date_from_year[$a]  = $data_temp_cf_date[0];
        $cf_applicant_school_attended_date_from_month[$a] = $data_temp_cf_date[1];
        $cf_applicant_school_attended_date_from_day[$a]   = $data_temp_cf_date[2];
        $cf_applicant_school_attended_date_to[$a] = $column['date_to'];
        $data_temp_cf_date = explode('-',$column['date_to']);
        $cf_applicant_school_attended_date_to_year[$a]  = $data_temp_cf_date[0];
        $cf_applicant_school_attended_date_to_month[$a] = $data_temp_cf_date[1];
        $cf_applicant_school_attended_date_to_day[$a]   = $data_temp_cf_date[2];
++$a;
}

require_once 'subclasses/applicant_skills.php';
$dbh_applicant_skills = new applicant_skills;
$dbh_applicant_skills->set_fields('skill, proficiency, years_of_experience');
$dbh_applicant_skills->set_where("applicant_id = ?");
$dbh_applicant_skills->stmt_bind_param($applicant_id);

$data = $dbh_applicant_skills->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_applicant_skills = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_applicant_skills_skill[$a] = $column['skill'];
        $cf_applicant_skills_proficiency[$a] = $column['proficiency'];
        $cf_applicant_skills_years_of_experience[$a] = $column['years_of_experience'];
++$a;
}

require_once 'subclasses/applicant_attachments.php';
$dbh_applicant_attachments = new applicant_attachments;
$dbh_applicant_attachments->set_fields('attachments, date_uploaded');
$dbh_applicant_attachments->set_where("applicant_id = ?");
$dbh_applicant_attachments->stmt_bind_param($applicant_id);

$data = $dbh_applicant_attachments->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_applicant_attachments = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_applicant_attachments_attachments[$a] = $column['attachments'];
        $cf_applicant_attachments_date_uploaded[$a] = $column['date_uploaded'];
++$a;
}

require_once 'subclasses/applicant_preferred_positions.php';
$dbh_applicant_preferred_positions = new applicant_preferred_positions;
$dbh_applicant_preferred_positions->set_fields('position_id, priority');
$dbh_applicant_preferred_positions->set_where("applicant_id = ?");
$dbh_applicant_preferred_positions->stmt_bind_param($applicant_id);

$data = $dbh_applicant_preferred_positions->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_applicant_preferred_positions = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_applicant_preferred_positions_position_id[$a] = $column['position_id'];
        $cf_applicant_preferred_positions_priority[$a] = $column['priority'];
++$a;
}

//fetch(other_declaration_id )
require_once 'subclasses/applicant_other_declarations.php';
$dbh_applicant_other_declarations = new applicant_other_declarations;
$dbh_applicant_other_declarations->set_fields('applicant_other_declaration_id');
$dbh_applicant_other_declarations->set_where("applicant_id = ?");
$dbh_applicant_other_declarations->stmt_bind_param($applicant_id);

$applicant_other_declaration_id = $dbh_applicant_other_declarations->stmt_prepare()->stmt_fetch('single')->dump['applicant_other_declaration_id'];


require_once 'subclasses/other_declarations_name_smgroup.php';
$dbh_other_declarations_name_smgroup = new other_declarations_name_smgroup;
$dbh_other_declarations_name_smgroup->set_fields('name, relationship, position, company');
$dbh_other_declarations_name_smgroup->set_where("applicant_other_declaration_id = ?");
$dbh_other_declarations_name_smgroup->stmt_bind_param($applicant_other_declaration_id);

$data = $dbh_other_declarations_name_smgroup->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_other_declarations_name_smgroup = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_other_declarations_name_smgroup_name[$a] = $column['name'];
        $cf_other_declarations_name_smgroup_company[$a] = $column['company'];
        $cf_other_declarations_name_smgroup_relationship[$a] = $column['relationship'];
        $cf_other_declarations_name_smgroup_position[$a] = $column['position'];
++$a;
}

require_once 'subclasses/other_declarations_name_smic_employ.php';
$dbh_other_declarations_name_smic_employ = new other_declarations_name_smic_employ;
$dbh_other_declarations_name_smic_employ->set_fields('name, relationship, position, department_company');
$dbh_other_declarations_name_smic_employ->set_where("applicant_other_declaration_id = ?");
$dbh_other_declarations_name_smic_employ->stmt_bind_param($applicant_other_declaration_id);

$data = $dbh_other_declarations_name_smic_employ->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_other_declarations_name_smic_employ = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_other_declarations_name_smic_employ_name[$a] = $column['name'];
        $cf_other_declarations_name_smic_employ_department_company[$a] = $column['department_company'];
        $cf_other_declarations_name_smic_employ_relationship[$a] = $column['relationship'];
        $cf_other_declarations_name_smic_employ_position[$a] = $column['position'];
++$a;
}
