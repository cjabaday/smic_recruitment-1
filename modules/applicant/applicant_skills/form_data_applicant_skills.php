<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_skills.php';
$dbh_applicant_skills = new applicant_skills;
$dbh_applicant_skills->set_where("applicant_skill_id = ?");
$dbh_applicant_skills->stmt_bind_param($applicant_skill_id);

$data = $dbh_applicant_skills->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

