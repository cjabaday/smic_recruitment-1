<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_languages_proficiency.php';
$dbh_applicant_languages_proficiency = new applicant_languages_proficiency;
$dbh_applicant_languages_proficiency->set_fields("language");
$dbh_applicant_languages_proficiency->set_where("applicant_language_proficiency_id = ?");
$dbh_applicant_languages_proficiency->stmt_bind_param($applicant_language_proficiency_id);

$data = $dbh_applicant_languages_proficiency->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

}
