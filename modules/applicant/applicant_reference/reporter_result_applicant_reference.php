<?php
//****************************************************************************************
//Generated by Cobalt, a rapid application development framework. http://cobalt.jvroig.com
//Cobalt developed by JV Roig (jvroig@jvroig.com)
//****************************************************************************************
require 'path.php';
init_cobalt('View applicant reference');

require 'reporter_class.php';
$reporter = cobalt_load_class('applicant_reference_rpt');
require 'components/reporter_result_query_constructor.php';
require 'components/reporter_result_body.php';