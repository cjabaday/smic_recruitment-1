<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_interview.php';
$dbh_applicant_interview = new applicant_interview;
$dbh_applicant_interview->set_where("applicant_interview_id = ?");
$dbh_applicant_interview->stmt_bind_param($applicant_interview_id);

$data = $dbh_applicant_interview->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$interview_date);
    if(count($data) == 3)
    {
        $interview_date_year = $data[0];
        $interview_date_month = $data[1];
        $interview_date_day = $data[2];
    }
}

