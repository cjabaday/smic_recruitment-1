<?php
//****************************************************************************************
//Generated by Cobalt, a rapid application development framework. http://cobalt.jvroig.com
//Cobalt developed by JV Roig (jvroig@jvroig.com)
//****************************************************************************************
require 'path.php';
init_cobalt('View branch type');

if(isset($_GET['branch_type_id']))
{
    $branch_type_id = rawurldecode($_GET['branch_type_id']);
    require 'form_data_branch_type.php';
}

if(xsrf_guard())
{
    init_var($_POST['btn_back']);

    if($_POST['btn_back'])
    {
        log_action('Pressed cancel button');
        require 'components/query_string_standard.php';
        redirect("listview_branch_type.php?$query_string");
    }
}
require 'subclasses/branch_type_html.php';
$html = new branch_type_html;
$html->draw_header('Detail View: %%', $message, $message_type);
$html->draw_listview_referrer_info($filter_field_used, $filter_used, $page_from, $filter_sort_asc, $filter_sort_desc);
$html->detail_view = TRUE;
$html->draw_controls('view');

$html->draw_footer();