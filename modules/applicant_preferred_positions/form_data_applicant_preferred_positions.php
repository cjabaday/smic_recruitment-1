<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_preferred_positions.php';
$dbh_applicant_preferred_positions = new applicant_preferred_positions;
$dbh_applicant_preferred_positions->set_where("applicant_preferred_position_id = ?");
$dbh_applicant_preferred_positions->stmt_bind_param($applicant_preferred_position_id);

$data = $dbh_applicant_preferred_positions->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

