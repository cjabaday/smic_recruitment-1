<?php
require 'components/get_listview_referrer.php';

require 'subclasses/action_notice.php';
$dbh_action_notice = new action_notice;
$dbh_action_notice->set_where("action_notice_id = ?");
$dbh_action_notice->stmt_bind_param($action_notice_id);

$data = $dbh_action_notice->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$action_notice_date);
    if(count($data) == 3)
    {
        $action_notice_date_year = $data[0];
        $action_notice_date_month = $data[1];
        $action_notice_date_day = $data[2];
    }
}

