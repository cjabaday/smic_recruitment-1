<?php
require 'components/get_listview_referrer.php';

require 'subclasses/employee.php';
$dbh_employee = new employee;
$dbh_employee->set_where("employee_id = ?");
$dbh_employee->stmt_bind_param($employee_id);

$data = $dbh_employee->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$birthdate);
    if(count($data) == 3)
    {
        $birthdate_year = $data[0];
        $birthdate_month = $data[1];
        $birthdate_day = $data[2];
    }
    $data = explode('-',$start_date);
    if(count($data) == 3)
    {
        $start_date_year = $data[0];
        $start_date_month = $data[1];
        $start_date_day = $data[2];
    }
}

