<?php
require 'components/get_listview_referrer.php';

require 'subclasses/employee_evaluation_template.php';
$dbh_employee_evaluation_template = new employee_evaluation_template;
$dbh_employee_evaluation_template->set_where("employee_evaluation_template_id = ?");
$dbh_employee_evaluation_template->stmt_bind_param($employee_evaluation_template_id);

$data = $dbh_employee_evaluation_template->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

