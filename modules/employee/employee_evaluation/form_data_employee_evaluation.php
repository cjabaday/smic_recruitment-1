<?php
require 'components/get_listview_referrer.php';

require 'subclasses/employee_evaluation.php';
$dbh_employee_evaluation = new employee_evaluation;
$dbh_employee_evaluation->set_where("employee_evaluation_id = ?");
$dbh_employee_evaluation->stmt_bind_param($employee_evaluation_id);

$data = $dbh_employee_evaluation->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$evaluation_date);
    if(count($data) == 3)
    {
        $evaluation_date_year = $data[0];
        $evaluation_date_month = $data[1];
        $evaluation_date_day = $data[2];
    }
}

