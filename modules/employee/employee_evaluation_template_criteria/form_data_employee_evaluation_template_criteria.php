<?php
require 'components/get_listview_referrer.php';

require 'subclasses/employee_evaluation_template_criteria.php';
$dbh_employee_evaluation_template_criteria = new employee_evaluation_template_criteria;
$dbh_employee_evaluation_template_criteria->set_where("employee_evaluation_template_criteria_id = ?");
$dbh_employee_evaluation_template_criteria->stmt_bind_param($employee_evaluation_template_criteria_id);

$data = $dbh_employee_evaluation_template_criteria->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

