<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_trainings.php';
$dbh_applicant_trainings = new applicant_trainings;
$dbh_applicant_trainings->set_where("applicant_training_id = ?");
$dbh_applicant_trainings->stmt_bind_param($applicant_training_id);

$data = $dbh_applicant_trainings->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

