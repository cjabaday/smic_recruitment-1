<?php
require_once 'path.php';
init_cobalt('');
?>

<html>

<head>
    <title>SMIC Applicant Portal</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css_applicant_portal/style.css">
</head>

<body onload="">

<?php
require_once 'header_menu_xhr.php';
?>

<div class="bg_home" >

    <div class="slideshow-container" style="width:60%;margin-left:auto;margin-right:auto">


<?php

$dbh = cobalt_load_class('upload_material');
$dbh->set_where('active = "Yes"');
$dbh->stmt_fetch('rowdump');
// debug($dbh->dump);

for($a = 0; $a < $dbh->num_rows; ++$a)
{
    echo "<div class='mySlides fade' >";
    $file_path = "tmp/{$dbh->dump[$a]['file_name']}";
    echo "<img src ='$file_path' style='width:100%;margin-top:20px;min-height:500px;max-height:500px'>";
    echo "</div>";
}
?>

</div>

    <br>
<?php
echo "<div style='text-align:center'>";
for($a = 0; $a < $dbh->num_rows; ++$a)
{

    echo '<span class="dot"></span>';

}
    echo "</div>";
?>

</div>

<?php

require_once 'javascript/slider.php';
