<?php
require_once 'path.php';
init_cobalt('',FALSE);
init_var($search_bar);


// $dbh->set_where('status = ""');
if(isset($_GET['message']))
{
    $error_message = "Please login <a href='applicant_portal.php' style='color:white;font-weight:bold'>here</a>  or register <a href='applicant_registration.php?show_consent=Yes' style='color:white;font-weight:bold'>here</a>  first to apply for a job.";
}



//carreer page apply

if(isset($_GET['apply']))
{
    $message = '<p style="text-align:center">

                Thank you for applying at SM Investments Corporation.<br><br>

                Your application has been forwarded to SMIC HR. You will receive an email notification
                from us once we have reviewed your application.
            </p>';
}
if(isset($_SESSION['logged']))
{
    $link = "detailed_prf.php?id=";
}
else
{
    $link = "carreer_page.php?message=register";
}

$dbh = cobalt_load_class('iprf_staffrequest');



if(isset($_SESSION['logged']))
{
    $dbh->set_fields('*, a.iprf_staffrequest_id as `staffrequest_id`');
    $dbh->set_table('iprf_staffrequest a LEFT JOIN department b on a.department_id = b.department_id
                                 LEFT JOIN company c on a.company_id = c.company_id
                                 LEFT JOIN position d ON a.position_id = d.position_id
                                 LEFT JOIN iprf_staffrequest_applicants e ON a.iprf_staffrequest_id = e.iprf_staffrequest_id');
    $dbh->set_group_by('a.iprf_staffrequest_id');
}
else
{
    $dbh->set_fields('*, a.iprf_staffrequest_id as `staffrequest_id`');
    $dbh->set_table('iprf_staffrequest a LEFT JOIN department b on a.department_id = b.department_id
                                 LEFT JOIN company c on a.company_id = c.company_id
                                 LEFT JOIN position d ON a.position_id = d.position_id');
}
$dbh->stmt_fetch('rowdump');

// debug($dbh->dump);
$job_listing_count = $dbh->num_rows;

$html = new html;

// $html->draw_header();
?>
<html>

<head>
    <title>SMIC Applicant Portal</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css_applicant_portal/style.css">
</head>

<style>

.messageInfo {
    color: #000000;
    border-color: #3366cc;
    background-color: #76aadb;
    background: -webkit-gradient(linear, left top, left bottom, from(#76aadb), to(#5795cf));
    background: -moz-linear-gradient(top,  #76aadb,  #5795cf);
    background: -o-linear-gradient(top,  #76aadb,  #5795cf);
    text-shadow: 1px 1px 1px #99c5ef;

}

.messageInfo {
    padding: 20px;
    border-style: solid;
    border-width: medium;
    border-collapse: collapse;
    border-radius: 10px;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
}

.posting_table th {

    text-align:left;
    border-bottom: 1px solid;
    padding-top:10px;
    padding-bottom:5px;
    color:#1E398d

}

.posting_table td {

    padding-top:5px;
    padding-bottom:5px;

}

.posting_table td {
    font-size:12px;
}

.button_link {
    width:90%;
    border:1px solid;
    text-align:center;
    background-color:#ff3300;
    color:white;
    padding:5px;
}

.button_link:hover {
    text-decoration: none;
    background-color:white;
    color:#ff3300;
}
/*
.row_content:hover {
    cursor:pointer;
} */

</style>

    <?php
    //HEADER
    require_once 'header_menu_xhr.php';


    echo '<form method="POST" action="' . basename($_SERVER['SCRIPT_NAME']) . '">';
    $form_key = generate_token();
    $form_identifier = $_SERVER['SCRIPT_NAME'];
    $_SESSION['cobalt_form_keys'][$form_identifier] = $form_key;
    echo '<input type="hidden" name="form_key" value="' . $form_key .'">';

    require_once 'components/format_date.php';
    ?>

    <div class="bg_home">
        <div class = "space">
            <?php
            init_var($error_message);
            $_SESSION['icon_set'] = 'cobalt';
            $html->display_error($error_message);
            $html->display_info($message);
            ?>
        </div>
        <div class = "containers" style="max-width:60%;padding:10px">
            <div style="float:left;font-size:20px"><strong style="color:#1E398d">Job Postings</strong></div>
            <table border="0" width="100%" cellspacing="1" class="login_text"  style="margin-left:auto;margin-right:auto">
                <tr>
                    <td>
                        <input type="text" name="search_bar" id ="search_bar" size="40" value="<?php echo $search_bar ?>"> &nbsp
                        <input type="submit" name="btn_search" id ="btn_search" value="SEARCH">
                    </td>
                </tr>
            </table>
            <table class ="posting_table" width="100%" >
                <tr>
                    <th>TITLE</th>
                    <th>DEPARTMENT</th>
                    <th>COMPANY</th>
                    <th>DATE POSTED</th>
                    <th>STATUS</th>
                    <?php

                    if(isset($_SESSION['logged']))
                    {
                        $logged_in = TRUE;
                        if(check_link('Recruiter access'))
                        {
                            echo "<th>APPLICANTS</th>";
                        }
                        else
                        {
                            echo "<th>ACTION</th>";

                        }
                    }
                    else
                    {
                        $logged_in = FALSE;
                        echo "<th>ACTION</th>";
                    }



                    ?>

                </tr>

<?php


// if()
if($job_listing_count > 0)
{
    for($a = 0; $a < $job_listing_count; ++$a)
    {


        extract($dbh->dump[$a]);
        // debug($iprf_staffrequest_applicant_id);
        if($a %2 == 0)
        {
            // echo "<tr class='row_content' style='background-color:#f5f5ef' onclick=\"row_link('detailed_prf.php?id=$iprf_staffrequest_id')\">";
            echo '<tr class="row_content" style="background-color:#f5f5ef">';

        }
        else
        {

            // echo "<tr class='row_content' onclick=\"row_link('detailed_prf.php?id=$iprf_staffrequest_id')\">";
            echo '<tr class="row_content" >';


        }

        // debug($link);
        echo "<td>$title</td>
                <td>$name</td>
                <td>$official_name</td>
                <td>".format_date($date_filed)."</td>
                <td>$status</td>
                <td style='text-align:center'>";
                // debug($_SESSION['applicant_id']);
                // debug($iprf_staffrequest_id);

                if($logged_in)
                {
                    if(check_link('Recruiter access'))
                    {
                        $d = cobalt_load_class('iprf_staffrequest_applicants');
                        $d->set_where('iprf_staffrequest_id = ?');
                        $d->stmt_bind_param($staffrequest_id);
                        $d->stmt_fetch();

                        $applicant_count =  $d->num_rows;

                        if($applicant_count > 0)
                        {
                            echo "$applicant_count &nbsp <a href='modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php?iprf_staffrequest_id=$iprf_staffrequest_id' target='_blank'>View</a>";
                        }
                        else
                        {
                            echo "$applicant_count";
                        }
                    }
                    else
                    {
                        // debug($applicant_id);
                        if( $applicant_id == $_SESSION['applicant_id'])
                        {
                            // brpt();


                            echo "Pending Application";
                        }
                        else
                        {
                            echo "<a href='$link$staffrequest_id'><div class='button_link' style=''>Apply</div></a>";
                        }
                    }
                }
                else
                {
                    echo "<a href='$link$staffrequest_id'><div class='button_link' style=''>Apply</div></a>";
                }


                echo "</td>
            </tr>";
    }
}
else
{

    echo "<tr><td colspan = '5' align='center'> No Job Listing Available</a></tr>";
}
?>
            </table>


        </div>
    </div>

</body>
</form>
</html>

<?php

require_once "javascript/rowlink.php";
?>
