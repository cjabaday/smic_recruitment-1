<?php
require 'path.php';
init_cobalt();

$server = new SoapServer(null, ['uri'=>'urn://smic_recruitment']);
$server->addFunction('export_prf');
$server->handle();


function export_prf($arr_prf)
{
    //$arr_prf consists of multiple arrays
    // this method expects array for company, department, position, rank, and classification for set up tables
    // this also expects array for the prf_details

    //check if set up table records are existing
    $d = cobalt_load_class('company');
    if($d->check_uniqueness_label($arr_prf['company'])->is_unique)
    {
        // insert record
        $d->add_company($arr_prf['company']);
    }

    $d = cobalt_load_class('rank');
    if($d->check_uniqueness_label($arr_prf['rank'])->is_unique)
    {
        // insert record
        $d->add_rank($arr_prf['rank']);

    }

    $d = cobalt_load_class('position');
    if($d->check_uniqueness_label($arr_prf['position'])->is_unique)
    {
        // insert record
        $d->add_position($arr_prf['position']);

    }

    $d = cobalt_load_class('classification');
    if($d->check_uniqueness_label($arr_prf['classification'])->is_unique)
    {
        // insert record
        $d->add_classification($arr_prf['classification']);
    }

    $d = cobalt_load_class('position');
    if($d->check_uniqueness_label($arr_prf['position'])->is_unique)
    {
        // insert record
        $d->add_position($arr_prf['position']);
    }

    // print_r($arr_prf);
    $d = cobalt_load_class('iprf_staffrequest');

    if($d->check_uniqueness($arr_prf['prf_details'])->is_unique)
    {
        $d->add_iprf($arr_prf['prf_details']);
        return "/".DEFAULT_DB_HOST."/".BASE_DIRECTORY."/career_page.php";
    }
    else
    {

        return 'duplicate';
    }

}


// //comment out, only for testing
// $arr_company        = array('company_id' => 1, 'official_name' => 'SM Investments Corporation');
// $arr_department     = array('department_id' => 1, 'name' => 'Human Resources');
// $arr_position       = array('position_id' => 1, 'title' => 'Compensation and Benefits');
// $arr_rank           = array('rank_id' => 1, 'name' => 'Rank and File');
// $arr_classification = array('classification_id' => 1, 'name' => 'Rank and File');
// $arr_prf_details = array(
//                     'iprf_staffrequest_id'            => 1,
//                     'PRF_No'                          => 'PRF-2018-00001',
//                     'company_id'                      => 1,
//                     'department_id'                   => 1,
//                     'position_id'                     => 1,
//                     'rank_id'                         => 1,
//                     'classification_id'               => 1,
//                     'status'                          => 'In progress',
//                     'duration'                        => 'Sample Duration Data',
//                     'responsibility'                  => 'Collects and process compensation and benefits of employees',
//                     'general_duties'                  => 'Collects and process compensation and benefits of employees in general',
//                     'detailed_duties'                 => 'Collects and process compensation and benefits of employees in detailed',
//                     'education'                       => "Must have be college graduate",
//                     'professional_eligibility_skills' => "Must clerical and attentive",
//                     'experience'                      => "Nothing in Particular.",
//                     'skills'                          => 'N/A',
//                     'date_needed'                     => '2018-11-01',
//                     'date_filed'                      => '2018-10-20'
// );
//
// //combine all arrays into a single array
//
// $arr_prf = array(
//                     'company'        => $arr_company,
//                     'department'     => $arr_department,
//                     'position'       => $arr_position,
//                     'rank'           => $arr_rank,
//                     'classification' => $arr_classification,
//                     'prf_details'    => $arr_prf_details,
//                 );
//
// export_prf($arr_prf);
