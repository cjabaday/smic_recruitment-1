<?php
require_once 'path.php';
init_cobalt('',FALSE);
init_var($search_bar);
init_var($_POST['btn_apply']);
init_var($_POST['btn_back']);
init_var($search_bar);

$url_param = "";
if(isset($_GET['id']))
{
    $id = $_GET['id'];
    $url_param .= "&id=$id";
}
else
{
    $id = $_POST['id'];
}

if(isset($_SESSION['logged']))
{
    $applicant_id = $_SESSION['applicant_id'];
    $url_param .= "&applicant_id=$applicant_id";
}

$dbh = cobalt_load_class('iprf_staffrequest');
$dbh->set_table('iprf_staffrequest a LEFT JOIN department b on a.department_id = b.department_id
                             LEFT JOIN company c on a.company_id = c.company_id
                             LEFT JOIN position d ON a.position_id = d.position_id
                             LEFT JOIN rank e ON a.rank_id = e.rank_id
                             LEFT JOIN classification f ON a.classification_id = f.classification_id ');
$dbh->set_fields('*, e.name as `rank_name`, f.name as `classification_name`, b.name as `department_name` ');
$dbh->set_where('iprf_staffrequest_id = ?');
$dbh->stmt_bind_param($id);
$dbh->stmt_fetch('single');
extract($dbh->dump);

if($_POST['btn_back'])
{
    redirect('career_page.php');
}

if($_POST['btn_apply'])
{
    $dbh = cobalt_load_class('iprf_staffrequest_applicants');
    $param = ['iprf_staffrequest_id' => $id,
              'applicant_id' => $_SESSION['applicant_id'],
              'date_applied' => date('Y-m-d'),
              'updates'      => 'Pending Application'
            ];
            // debug($param);
    $dbh->add($param);

    require_once 'components/notify_recruiter.php';
    // die();

    redirect("career_page.php?apply=Yes");
}
// debug($dbh->dump);
// $job_listing_count = $dbh->num_rows;

$html = new html;
?>
<html>

<head>
    <title>SMIC Applicant Portal</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css_applicant_portal/style.css">
</head>

<style>

td {
    padding:5px;
    padding-right
}

a {
    text-decoration:none;
    color:black;
}

</style>

<body>

    <?php
    //HEADER
    require_once 'header_menu_xhr.php';


    echo '<form method="POST" action="' . basename($_SERVER['SCRIPT_NAME']) . '">';
    $form_key = generate_token();
    $form_identifier = $_SERVER['SCRIPT_NAME'];
    $_SESSION['cobalt_form_keys'][$form_identifier] = $form_key;
    echo '<input type="hidden" name="form_key" value="' . $form_key .'">';



    $html->draw_hidden('id');
    ?>

    <div class="bg_home">
        <div class = "space">
            <?php
            init_var($error_message);
            $_SESSION['icon_set'] = 'cobalt';
            $html->display_error($error_message);
            ?>
        </div>
        <div class = "containers" style="max-width:60%;padding:10px;padding-bottom:40px">
            <div style="float:left;font-size:20px"><strong><?php echo $PRF_No?></strong></div>
            <br>
            <br>
            <div>
                <table width="100%">
                    <tr><td>Position:</td><td   ><strong><?php echo $title?></strong></td></tr>
                    <tr><td>Company:</td><td><strong><?php echo $official_name?></strong></td> <td>Rank:</td><td><strong><?php echo $rank_name?></strong></td></tr>
                    <tr><td>Department:</td><td><strong><?php echo $department_name?></strong> <td>Classification:</td><td><strong><?php echo $classification_name?></strong></td></td></tr>
                    <tr><td style="border:1px solid grey" colspan="4"><i>General Duties: <br> <?php echo $general_duties?></td><tr>
                    <tr><td style="border:1px solid grey" colspan="4"><i>Detailed Duties:</i> <br> <?php echo $detailed_duties?></td><tr>
                    <tr><td style="border:1px solid grey" colspan="4"><i>Professional Eligibility Skills:</i> <br> <?php echo $professional_eligibility_skills?></td><tr>
                    <tr><td style="border:1px solid grey" colspan="4"><i>Skills:</i> <br> <?php echo $skills?></td><tr>
                    <tr><td style="border:1px solid grey" colspan="4"><i>Education:</i> <br> <?php echo $education?></td><tr>
                    <tr><td style="border:1px solid grey" colspan="4"><i>Experience:</i> <br> <?php echo $experience?></td><tr>
                    <tr><td style="border:1px solid grey" colspan="4"><i>Responsibility:</i> <br> <?php echo $responsibility?></td><tr>
                </table>
            </div>

            <div style="width:50%;text-align:center;float:left;margin-top:10px">
                <?php
                $html->draw_button($type='special', 'submit', 'btn_apply', 'APPLY');

                ?>
            </div>
            <div style="width:50%;text-align:center;float:right;margin-top:10px">
                <?php
                $html->draw_button('back');
                ?>
            </div>
        </div>
    </div>

</body>
</form>
</html>

<?php

require_once "javascript/rowlink.php";
?>
