<?php
require_once "path.php";
init_cobalt();

init_var($_POST['btn_submit']);
init_var($_GET['from_forgot']);
init_var($_GET['token']);
$error_message = "";
if(isset($_GET['token']))
{
    $token = $_GET['token'];
}
else
{
    $token = $_POST['token'];
}

if(isset($_SESSION['logged']) && $_SESSION['logged'] == 'Logged')
{
    redirect('start.php');
}


if($_POST['btn_submit'])
{
    extract($_POST);

    // debug($_POST);

    if($password == $confirm_password)
    {
        //do nothing..
    }
    else
    {
        $error_message = "Password does not match.";
    }



    if($error_message == "")
    {
        //add expiry
        //password expiry time pass this to system settings
        // $request_expiry = 3000;
        //
        // debug(date('i',$request_expiry));

        //get email_info
        $dbh = cobalt_load_class('forgot_pass_table');
        $dbh->set_where('token = ?');
        $dbh->stmt_bind_param($token);
        $dbh->stmt_prepare();
        $dbh->stmt_fetch('single');
        $username = $dbh->dump['email_address'];

        // debug($username);

        //reset password
        require 'password_crypto.php';
        //Hash the password using default Cobalt password hashing technique
        $hashed_password = cobalt_password_hash('NEW',$password, $username, $new_salt, $new_iteration, $new_method);
        $param = array();
        $param['username'] = $username;
        $param['password'] = $hashed_password;
        $param['salt'] = $new_salt;
        $param['iteration'] = $new_iteration;
        $param['method'] = $new_method;

        $dbh = cobalt_load_class('user');
        $dbh->password_reset($param);
        redirect('reset_password.php?from_forgot=No');

    }




    // die();

}

$html = new html;
?>
<html>

<head>
    <title>SMIC Applicant Portal</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css_applicant_portal/style.css">
</head>

<body onload="document.getElementById('password').focus();">

    <?php
    //HEADER
    require_once 'applicant_portal_header.php';


    echo '<form method="POST" action="' . basename($_SERVER['SCRIPT_NAME']) . '">';
    $form_key = generate_token();
    $form_identifier = $_SERVER['SCRIPT_NAME'];
    $_SESSION['cobalt_form_keys'][$form_identifier] = $form_key;
    echo '<input type="hidden" name="form_key" value="' . $form_key .'">';
    echo '<input type="hidden" name="token" value="' . $token .'">';
    ?>

    <div class="bg_home">
        <div class = "space">
            <?php
            init_var($error_message);
            $_SESSION['icon_set'] = 'cobalt';
            $html->display_error($error_message);
            ?>
        </div>
        <div class = "container">
            <strong>Account Recovery</strong>
            <table border="0" width="50%" cellspacing="1" class="login_text" style="margin-left:auto;margin-right:auto">

                <?php
                if($_GET['from_forgot'] == 'Yes')
                {
                    echo "
                    <tr>
                        <td align='center'>
                     <hr>Password reset request has been sent to your email, please log in your email address and follow the instruction.
                     </td>
                 </tr>
                     ";
                }
                elseif($_GET['from_forgot'] == 'No')
                {
                    echo "
                    <tr>
                        <td align='center'>
                     <hr>You successfully reset your password, you can now access your account.
                     <br>
                     <br>
                        Click <a href='applicant_portal.php'>here</a> to return to login page.
                     </td>
                 </tr>
                     ";
                }
                elseif($token != "")
                {
                    ?>
                    <tr>
                        <td align="center">
                            <?php $html->draw_text_field('','password',FALSE,'password',FALSE,'maxlength="' . MAX_PASSWORD_LENGTH . '" size="37" autocomplete="off" placeholder="Password"'); ?>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <?php $html->draw_text_field('','confirm_password',FALSE,'password',FALSE,'maxlength="' . MAX_PASSWORD_LENGTH . '" size="37" autocomplete="off" placeholder="Confirm Password"'); ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>

            <tr>
                <?php
                    if($token != "")
                    {
                        echo '
                        <td align="right">
                            <input type=submit value="SUBMIT" name="btn_submit">
                        </td>

                        ';

                        ?>
                        <hr>
                            <p class="link">Enter your new password.</a></p>
                        <?php
                    }
                ?>

            </tr>
            </table>



        </div>
    </div>

</body>
</form>
</html>
