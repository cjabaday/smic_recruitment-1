<?php
require 'components/get_listview_referrer.php';

require 'subclasses/user_links.php';
$dbh_user_links = new user_links;
$dbh_user_links->set_where("link_id= ?");
$dbh_user_links->stmt_bind_param($link_id);

$data = $dbh_user_links->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}
