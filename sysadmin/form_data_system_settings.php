<?php
require 'components/get_listview_referrer.php';

require 'subclasses/system_settings.php';
$dbh_system_settings = new system_settings;
$dbh_system_settings->set_where("setting= ?");
$dbh_system_settings->stmt_bind_param($setting);

$data = $dbh_system_settings->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}
