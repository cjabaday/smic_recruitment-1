<?php
require 'components/get_listview_referrer.php';

require 'subclasses/system_skins.php';
$dbh_system_skins = new system_skins;
$dbh_system_skins->set_where("skin_id= ?");
$dbh_system_skins->stmt_bind_param($skin_id);

$data = $dbh_system_skins->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}
