<?php
require 'components/get_listview_referrer.php';

require 'subclasses/user_role_links.php';
$dbh_user_role_links = new user_role_links;
$dbh_user_role_links->set_where("role_id= ? AND link_id= ?");
$dbh_user_role_links->stmt_bind_param($role_id);
$dbh_user_role_links->stmt_bind_param($link_id);

$data = $dbh_user_role_links->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}
