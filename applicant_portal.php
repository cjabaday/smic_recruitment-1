<?php
require_once "path.php";
init_cobalt();

if(isset($_SESSION['logged']) && $_SESSION['logged'] == 'Logged')
{
    redirect('start.php');
}

if(isset($_GET['reason']))
{
    if($_GET['reason'] == 'ipchange')
    {
        $error_message = 'You have been logged out because your IP address has changed. Please log in again.';
    }
}

if(xsrf_guard())
{
    init_var($_POST['btnSubmit']);

    if($_POST['btnSubmit'])
    {
        require 'password_crypto.php';

        $error_message = '';
        extract($_POST);

        //Deal with passwords longer than MAX_PASSWORD_LENGTH (possible DoS vulnerability)
        if(strlen($password) > MAX_PASSWORD_LENGTH)
        {
            //Reset password to an arbitrarily small string to thwart DoS attempt
            $password = 'x';
        }

        $data_con = new data_abstraction;
        $mysqli = $data_con->connect_db()->mysqli;
        $clean_password = cobalt_password_hash('RECREATE', $password, $username);
        $data_con->set_table('user a LEFT JOIN applicant b ON a.applicant_id = b.applicant_id');
        $data_con->set_fields('username, skin_id, first_name, last_name, middle_name, a.applicant_id, is_verified, a.applicant_id');
        $data_con->set_where('username = ? AND password = ?');
        $data_con->stmt_bind_param($username);
        $data_con->stmt_bind_param($clean_password);
        $data_con->stmt_prepare();
        $data_con->stmt_fetch('single');

        if($data_con->num_rows > 0)
        {
            extract($data_con->dump);


            $_SESSION['logged']       = 'Logged';
            $_SESSION['user']         = $username;
            $_SESSION['first_name']   = $first_name;
            $_SESSION['middle_name']  = $middle_name;
            $_SESSION['last_name']    = $last_name;
            $_SESSION['applicant_id'] = $applicant_id;
            $_SESSION['ip_address']   = get_ip();



            $data_con = new data_abstraction;
            $data_con->set_fields('skin_name, header, footer, master_css, colors_css, fonts_css, override_css, icon_set');
            $data_con->set_table('system_skins');
            $data_con->set_where("skin_id=?");
            $data_con->stmt_bind_param($skin_id);
            $data_con->stmt_prepare();
            $data_con->stmt_fetch('single');
            if($data_con->num_rows==1)
            {
                extract($data_con->dump);
                $_SESSION['header']       = $header;
                $_SESSION['footer']       = $footer;
                $_SESSION['skin']         = $skin_name;
                $_SESSION['master_css']   = $master_css;
                $_SESSION['colors_css']   = $colors_css;
                $_SESSION['fonts_css']    = $fonts_css;
                $_SESSION['override_css'] = $override_css;
                $_SESSION['icon_set']     = $icon_set;
                if(trim($_SESSION['icon_set'] == ''))
                {
                    $_SESSION['icon_set'] = 'cobalt';
                }
            }
            $data_con->close_db();

            log_action('Logged in');

            //check if user must rehash his password due to updated method or work factor/iterations
            if(cobalt_password_must_rehash($username))
            {
                $hashed_password = cobalt_password_hash('NEW',$password, $username, $new_salt, $new_iteration, $new_method);
                $data_con = new data_abstraction;
                $data_con->set_query_type('UPDATE');
                $data_con->set_table('user');
                $data_con->set_update("`password`=?, `salt`=?, `iteration`=?, `method`=?");
                $data_con->set_where("username=?");
                $data_con->stmt_bind_param($hashed_password);
                $data_con->stmt_bind_param($new_salt);
                $data_con->stmt_bind_param($new_iteration);
                $data_con->stmt_bind_param($new_method);
                $data_con->stmt_bind_param($username);
                $data_con->stmt_prepare();
                $data_con->stmt_execute();
            }



            redirect('start.php');



        }
        else
        {
            $error_message = "Login failed, please check username and password.";
        }

        $data_con->close_db();
    }
}

$html = new html;
?>
<html>

<head>
    <title>SMIC Applicant Portal</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css_applicant_portal/style.css">
</head>

<body onload="document.getElementById('username').focus();">

    <?php
    //HEADER
    require_once 'header_menu_xhr.php';


    echo '<form method="POST" action="' . basename($_SERVER['SCRIPT_NAME']) . '">';
    $form_key = generate_token();
    $form_identifier = $_SERVER['SCRIPT_NAME'];
    $_SESSION['cobalt_form_keys'][$form_identifier] = $form_key;
    echo '<input type="hidden" name="form_key" value="' . $form_key .'">';
    ?>

    <div class="bg_home">
        <div class = "space">
            <?php
            init_var($error_message);
            $_SESSION['icon_set'] = 'cobalt';
            $html->display_error($error_message);
            ?>
        </div>
        <div class = "containers">
            <strong>Please login using your registered account</strong>
            <table border="0" width="50%" cellspacing="1" class="login_text" style="margin-left:auto;margin-right:auto">
            <tr>
                <td align="center">
                <?php $html->draw_text_field('','username',FALSE,'text',FALSE, 'id="username" size="37" autocomplete="off" placeholder="Email Address"'); ?>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <?php $html->draw_text_field('','password',FALSE,'password',FALSE,'maxlength="' . MAX_PASSWORD_LENGTH . '" size="37" autocomplete="off" placeholder="Password"'); ?>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <input type=submit value="LOG IN" name="btnSubmit">
                </td>
            </tr>
            <tr>
                <td align="center"><a href="forgot_password.php">Forgot password</a></td>
            </tr>
            </table>
            <hr>
                <p class="link">Don't have an account? <a href="applicant_registration.php?show_consent=Yes">Sign up Here</a></p>


        </div>
    </div>

</body>
</form>
</html>
