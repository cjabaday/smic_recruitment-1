<?php

class select_record
{
    var $applicant_id = 0;
    var $table_name   = '';

    function __construct()
    {

    }

    function inherit($applicant_id, $table_name)
    {
        $this->applicant_id = $applicant_id;
        $this->table_name   = $table_name;

        return $this;
    }

    function go()
    {
        $d = cobalt_load_class($this->table_name);

        return $d->select_record($this->applicant_id)->dump;
    }
}
