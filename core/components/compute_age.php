<?php
$current_year = date('Y');
$current_month = date('m');
$current_day = date('d');

$year_ob = substr($date_of_birth, 0, 4);
$month_ob = substr($date_of_birth, 5, 2);
$day_ob = substr($date_of_birth, 8, 2);


//Estimated age
$age = $current_year - $year_ob;


//Get exact age
if($current_month < $month_ob)
{
    $age -= 1;
}
elseif($current_month == $month_ob)
{
    if($current_day < $day_ob)
    {
        $age -= 1;
    }
}
