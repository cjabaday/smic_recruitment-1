<?php
require_once 'html_class.php';
$modal = new html;

echo "<div id='locking_overlay' class='throbber_screen_locker' style='min-width:50%'>";
    echo "<div class = 'container' style='width:50%;margin:auto'>";
        echo "<fieldset style='min-width:500px;display: inline-block;border:0;background-color:RGBA(255,255,255,.1);text-align:justify;line-height:2;'>";

            $modal->draw_fieldset_header('Verify your Account');
            $modal->draw_fieldset_body_start();
                //body content starts here
?>
    <p>You are one step away from using the <strong>SMIC Applicant Portal</strong>. We sent a verification to your email, kindly click the verification link to activate your account.<br><br>Thank you!</p>


<?php

                //body content ends here
            $modal->draw_fieldset_body_end();
            $modal->draw_fieldset_footer_start();
            // $modal->draw_button($type='special', $button_class='btn_submit', $button_name='btn_accept', $button_label='AGREE', $draw_table_tags=FALSE, $colspan="2", $extra='');
            $modal->draw_button($type='special', $button_class='btn_back', $button_name='btn_cancel', $button_label='BACK TO LOGIN', $draw_table_tags=FALSE, $colspan="2", $extra='');
        echo "</fieldset>";
    echo "</div>";
echo "</div>";


?>
