<?php
// expects applicant_id

//returns message


// PROTOTYPE
// checks info of the applicant table if the required number of fields filled out is met.
// if not, some modules will not be accessible


//fetch all fields
$dbh = cobalt_load_class("applicant");
$dbh->set_fields('image, last_name, first_name,
                  first_name, middle_name, nickname,
                  gender, civil_status, citizenship,
                  date_of_birth, height, weight,
                  blood_type, birth_place, religion,
                  present_address_line_1, present_address_line_2, present_address_barangay,
                  present_address_city_municipality_id, present_address_province_id, provincial_address_contact_number,
                  provincial_address_line_1, provincial_address_line_2, provincial_address_barangay,
                  provincial_address_city_municipality_id, provincial_address_province_id, contact_number,
                  sss_number, tin, philhealth_id_number,
                  hdmf_number, umid_number, religion,
                  contact_name, contact_relationship, contact_address,
                  contact_contact_number
');
$dbh->set_where('applicant_id = ?');
$dbh->stmt_bind_param($applicant_id);
$dbh->stmt_prepare();
$arr_app = $dbh->stmt_fetch('single')->dump;

$total_field = 0;
$blank_field = 0;
//check if it has values and count blank fields
foreach($arr_app as $key => $value)
{
    if($value == "")
    {
        ++$blank_field;
    }
    ++$total_field;
}
// debug($total_field);
// debug($blank_field);



// get percentage of filled out; There must be a setting for allowed percentage

// show error message
