<?php
//get table_name from dd
$table = 'applicant';

//get row count of object's table
$object = cobalt_load_class($table);
$object->exec_fetch();
$row_count = $object->num_rows;

// //get transaction prefix
// require 'get_ref_type.php';
$transaction_prefix = 'APL';

//assemble transaction column name
$transaction_column_name = $table . '_number';

//assemble transaction number
$date_year_month = date('ym');
$counter = str_pad($row_count+1, 5, '0', STR_PAD_LEFT);

//pass this to gen
$applicant_number = "$transaction_prefix-$date_year_month-$counter";
