<?php
class check_applicant_child_tables
{
    var $applicant_id = 0;
    var $arr_applicant_child_table = array("applicant_family_members", "applicant_languages_proficiency",
                                           "applicant_license", "applicant_previous_employers",
                                           "applicant_reference", "applicant_school_attended",
                                           "applicant_skills", "applicant_trainings");


    function __construct()
    {

    }

    function inherit($id)
    {
        $this->applicant_id = $id;

        return $this;
    }

    function go()
    {

        //check if applicant have records in the following tables
        $string = "";
        foreach($this->arr_applicant_child_table as $value)
        {
            $dbh = cobalt_load_class($value);
            $dbh->set_where('applicant_id = ?');
            $dbh->stmt_bind_param($this->applicant_id);
            $dbh->stmt_prepare();
            $dbh->stmt_fetch();
            if($dbh->num_rows < 1)
            {

                $value = str_replace("_"," ",$value);
                $value = ucwords($value);
                $where_value = "Add $value";
                $value = substr($value, 10);

                $d = cobalt_load_class("user_links");
                $d->set_where("descriptive_title = ?");
                $d->stmt_bind_param($where_value);
                $d->stmt_prepare();
                $link = $d->stmt_fetch('single')->dump['target'];

                $string .= "<a href='$link'>$value</a><br>";

            }

        }
        return $string;
    }
}
