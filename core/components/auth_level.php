<?php
//Requires: $auth_level_args - array containing the rest of the required and optional parameters
//Requires: $auth_level_user - the username that is being accessed for an update operation
//Requires: $redirect_page - if authentication fails, redirect user to this page
//Optional: $pass_username - TRUE/FALSE; if TRUE, this means pass the username to the $redirect_page as a GET parameter
//Optional: $username_var - if $pass_username is TRUE, this will hold the name of the GET parameter to hold the username to be passed. If empty, default value is 'username'
//Optional: $pass_message - holds the message to be passed to $redirect_page as information to the user, usually to indicate why she was redirected. If empty, default value is 'You have been redirected due to insufficient user level'

if(is_array($auth_level_args))
{
    extract($auth_level_args);

    if(isset($auth_level_user))
    {
        //If no username is passed, but $auth_level_args|user are initialized, no need for a hard error
        if(strlen($auth_level_user) > 0)
        {
            require_once 'subclasses/user.php';
            $dbh_user = new user;
            $user_level = $dbh_user->check_level($auth_level_user)->dump['user_level']; //user_level of the user being modified
            $self_level = $dbh_user->check_level($_SESSION['user'])->dump['user_level'];

            if($self_level >= $user_level)
            {
                //Allow user to edit this record
            }
            else
            {
                //User must not edit, redirect or show message or both
                //FIXME: When GET message passing infra is done (separate patch), apply it here
                $redirect_string = $redirect_page . '?';
                if($pass_username === TRUE)
                {
                    init_var($username_var, 'username');
                    init_var($pass_message, 'You have been redirected due to insufficient user level');
                    $username_var = rawurlencode($username_var);
                    $pass_message = rawurlencode($pass_message);

                    $redirect_string .= $username_var . '=' . $auth_level_user . '&';
                }
                $redirect_string .= 'message=' . $pass_message;
                //This is so we can authenticate message validity, else the form will not display our message (blocked by Cobalt)
                $GET_key = rawurlencode(enable_GET_message_auth());
                $redirect_string .= '&message_key=' . $GET_key;

                redirect($redirect_string);
            }
        }
    }
    else
    {
        error_handler('Component startup error.','This component must have $auth_level_user initialized in order to function.');
    }
}
else
{
    error_handler('Component startup error.', 'This component must have the $auth_level_args array defined before starting.');
}
