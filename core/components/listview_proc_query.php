<?php
require_once 'subclasses/' . $db_subclass . '.php';
$data_con = new $db_subclass;
if(empty($join_clause))
{
    $join_clause = $data_con->get_join_clause()->join_clause;
}
$data_con->set_table($join_clause);
$data_con->set_fields($lst_fields);
if(isset($where_clause))
{
    $data_con->set_where($where_clause);
}

if($filter != '')
{
    if($data_con->where_clause != '')
    {
        $data_con->where_clause .= " AND ";
    }


    if(in_array($filter_field, $filter_watchlist_both))
    {
        $temp_filter = '%'.$filter.'%';
    }
    elseif(in_array($filter_field, $filter_watchlist_left))
    {
        $temp_filter = '%'.$filter;
    }
    elseif(in_array($filter_field, $filter_watchlist_right))
    {
        $temp_filter = $filter.'%';
    }
    elseif(in_array($filter_field, $filter_watchlist_none))
    {
        $temp_filter = $filter;
    }
    else
    {
        switch($filter_wildcard_usage)
        {
            case 'both':  $temp_filter = '%'.$filter.'%'; break;
            case 'right': $temp_filter = $filter.'%'; break;
            case 'left':  $temp_filter = '%'.$filter; break;
            case 'none':  $temp_filter = $filter; break;
            default:      error_handler("Invalid filter wildcard.", "Tried to use '$filter_wildcard_usage'; acceptable values are: 'both', 'left', 'right', and 'none'.");
        }
    }

    if($filter_field == '')
    {
        //No field chosen, try to find match in all fields
        //Compatibility note: $field_to_match here will need proper escaping if it ends up being a reserved word,
        //          and both the escaping style and list of reserved words vary per database product. This is not
        //          yet safely compatible
        $data = explode(',', $lst_filter_fields);
        $num_fields = count($data);
        for($a=0; $a<$num_fields; ++$a)
        {
            $field_to_match = trim($data[$a]);

            if($a==0)
            {
                $data_con->where_clause .= '(';
            }
            else
            {
                $data_con->where_clause .= ' OR ';
            }

            $data_con->where_clause .= "$field_to_match LIKE ?";
            $data_con->stmt_bind_param($temp_filter);

        }
        $data_con->where_clause .= ')';
    }
    else
    {
        //Check if filter_field is valid
        if(is_in_whitelist(array('val'=>$filter_field, 'list'=>$lst_filter_fields)))
        {
            $data_con->where_clause .= "($filter_field LIKE ?)";
            $data_con->stmt_bind_param($temp_filter);
        }
        else
        {
            //Invalid filter_field
            //Do nothing
        }
    }
}
else
{
    $filter_field     = '';
    $enc_filter_field = '';
}

if($filter_sort_asc != '' || $filter_sort_desc != '')
{
    $filter_string = '';
    $sort_order    = '';
    if($filter_sort_asc != '')
    {
        $key = $filter_sort_asc;
        $sort_order = 'ASC';
    }
    else
    {
        $key = $filter_sort_desc;
        $sort_order = 'DESC';
    }

    //Security note (2017-06-08): This looks safe enough, $filter_sort_asc/desc are not used directly,
    //and are checked inside a non-tamperable array ($arr_fields) for validity. Invalid values are ignored.
    if(isset($arr_fields[$key]) && is_array($arr_fields[$key]))
    {
        foreach($arr_fields[$key] as $filter_sort_field)
        {
            make_list($filter_string, $filter_sort_field . ' ' . $sort_order, ', ', FALSE);
        }
    }
    elseif(isset($arr_fields[$key]))
    {
        make_list($filter_string, $arr_fields[$key] . ' ' . $sort_order, ', ', FALSE);
    }
    else
    {
        //invalid field key, ignore
    }

    if($filter_string != '')
    {
        $data_con->set_order($filter_string);
    }
}
else
{
    if(isset($default_sort_order))
    {
        $data_con->set_order($default_sort_order);
    }
}

$data_con->stmt_prepare();
$data_con->stmt_execute();
$total_records = $data_con->num_rows;

require_once 'paged_result_class.php';
$pager = new paged_result($total_records, $results_per_page);
$pager->get_page_data($result_pager, $current_page);
$current_page = $pager->current_page;
$data_con->set_limit($pager->offset, $pager->records_per_page);

$data_con->stmt_template = ''; //clear for another statement to be prepared, this time with the proper limits.
$data_con->stmt_prepare();
$data_con->stmt_fetch('rowdump');
$num_page_records = $data_con->num_rows;

if(DEBUG_MODE && isset($print_settings) && $print_settings == TRUE)
{
    echo '<pre>';
    echo '$join_clause = \'' . $join_clause . '\';<br>';
    echo '$where_clause = "' . $where_clause . '";<br>';
    echo '$lst_fields = \'' . $lst_fields . '\';<br>';
    echo '$arr_fields = ';
    array_to_source($arr_fields, FALSE, 20, 6, 'custom');
    echo '<br>';
    echo '$arr_field_labels = ';
    array_to_source($arr_field_labels, FALSE, 26, 6, 'custom');
    echo '<br>';
    echo '$lst_filter_fields = \'' . $lst_filter_fields . '\';<br>';
    echo '$arr_filter_field_labels = ';
    array_to_source($arr_filter_field_labels, FALSE, 33, 6, 'custom');
    echo '<br>';
    echo '$arr_subtext_separators = ';
    array_to_source($arr_subtext_separators, FALSE, 32, 6, 'custom');
    echo '<br>';
    echo '</pre>';
}
