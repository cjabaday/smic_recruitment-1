<?php
function init_cobalt($required_passport=null, $log=TRUE)
{
    //Start the performance timer
    $start = microtime(TRUE);
    define('PROCESS_START_TIME', $start);

    //Load the global config file and any other class or library files you want to be autoloaded at every page.
    require 'global_config.php';
    require 'data_abstraction_class.php';
    require 'html_class.php';

    if(DEBUG_MODE)
    {
        require_once 'core_debug.php';
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
    }

    //Set timezone as specified in load
    date_default_timezone_set(TIMEZONE_SETTING);

    //Start session. Prevent simple session fixation attacks by regenerating session ID when it is first set.
    session_name(GLOBAL_SESSION_NAME);
    session_start();
    if(!isset($_SESSION['initiated']))
    {
        //To mitigate session prediction attacks, ensure entropy length is at leats 16 bytes (128 bits)
        //and the hash function is SHA256 if supported, else SHA1.
        $sess_entropy_length = ini_get('session.entropy_length');
        if($sess_entropy_length < 16)
        {
            ini_set('session.entropy_length',16);
        }
        if(in_array('sha256', hash_algos()))
        {
            ini_set('session.hash_function','sha256');
        }
        else
        {
            ini_set('session.hash_function',1);
        }

        session_regenerate_id(TRUE);
        $_SESSION['initiated'] = TRUE;
    }

    //Default database link - for use with quote_smart()
    //and any other functions that rely on MySQL functions
    //which rely on a valid database link being opened at one point.
    global $default_db_link;
    $dbh = new data_abstraction;
    $default_db_link = $dbh->connect_db()->mysqli;

    if($required_passport!=null)
    {
        //Check if logged; if not, redirect to login page defined by global_config.php.
        if(!isset($_SESSION['logged']) || $_SESSION['logged'] != "Logged")
        {
            redirect(LOGIN_PAGE);
        }
        elseif($_SESSION['ip_address'] != get_ip())
        {
            if(IP_CHANGE_DETECTION)
            {
                //If IP changes, log user out to prevent potential session hijacks.
                log_action('Logged out due to IP address change, from ' . $_SESSION['ip_address'] . ' to ' . get_ip());
                $_SESSION = array();
                if(isset($_COOKIE[session_name()]))
                {
                    setcookie (session_name(), "", time() - 86400);
                }
                session_destroy();
                redirect(LOGIN_PAGE . '?reason=ipchange');
            }
        }

        if($required_passport != 'ALLOW_ALL') check_passport($required_passport);
    }

    //If magic_quotes_gpc is enabled in the server, we have to "clean" the POST data so
    //we always make use of 'virgin' input. This way, all other methods can rely on the fact
    //that all input data will be unescaped when they receive it.
    //OPTIMIZATION TIP: If you can set magic qoutes off in php.ini, do so. This will save processing time.
    if(get_magic_quotes_gpc())
    {
        reverse_magic_quotes($_POST);
    }

    mb_internal_encoding(MULTI_BYTE_ENCODING);

    //Initialize these two variables, they're practically in every page
    global $message;
    global $message_type;
    $message ='';
    $message_type='';

    if($log && LOG_MODULE_ACCESS)
    {
        if(empty($_POST['form_key'])) log_action('Module Access');
    }
}

function check_passport($required_passport)
{
    //Check if '$required_passport' is in the user's passport settings.
    //Not finding it here would mean an illegal access attempt.
    //Similarly, if we find that the module status of '$required_passport' is set to "Off",
    //it also constitutes an illegal access attempt, because modules that are turned off
    //are not displayed in the control center.
    $d = new data_abstraction;
    $d->set_fields('a.status');
    $d->set_table('user_links a LEFT JOIN user_passport b ON a.link_id = b.link_id');
    $d->set_where("a.name = ? AND b.username = ? AND a.status = 'On'");
    $d->stmt_bind_param($required_passport);
    $d->stmt_bind_param($_SESSION['user']);
    $d->stmt_prepare();
    $data = $d->stmt_fetch('single')->dump;
    $numrows = $d->num_rows;
    $d->close_db();

    if($numrows==0)
    {
        //Verify that the required passport actually exists
        $d = new data_abstraction;
        $d->set_fields('link_id');
        $d->set_table('user_links');
        $d->set_where("name = ?");
        $d->stmt_bind_param($required_passport);
        $d->stmt_prepare();
        $d->stmt_fetch('single');
        $numrows = $d->num_rows;
        $d->close_db();
        if($numrows==1)
        {
            log_action("ILLEGAL ACCESS ATTEMPT - Tried to access '$_SERVER[PHP_SELF]' without sufficient privileges.", $_SERVER['PHP_SELF']);

            //Get the security level. Security level setting determines what to do in a detected illegal access attept.
            $d = new data_abstraction;
            $d->set_fields('value');
            $d->set_table('system_settings');
            $d->set_where("setting='Security Level'");
            $d->stmt_prepare();
            $data = $d->stmt_fetch('single')->dump;
            $numrows = $d->num_rows;
            if($numrows == 1)
            {
                $security_level = $data['value'];
            }
            else error_handler("Error getting the security level, possible misconfiguration in system settings. ",  $d->error);
            $d->close_db();

            if(strtoupper($security_level)=="HIGH")
            {
                $enable_red_alert=true;
                require 'components/red_alert_screen.php';
                die();
            }
            else
            {
                redirect(HOME_PAGE);
            }
        }
        else
        {
            error_handler("Passport tag does not exist in module list!", 'Passport tag: "' . $required_passport . '"');
        }
    }
}

function check_link($link, $user='')
{
    if($user=='') $user = $_SESSION['user'];
    $in_passport=FALSE;

    $d = new data_abstraction;
    $d->set_fields('a.status');
    $d->set_table('user_links a LEFT JOIN user_passport b ON a.link_id = b.link_id');
    $d->set_where("a.name = ? AND b.username = ? AND a.status = 'On'");
    $d->stmt_bind_param($link);
    $d->stmt_bind_param($user);
    $d->stmt_prepare();
    $d->stmt_fetch('single');
    $numrows = $d->num_rows;
    if ($numrows==1) $in_passport=TRUE;

    return $in_passport;
}

function log_action($action, $module='')
{
    if(isset($_SESSION['user']))
    {
        $username = $_SESSION['user'];
    }
    else
    {
        $username = 'Not Logged In';
    }

    if($module=='')
    {
        $module = $_SERVER['SCRIPT_NAME'];
    }

    $datetime = date('Y-m-d H:i:s');
    $ip_address = get_ip();

    $d = new data_abstraction;
    $d->set_query_type('INSERT');
    $d->set_table('system_log');
    $d->set_fields('ip_address, user, datetime, action, module');
    $d->set_values("?, ?, ?, ?, ?");
    $d->stmt_bind_param($ip_address);
    $d->stmt_bind_param($username);
    $d->stmt_bind_param($datetime);
    $d->stmt_bind_param($action);
    $d->stmt_bind_param($module);
    $d->stmt_prepare();
    $d->stmt_execute(FALSE);
}

function get_ip()
{
    $ip_address = '';
    if(isset($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    elseif(isset($_SERVER['HTTP_X_FORWARDED']))
    {
        $ip_address = $_SERVER['HTTP_X_FORWARDED'];
    }
    elseif(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
    {
        $ip_address = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    }
    elseif(isset($_SERVER['HTTP_FORWARDED_FOR']))
    {
        $ip_address = $_SERVER['HTTP_FORWARDED_FOR'];
    }
    elseif(isset($_SERVER['HTTP_FORWARDED']))
    {
        $ip_address = $_SERVER['HTTP_FORWARDED'];
    }

    if($ip_address == '')
    {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }
    else
    {
        $ip_address .= ' : ' . $_SERVER['REMOTE_ADDR'];
    }

    return $ip_address;
}


function make_list(&$list_var, $new_entry, $delimiter=',', $quotes=TRUE, $quote_string_start="'", $quote_string_end="")
{
    if($list_var != '') $list_var .= $delimiter;

    if($quotes==TRUE)
    {
        if($quote_string_end=='') $quote_string_end = $quote_string_start;
        $list_var .= $quote_string_start . $new_entry . $quote_string_end;
    }
    else $list_var .= "$new_entry";
}

function make_list_array(&$array, $new_entry)
{
    if(!is_array($array)) $array = array();
    if(!in_array($new_entry, $array)) $array[] = $new_entry;
}

function back_quote_smart($var)
{
    if(substr($var,0,1) != '`')
    {
        $var = '`' . $var . '`';
    }

    return $var;
}

function cobalt_htmlentities($unclean, $flag=ENT_QUOTES)
{
    $clean = htmlspecialchars($unclean, $flag, MULTI_BYTE_ENCODING);
    return $clean;
}

function cobalt_htmlentities_decode($unclean, $flag=ENT_QUOTES)
{
    $clean = htmlspecialchars_decode($unclean, $flag);
    return $clean;
}

function cobalt_load_generic($class_name, $class_file='', $subdirectory='', $directory = 'subclasses')
{
    if($class_file == '')
    {
        $class_file = $class_name;
    }

    if($subdirectory== '')
    {
        $subdirectory = '';
    }
    else
    {
        $subdirectory = $subdirectory . '/';
    }

    require_once $directory . '/' . $subdirectory . $class_file . '.php';
    return new $class_name;
}

function cobalt_load_class($class_name, $class_file='', $subdirectory='')
{
    return cobalt_load_generic($class_name, $class_file, $subdirectory, 'subclasses');
}

function cobalt_load_component($class_name, $class_file='', $subdirectory='')
{
    return cobalt_load_generic($class_name, $class_file, $subdirectory, 'components');
}

function error_handler($generic_message, $debugging_message='')
{
    $error_message = $generic_message;
    if(DEBUG_MODE)
    {
        brpt();
        $error_message .= ' ' . $debugging_message;
    }
    die('An error occured: ' . $error_message);
}

function generate_token($length=16, $output_type='base64', &$crypto_secure=FALSE)
{
    if($length < 1)
    {
        $length = 16; //resulting length if purposely skipped by dev to settle on whatever is default
    }

    $token='';
    if(function_exists('random_bytes'))
    {
        $token = random_bytes($length);
        $crypto_secure = TRUE; //random_bytes() guarantees a cryptographically-secure source
    }
    elseif(is_readable('/dev/urandom'))
    {
        $token = fread(fopen('/dev/urandom','rb'),$length);
        $crypto_secure = TRUE; //output of /dev/urandom is good for crypto purposes
    }
    elseif(function_exists('openssl_random_pseudo_bytes'))
    {
        $token = openssl_random_pseudo_bytes($length, $crypto_secure);
    }
    else
    {
        error_handler('Token generation failed.','No supported CSPRNG found.');
    }

    if($crypto_secure === FALSE)
    {
        error_handler('Token generation failed.','Unable to produce crypto-secure token');
    }

    $output_type = strtolower($output_type);
    switch($output_type)
    {
        case 'base64': $token = str_replace('=', '', base64_encode($token));
                        //any '=' padding is removed, useless for tokens because they do not contribute to randomness, and
                        //only increase the size unnecessarily (e.g., for database storage as GUID or as salt for hash functions)
                       break;

        case 'fs'    : //Make the token safe for use as filesystem name tokens.
                       //sha1 is good here because it makes it fixed length (necessary for prepending tokens to uploaded files)
                       //plus makes sure we do not have any characters that are not safe for use in filenames in different platforms
                       $token = sha1($token);
                       break;

        case 'raw'   : break;

        default      : error_handler('Token generation failed.', 'Invalid output type specified for token');
    }

    return $token;
}

function token_salt()
{
    return generate_token(16);
}

function token_iv()
{
    return generate_token(32);
}

function token_fs()
{
    return generate_token(16,'fs');
}

function token_bytes($length=16)
{
    return generate_token($length, 'raw');
}

function init_var(&$var, $initialized_value='')
{
    if(empty($var) && $var != '0')
    {
        $var = $initialized_value;
    }
}

function quote_smart($unclean)
{
    global $default_db_link;
    if(get_magic_quotes_gpc())
    {
        $unclean = stripslashes($unclean);
    }
    $clean = mysqli_real_escape_string($default_db_link, $unclean);
    return $clean;
}

function quote_smart_recursive(&$var)
{
    if(is_array($var))
    {
        foreach($var as $key=>$new_var)
        {
            quote_smart_recursive($new_var);
        }
    }
    else
    {
        $var = mysql_real_escape_string($var);
    }
}

function redirect($location, $http_status_code=303)
{
    //** SST Injection ***//
    if(isset($_SESSION['sst']) && $_SESSION['sst']['enabled'] == TRUE)
    {
        $injector = $_SESSION['sst']['tasks'][0]['post'];
        if($injector != '')
        {
            require FULLPATH_BASE . 'sst/post/' . $injector;
        }

        //After SST post script runs (if any), remove the current entry then go to next area.
        array_shift($_SESSION['sst']['tasks']); //removes index 0, which just wrapped up a while ago

        if(count($_SESSION['sst']['tasks']) > 0)
        {
            //More tasks to do, get to it
            $location = '/' . BASE_DIRECTORY . '/' . $_SESSION['sst']['tasks'][0][0];
        }
        else
        {
            //No more tasks to do, end SST.
            unset($_SESSION['sst']);

            //Return to SST listview page
            $location = '/' . BASE_DIRECTORY . '/sst/listview_cobalt_sst.php';
        }
    }

    header('location: ' . $location, true, $http_status_code);
    exit();
}

function reverse_magic_quotes(&$var)
{
    if(is_array($var))
    {
        foreach($var as $key=>$new_var)
        {
            reverse_magic_quotes($var[$key]);
        }
    }
    else
    {
        $var = stripslashes($var);
    }
}

function strip_back_quote_smart($var)
{
    if(substr($var,0,1) == '`')
    {
        $var = substr($var, 1, -1);
    }

    return $var;
}

function enable_GET_message_auth()
{
    $GET_key = generate_token();
    $_SESSION['GET_message_key'] = $GET_key;
    return $GET_key;
}

function GET_message_auth($GET_key)
{
    //strlen here is a sanity check, to make sure we don't get into cases where FALSE === FALSE is compared and as a result is evaluated as TRUE.
    if(isset($_SESSION['GET_message_key']) && rawurldecode($GET_key) === $_SESSION['GET_message_key'] && strlen($GET_key) > 10)
    {
        //Message passed authentication, must be legit.
        //Revoke key, as it should be one-time use only.
        unset($_SESSION['GET_message_key']);
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function enable_xsrf_guard()
{
    $form_key = generate_token();
    $form_identifier = $_SERVER['PHP_SELF'];
    $_SESSION['cobalt_form_keys'][$form_identifier] = $form_key;
    echo '<input type="hidden" name="form_key" value="' . $form_key .'">' . "\r\n";

    //We don't want to accumulate an unlimited amount of Cobalt Form Keys, so once we exceed limit, we remove the oldest one.
    if(count($_SESSION['cobalt_form_keys']) > MAX_FORM_KEYS)
    {
        array_shift($_SESSION['cobalt_form_keys']);
    }
}

function xsrf_guard()
{
    $xsrf_passed = FALSE;
    $session_token_exists = FALSE;
    $form_key_validated = FALSE;

    if(isset($_SESSION['cobalt_form_keys'][$_SERVER['SCRIPT_NAME']]))
    {
        $session_token_exists = TRUE;

        if(isset($_POST['form_key']) && isset($_SESSION['cobalt_form_keys']))
        {
            if($_POST['form_key'] === $_SESSION['cobalt_form_keys'][$_SERVER['SCRIPT_NAME']])
            {
                $form_key_validated = TRUE;
            }
        }
    }

    if($session_token_exists && $form_key_validated)
    {
        $xsrf_passed = TRUE;
    }

    return $xsrf_passed;
}

function is_in_whitelist($settings)
{
    //****************************
    //Required parameters checking
    if(array_key_exists('val', $settings) && array_key_exists('list', $settings))
    {
        $value_to_check = $settings['val'];
        $whitelist = $settings['list'];
    }
    else
    {
        error_handler("Missing required parameters for is_in_whitelist().", "is_in_whitelist() must have 'val' and 'list' indexes in its parameter array.");
    }
    //End of required parameters checking and initialization
    //******************************************************

    //*******************
    //Optional parameters
    $separator = ',';
    $delimiter = "";
    $trim = FALSE;
    if(array_key_exists('separator', $settings))
    {
        $separator = $settings['separator'];
    }

    if(array_key_exists('delimiter', $settings))
    {
        $delimiter = $settings['delimiter'];
    }

    if(array_key_exists('trim', $settings))
    {
        $trim = $settings['trim'];
    }
    //End of optional parameters initialization
    //******************************************

    //Create whitelist array if not already an array
    if(is_array($whitelist))
    {
        //Do nothing
    }
    else
    {
        //Convert to array
        $whitelist = explode($separator, $whitelist);

        if($delimiter || $trim)
        {
            foreach($whitelist as $key=>$value)
            {
                if($delimiter)
                {
                    $length = strlen($value);
                    if($value[0] == $delimiter &&
                       $value[$length-1] == $delimiter)
                    {
                        $whitelist[$key] = substr($value, 1, $length-2);
                    }
                }

                if($trim)
                {
                    $whitelist[$key] = trim($whitelist[$key]);
                }
            }
        }
    }

    return in_array($value_to_check, $whitelist);
}
