<?php
require_once 'applicant_dd.php';
class applicant_html extends html
{
    function __construct()
    {
        $this->fields        = applicant_dd::load_dictionary();
        $this->relations     = applicant_dd::load_relationships();
        $this->subclasses    = applicant_dd::load_subclass_info();
        $this->table_name    = applicant_dd::$table_name;
        $this->readable_name = applicant_dd::$readable_name;

        $this->fieldsets = array(
                                 'default'=>array(
                                             'applicant_id',
                                             'current_application_status',
                                             'company_id',
                                             'source',
                                             'employee_id',
                                             'personnel_requisition_id',
                                             'application_date',
                                             'application_status'),
                                 'Basic Information' => array(
                                                     'applicant_number',
                                                     'image',
                                                     'last_name',
                                                     'first_name',
                                                     'middle_name',
                                                     'nickname',
                                                     'email_address',
                                                     'gender',
                                                     'civil_status',
                                                     'citizenship',
                                                     'date_of_birth',
                                                     'height',
                                                     'weight',
                                                     'blood_type',
                                                     'birth_place',
                                                     'religion'),
                                 'Address and Contact Details' => array(
                                                                 'present_address_province_id',
                                                                 'present_address_city_municipality_id',
                                                                 'present_address_line_1',
                                                                 'present_address_line_2',
                                                                 'present_address_barangay',
                                                                 'contact_number'),
                                 'Permanent Address' => array(
                                     'provincial_address_province_id',
                                     'provincial_address_city_municipality_id',
                                     'provincial_address_line_1',
                                     'provincial_address_line_2',
                                 'provincial_address_barangay',
                                     'provincial_address_contact_number',
                                 ),
                                 'Government IDs' => array('sss_number',
                                                          'tin',
                                                          'philhealth_id_number',
                                                          'hdmf_number',
                                                          'umid_number'),
                                 'Emergency Contact Person' => array(
                                                         'contact_name',
                                                         'contact_relationship',
                                                         'contact_address',
                                                         'contact_contact_number',)
                                );
    }

    function draw_applicant_details($arr_additional_applicant_info)
    {

        foreach($arr_additional_applicant_info as $value)
        {
            extract($value);
            $$field_name = $field_value;
        }
        ?>
        <style>
            td.label {
                /* color:blue; */
            }

        </style>
        <?php
        echo '<fieldset class="container">';
        menuGroupWindowHeader('Personal Info','../login_badge.png');
        echo "<div>";
            echo '<div class="container_head" style="display:block;width:100%;margin-left:20px">';

                echo "<div style='float:left;margin-right:20px;margin-bottom:10px'>";

                    if($image == "")
                    {
                        // do nothing..
                    }
                    else
                    {
                        echo '<img src="/' . BASE_DIRECTORY .'/tmp/'. urlencode($image) . '" style="max-height: 200px; max-width: 200px">';

                    }

                echo '</div>';
                echo "<div style='float:left'>";
                    echo "<span class='name_class'>$full_name</span>";
                    echo '<br><span style="font-size:18px"><strong>'.$personal_email.'</strong></span><br>';
                    if(check_link('Edit applicant'))
                    {
                        echo "<br><a href='modules/applicant/applicant/edit_applicant.php?applicant_id=$applicant_id&applicant=Yes'>Edit</a><br>";
                    }

                echo '</div>';



            echo '</div>';

            echo '<div class="container_content" style="clear:both">';


                echo "<div style='width:100%'>";
                    echo '<fieldset><legend>Basic Information</legend>';
                        echo "<div style='float:left;width:55%'>";
                            echo '<table>';
                                $this->draw_field('nickname');
                                $this->draw_field('gender');
                                // $this->draw_field('date_of_birth');
                                echo "<tr><td class='label'> Date of Birth:</td><td><p class='detail_view'>$string_date</p></td></tr>";
                                echo "<tr><td class='label'> Age:</td><td><p class='detail_view'>$age</p></td></tr>";
                                $this->draw_field('birth_place');
                                $this->draw_field('civil_status');
                            echo '</table>';
                        echo '</div>';
                        echo "<div style='float:left;width:40%'>";
                            echo '<table>';
                                $this->draw_field('citizenship');
                                $this->draw_field('religion');
                                $this->draw_field('height');
                                $this->draw_field('weight');
                                $this->draw_field('blood_type');
                            echo '</table>';
                            echo '</div>';


                    echo '</fieldset>';
                echo '</div>';

                echo "<div style='float:left;width:48%;'>";
                    echo '<fieldset><legend>Present Address and Contact Details</legend>';
                        echo '<table class="table3">';
                            $this->draw_field('present_address_line_1');
                            $this->draw_field('present_address_line_2');
                            $this->draw_field('present_address_barangay');
                            $this->draw_field('present_address_city_municipality_id');
                            $this->draw_field('present_address_province_id');
                            $this->draw_field('contact_number');

                        echo '</table>';
                    echo '</fieldset>';
                echo '</div>';

                echo "<div style='float:right;width:48%;'>";
                    echo '<fieldset><legend>Permanent Address and Contact Details</legend>';
                        echo '<table>';
                            $this->draw_field('provincial_address_line_1');
                            $this->draw_field('provincial_address_line_2');
                            $this->draw_field('provincial_address_barangay');
                            $this->draw_field('provincial_address_city_municipality_id');
                            $this->draw_field('provincial_address_province_id');
                            $this->draw_field('provincial_address_contact_number');


                        echo '</table>';
                    echo '</fieldset>';
                echo '</div>';

            echo '</div>';

            echo '<div class="container_content" style="clear:both">';
                echo "<div style='float:left;width:48%'>";
                    echo '<fieldset><legend>Government IDs</legend>';
                        echo '<table>';
                            $this->draw_field('sss_number');
                            $this->draw_field('tin');
                            $this->draw_field('philhealth_id_number');
                            $this->draw_field('hdmf_number');
                            $this->draw_field('umid_number');
                        echo '</table>';
                    echo '</fieldset>';
                echo '</div>';

                echo "<div style='float:right;width:48%;'>";
                    echo '<fieldset><legend>Emergency Contact Person</legend>';
                        echo '<table>';
                            $this->draw_field('contact_name');
                            $this->draw_field('contact_relationship');
                            $this->draw_field('contact_address');
                            $this->draw_field('contact_contact_number');
                        echo '</table>';
                    echo '</fieldset>';
                echo '</div>';

            echo '</div>';
        echo '</div>';
        echo '</fieldset>';
    }


}
