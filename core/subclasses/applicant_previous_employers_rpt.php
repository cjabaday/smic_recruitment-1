<?php
require_once 'applicant_previous_employers_dd.php';
class applicant_previous_employers_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_PREVIOUS_EMPLOYERS_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_previous_employers_html';
    var $data_subclass = 'applicant_previous_employers';
    var $result_page = 'reporter_result_applicant_previous_employers.php';
    var $cancel_page = 'listview_applicant_previous_employers.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_previous_employers.php';

    function __construct()
    {
        $this->fields        = applicant_previous_employers_dd::load_dictionary();
        $this->relations     = applicant_previous_employers_dd::load_relationships();
        $this->subclasses    = applicant_previous_employers_dd::load_subclass_info();
        $this->table_name    = applicant_previous_employers_dd::$table_name;
        $this->tables        = applicant_previous_employers_dd::$table_name;
        $this->readable_name = applicant_previous_employers_dd::$readable_name;
        $this->get_report_fields();
    }
}
