<?php
require_once 'iemployeestatus_dd.php';
class iemployeestatus_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'iemployeestatus_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'iemployeestatus_html';
    var $data_subclass = 'iemployeestatus';
    var $result_page = 'reporter_result_iemployeestatus.php';
    var $cancel_page = 'listview_iemployeestatus.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_iemployeestatus.php';

    function __construct()
    {
        $this->fields        = iemployeestatus_dd::load_dictionary();
        $this->relations     = iemployeestatus_dd::load_relationships();
        $this->subclasses    = iemployeestatus_dd::load_subclass_info();
        $this->table_name    = iemployeestatus_dd::$table_name;
        $this->tables        = iemployeestatus_dd::$table_name;
        $this->readable_name = iemployeestatus_dd::$readable_name;
        $this->get_report_fields();
    }
}
