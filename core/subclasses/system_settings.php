<?php
require_once 'system_settings_dd.php';
class system_settings extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = system_settings_dd::load_dictionary();
        $this->relations  = system_settings_dd::load_relationships();
        $this->subclasses = system_settings_dd::load_subclass_info();
        $this->table_name = system_settings_dd::$table_name;
        $this->tables     = system_settings_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('setting, value');
            $this->set_values("?,?");

            $this->stmt_bind_param($param['setting']);
            $this->stmt_bind_param($param['value']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("setting = ?, value = ?");
            $this->set_where("setting = ?");

            $this->stmt_bind_param($param['setting']);
            $this->stmt_bind_param($param['value']);
            $this->stmt_bind_param($param['orig_setting']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("setting = ?");

            $this->stmt_bind_param($param['setting']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("setting = ?");

            $this->stmt_bind_param($param['setting']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("setting = ?");

        $this->stmt_bind_param($param['setting']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("setting = ? AND (setting != ?)");

        $this->stmt_bind_param($param['setting']);
        $this->stmt_bind_param($param['orig_setting']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function get($setting_name, $strict_mode=TRUE)
    {
        $setting = $setting_name; //just to create a reference for binding; some calls to get() in existing code provide a string value instead of a variable
        $this->set_fields('value');
        $this->set_where("setting = ?");
        $this->stmt_bind_param($setting_name);
        $this->stmt_prepare();
        $this->stmt_fetch('single');

        if($this->num_rows == 0)
        {
            if($strict_mode)
            {
                error_handler('NO SETTING "' . $setting_name . '" FOUND!', ' Complete query was: ' . $this->query);
            }
            else
            {
                $this->value='';
                $this->dump['value'] = '';
            }
        }

        $this->stmt_bind_args = array(); //reset the bound parameters accumulator so we can safely use this method again without much fuss

        return $this;
    }
}
