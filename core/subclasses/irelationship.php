<?php
require_once 'irelationship_dd.php';
class irelationship extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = irelationship_dd::load_dictionary();
        $this->relations  = irelationship_dd::load_relationships();
        $this->subclasses = irelationship_dd::load_subclass_info();
        $this->table_name = irelationship_dd::$table_name;
        $this->tables     = irelationship_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('RelationshipID, RelationshipCode, RelationshipDesc, EncodeBy, EncodeDate');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['RelationshipID']);
            $this->stmt_bind_param($param['RelationshipCode']);
            $this->stmt_bind_param($param['RelationshipDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("RelationshipID = ?, RelationshipCode = ?, RelationshipDesc = ?, EncodeBy = ?, EncodeDate = ?");
            $this->set_where("RelationshipID = ?");
            
            $this->stmt_bind_param($param['RelationshipID']);
            $this->stmt_bind_param($param['RelationshipCode']);
            $this->stmt_bind_param($param['RelationshipDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);
            $this->stmt_bind_param($param['orig_RelationshipID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("RelationshipID = ?");
            
            $this->stmt_bind_param($param['RelationshipID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("RelationshipID = ?");
            
            $this->stmt_bind_param($param['RelationshipID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("RelationshipID = ?");
        
        $this->stmt_bind_param($param['RelationshipID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("RelationshipID = ? AND (RelationshipID != ?)");
        
        $this->stmt_bind_param($param['RelationshipID']);
        $this->stmt_bind_param($param['orig_RelationshipID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
