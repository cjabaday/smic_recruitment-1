<?php
require_once 'clearance_approver_dd.php';
class clearance_approver_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'CLEARANCE_APPROVER_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'clearance_approver_html';
    var $data_subclass = 'clearance_approver';
    var $result_page = 'reporter_result_clearance_approver.php';
    var $cancel_page = 'listview_clearance_approver.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_clearance_approver.php';

    function __construct()
    {
        $this->fields        = clearance_approver_dd::load_dictionary();
        $this->relations     = clearance_approver_dd::load_relationships();
        $this->subclasses    = clearance_approver_dd::load_subclass_info();
        $this->table_name    = clearance_approver_dd::$table_name;
        $this->tables        = clearance_approver_dd::$table_name;
        $this->readable_name = clearance_approver_dd::$readable_name;
        $this->get_report_fields();
    }
}
