<?php
require_once 'applicant_dd.php';
class applicant extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = applicant_dd::load_dictionary();
        $this->relations  = applicant_dd::load_relationships();
        $this->subclasses = applicant_dd::load_subclass_info();
        $this->table_name = applicant_dd::$table_name;
        $this->tables     = applicant_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('image, applicant_number, last_name, first_name, middle_name, nickname, current_application_status, company_id, source, email_address, gender, civil_status, citizenship, date_of_birth, height, weight, blood_type, birth_place, present_address_line_1, present_address_line_2, present_address_barangay, present_address_city_municipality_id, present_address_province_id, provincial_address_contact_number, provincial_address_line_1, provincial_address_line_2, provincial_address_barangay, provincial_address_city_municipality_id, provincial_address_province_id, contact_number, sss_number, tin, philhealth_id_number, hdmf_number, umid_number, religion, contact_name, contact_relationship, contact_address, contact_contact_number, employee_id, personnel_requisition_id, application_date, application_status');
            $this->set_values("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?");

            $this->stmt_bind_param($param['image']);
            $this->stmt_bind_param($param['applicant_number']);
            $this->stmt_bind_param($param['last_name']);
            $this->stmt_bind_param($param['first_name']);
            $this->stmt_bind_param($param['middle_name']);
            $this->stmt_bind_param($param['nickname']);
            $this->stmt_bind_param($param['current_application_status']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['source']);
            $this->stmt_bind_param($param['email_address']);
            $this->stmt_bind_param($param['gender']);
            $this->stmt_bind_param($param['civil_status']);
            $this->stmt_bind_param($param['citizenship']);
            $this->stmt_bind_param($param['date_of_birth']);
            $this->stmt_bind_param($param['height']);
            $this->stmt_bind_param($param['weight']);
            $this->stmt_bind_param($param['blood_type']);
            $this->stmt_bind_param($param['birth_place']);
            $this->stmt_bind_param($param['present_address_line_1']);
            $this->stmt_bind_param($param['present_address_line_2']);
            $this->stmt_bind_param($param['present_address_barangay']);
            $this->stmt_bind_param($param['present_address_city_municipality_id']);
            $this->stmt_bind_param($param['present_address_province_id']);
            $this->stmt_bind_param($param['provincial_address_contact_number']);
            $this->stmt_bind_param($param['provincial_address_line_1']);
            $this->stmt_bind_param($param['provincial_address_line_2']);
            $this->stmt_bind_param($param['provincial_address_barangay']);
            $this->stmt_bind_param($param['provincial_address_city_municipality_id']);
            $this->stmt_bind_param($param['provincial_address_province_id']);
            $this->stmt_bind_param($param['contact_number']);
            $this->stmt_bind_param($param['sss_number']);
            $this->stmt_bind_param($param['tin']);
            $this->stmt_bind_param($param['philhealth_id_number']);
            $this->stmt_bind_param($param['hdmf_number']);
            $this->stmt_bind_param($param['umid_number']);
            $this->stmt_bind_param($param['religion']);
            $this->stmt_bind_param($param['contact_name']);
            $this->stmt_bind_param($param['contact_relationship']);
            $this->stmt_bind_param($param['contact_address']);
            $this->stmt_bind_param($param['contact_contact_number']);
            $this->stmt_bind_param($param['employee_id']);
            $this->stmt_bind_param($param['personnel_requisition_id']);
            $this->stmt_bind_param($param['application_date']);
            $this->stmt_bind_param($param['application_status']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ?");
            $this->set_where("applicant_id = ?");

            $this->stmt_bind_param($param['image']);
            $this->stmt_bind_param($param['last_name']);
            $this->stmt_bind_param($param['first_name']);
            $this->stmt_bind_param($param['middle_name']);
            $this->stmt_bind_param($param['nickname']);
            $this->stmt_bind_param($param['current_application_status']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['source']);
            $this->stmt_bind_param($param['email_address']);
            $this->stmt_bind_param($param['gender']);
            $this->stmt_bind_param($param['civil_status']);
            $this->stmt_bind_param($param['citizenship']);
            $this->stmt_bind_param($param['date_of_birth']);
            $this->stmt_bind_param($param['height']);
            $this->stmt_bind_param($param['weight']);
            $this->stmt_bind_param($param['blood_type']);
            $this->stmt_bind_param($param['birth_place']);
            $this->stmt_bind_param($param['present_address_line_1']);
            $this->stmt_bind_param($param['present_address_line_2']);
            $this->stmt_bind_param($param['present_address_barangay']);
            $this->stmt_bind_param($param['present_address_city_municipality_id']);
            $this->stmt_bind_param($param['present_address_province_id']);
            $this->stmt_bind_param($param['provincial_address_contact_number']);
            $this->stmt_bind_param($param['provincial_address_line_1']);
            $this->stmt_bind_param($param['provincial_address_line_2']);
            $this->stmt_bind_param($param['provincial_address_barangay']);
            $this->stmt_bind_param($param['provincial_address_city_municipality_id']);
            $this->stmt_bind_param($param['provincial_address_province_id']);
            $this->stmt_bind_param($param['contact_number']);
            $this->stmt_bind_param($param['sss_number']);
            $this->stmt_bind_param($param['tin']);
            $this->stmt_bind_param($param['philhealth_id_number']);
            $this->stmt_bind_param($param['hdmf_number']);
            $this->stmt_bind_param($param['umid_number']);
            $this->stmt_bind_param($param['religion']);
            $this->stmt_bind_param($param['contact_name']);
            $this->stmt_bind_param($param['contact_relationship']);
            $this->stmt_bind_param($param['contact_address']);
            $this->stmt_bind_param($param['contact_contact_number']);
            $this->stmt_bind_param($param['applicant_number']);
            $this->stmt_bind_param($param['employee_id']);
            $this->stmt_bind_param($param['personnel_requisition_id']);
            $this->stmt_bind_param($param['application_date']);
            $this->stmt_bind_param($param['application_status']);
            $this->stmt_bind_param($param['applicant_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_id = ?");

            $this->stmt_bind_param($param['applicant_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_id = ?");

        $this->stmt_bind_param($param['applicant_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_id = ? AND (applicant_id != ?)");

        $this->stmt_bind_param($param['applicant_id']);
        $this->stmt_bind_param($param['applicant_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function applicant_registration($param)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('applicant_number, last_name, first_name, middle_name, gender');
            $this->set_values("?,?,?,?,?");

            $this->stmt_bind_param($param['applicant_number']);
            $this->stmt_bind_param($param['last_name']);
            $this->stmt_bind_param($param['first_name']);
            $this->stmt_bind_param($param['middle_name']);
            $this->stmt_bind_param($param['gender']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select_record($applicant_id)
    {
        $this->set_table('applicant a LEFT JOIN city b ON a.present_address_city_municipality_id = b.city_id
                                      LEFT JOIN province c ON a.present_address_province_id = c.province_id
                                      LEFT JOIN city d ON a.provincial_address_city_municipality_id = d.city_id
                                      LEFT JOIN province e ON a.provincial_address_province_id = e.province_id
                                      LEFT JOIN user f ON a.applicant_id = f.applicant_id
                                      ');
        $this->set_fields('   a.applicant_id,
                              last_name,
                              first_name,
                              middle_name,
                              nickname,
                              current_application_status,
                              company_id,
                              source,
                              f.username as `email_address`,
                              gender,
                              civil_status,
                              citizenship,
                              date_of_birth,
                              height,
                              weight,
                              blood_type,
                              birth_place,
                              present_address_line_1,
                              present_address_line_2,
                              present_address_barangay,
                              provincial_address_line_1,
                              provincial_address_line_2,
                              provincial_address_barangay,
                              b.city_name as `present_address_city_Municipality`,
                              c.province_name as `present_address_province`,
                              provincial_address_contact_number,
                              d.city_name as `provincial_address_city_municipality`,
                              e.province_name as `province_address_province`,
                              contact_number,
                              sss_number,
                              tin,
                              philhealth_id_number,
                              hdmf_number,
                              umid_number,
                              religion,
                              contact_name,
                              contact_relationship,
                              contact_address,
                              contact_contact_number,
                              applicant_number,
                              employee_id,
                              application_date,
                              personnel_requisition_id


                              ');
        $this->set_where('a.applicant_id = ?');
        $this->stmt_bind_param($applicant_id);
        $this->stmt_fetch('single');

        // debug($this->dump);
        return $this;
    }
}
