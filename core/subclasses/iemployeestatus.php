<?php
require_once 'iemployeestatus_dd.php';
class iemployeestatus extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = iemployeestatus_dd::load_dictionary();
        $this->relations  = iemployeestatus_dd::load_relationships();
        $this->subclasses = iemployeestatus_dd::load_subclass_info();
        $this->table_name = iemployeestatus_dd::$table_name;
        $this->tables     = iemployeestatus_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('EmployeeStatusID, EmployeeStatusCode, EmployeeStatusDesc, EncodeBy, EncodeDate');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['EmployeeStatusID']);
            $this->stmt_bind_param($param['EmployeeStatusCode']);
            $this->stmt_bind_param($param['EmployeeStatusDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("EmployeeStatusID = ?, EmployeeStatusCode = ?, EmployeeStatusDesc = ?, EncodeBy = ?, EncodeDate = ?");
            $this->set_where("EmployeeStatusID = ?");
            
            $this->stmt_bind_param($param['EmployeeStatusID']);
            $this->stmt_bind_param($param['EmployeeStatusCode']);
            $this->stmt_bind_param($param['EmployeeStatusDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);
            $this->stmt_bind_param($param['orig_EmployeeStatusID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("EmployeeStatusID = ?");
            
            $this->stmt_bind_param($param['EmployeeStatusID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("EmployeeStatusID = ?");
            
            $this->stmt_bind_param($param['EmployeeStatusID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("EmployeeStatusID = ?");
        
        $this->stmt_bind_param($param['EmployeeStatusID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("EmployeeStatusID = ? AND (EmployeeStatusID != ?)");
        
        $this->stmt_bind_param($param['EmployeeStatusID']);
        $this->stmt_bind_param($param['orig_EmployeeStatusID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
