<?php
require_once 'documentation_class.php';
require_once 'training_dd.php';
class training_doc extends documentation
{
    function __construct()
    {
        $this->fields        = training_dd::load_dictionary();
        $this->relations     = training_dd::load_relationships();
        $this->subclasses    = training_dd::load_subclass_info();
        $this->table_name    = training_dd::$table_name;
        $this->readable_name = training_dd::$readable_name;
        parent::__construct();
    }
}
