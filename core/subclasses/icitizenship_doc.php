<?php
require_once 'documentation_class.php';
require_once 'icitizenship_dd.php';
class icitizenship_doc extends documentation
{
    function __construct()
    {
        $this->fields        = icitizenship_dd::load_dictionary();
        $this->relations     = icitizenship_dd::load_relationships();
        $this->subclasses    = icitizenship_dd::load_subclass_info();
        $this->table_name    = icitizenship_dd::$table_name;
        $this->readable_name = icitizenship_dd::$readable_name;
        parent::__construct();
    }
}
