<?php
require_once 'iprf_staffrequest_applicants_dd.php';
class iprf_staffrequest_applicants extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = iprf_staffrequest_applicants_dd::load_dictionary();
        $this->relations  = iprf_staffrequest_applicants_dd::load_relationships();
        $this->subclasses = iprf_staffrequest_applicants_dd::load_subclass_info();
        $this->table_name = iprf_staffrequest_applicants_dd::$table_name;
        $this->tables     = iprf_staffrequest_applicants_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('iprf_staffrequest_id, applicant_id, date_applied, updates');
            $this->set_values("?,?,?,?");

            $this->stmt_bind_param($param['iprf_staffrequest_id']);
            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['date_applied']);
            $this->stmt_bind_param($param['updates']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("iprf_staffrequest_id = ?, applicant_id = ?, date_applied = ?, updates = ? ");
            $this->set_where("iprf_staffrequest_applicant_id = ?");

            $this->stmt_bind_param($param['iprf_staffrequest_id']);
            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['date_applied']);
            $this->stmt_bind_param($param['updates']);
            $this->stmt_bind_param($param['iprf_staffrequest_applicant_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("iprf_staffrequest_id = ?");

            $this->stmt_bind_param($param['iprf_staffrequest_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("");


        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where(" AND ()");


        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function edit_updates(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("updates = ? ");
            $this->set_where("iprf_staffrequest_applicant_id = ?");

            $this->stmt_bind_param($param['updates']);
            $this->stmt_bind_param($param['iprf_staffrequest_applicant_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function check_application_in_staffrequest($applicant_id, $staffrequest_id)
    {
        $this->set_where('applicant_id = ? AND iprf_staffrequest_id = ?');
        $this->stmt_bind_param($applicant_id);
        $this->stmt_bind_param($staffrequest_id);
        $this->stmt_prepare();
        $this->stmt_fetch('single');

        return $this;

    }

    function fetch_iprf_applicants_details($id)
    {
        $this->set_table('iprf_staffrequest_applicants a LEFT JOIN applicant         b ON a.applicant_id         = b.applicant_id
                                                      LEFT JOIN iprf_staffrequest c ON a.iprf_staffrequest_id = c.iprf_staffrequest_id
                                                      LEFT JOIN position          d ON c.position_id          = d.position_id');
        $this->set_fields('last_name, first_name, middle_name, a.applicant_id, title, updates');
        $this->set_where('iprf_staffrequest_applicant_id = ?');
        $this->stmt_bind_param($id);
        $this->stmt_prepare();
        $this->stmt_fetch('single');

        return $this;
    }
}
