<?php
require_once 'icourse_dd.php';
class icourse_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'icourse_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'icourse_html';
    var $data_subclass = 'icourse';
    var $result_page = 'reporter_result_icourse.php';
    var $cancel_page = 'listview_icourse.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_icourse.php';

    function __construct()
    {
        $this->fields        = icourse_dd::load_dictionary();
        $this->relations     = icourse_dd::load_relationships();
        $this->subclasses    = icourse_dd::load_subclass_info();
        $this->table_name    = icourse_dd::$table_name;
        $this->tables        = icourse_dd::$table_name;
        $this->readable_name = icourse_dd::$readable_name;
        $this->get_report_fields();
    }
}
