<?php
require_once 'employee_evaluation_answer_dd.php';
class employee_evaluation_answer extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = employee_evaluation_answer_dd::load_dictionary();
        $this->relations  = employee_evaluation_answer_dd::load_relationships();
        $this->subclasses = employee_evaluation_answer_dd::load_subclass_info();
        $this->table_name = employee_evaluation_answer_dd::$table_name;
        $this->tables     = employee_evaluation_answer_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('employee_evaluation_id, criteria_name, details, rating, notes');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['employee_evaluation_id']);
            $this->stmt_bind_param($param['criteria_name']);
            $this->stmt_bind_param($param['details']);
            $this->stmt_bind_param($param['rating']);
            $this->stmt_bind_param($param['notes']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("employee_evaluation_id = ?, criteria_name = ?, details = ?, rating = ?, notes = ?");
            $this->set_where("employee_evaluation_answer_id = ?");
            
            $this->stmt_bind_param($param['employee_evaluation_id']);
            $this->stmt_bind_param($param['criteria_name']);
            $this->stmt_bind_param($param['details']);
            $this->stmt_bind_param($param['rating']);
            $this->stmt_bind_param($param['notes']);
            $this->stmt_bind_param($param['employee_evaluation_answer_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("employee_evaluation_answer_id = ?");
            
            $this->stmt_bind_param($param['employee_evaluation_answer_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("employee_evaluation_answer_id = ?");
        
        $this->stmt_bind_param($param['employee_evaluation_answer_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("employee_evaluation_answer_id = ? AND (employee_evaluation_answer_id != ?)");
        
        $this->stmt_bind_param($param['employee_evaluation_answer_id']);
        $this->stmt_bind_param($param['employee_evaluation_answer_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
