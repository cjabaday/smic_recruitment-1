<?php
require_once 'training_dd.php';
class training_html extends html
{
    function __construct()
    {
        $this->fields        = training_dd::load_dictionary();
        $this->relations     = training_dd::load_relationships();
        $this->subclasses    = training_dd::load_subclass_info();
        $this->table_name    = training_dd::$table_name;
        $this->readable_name = training_dd::$readable_name;
    }
}
