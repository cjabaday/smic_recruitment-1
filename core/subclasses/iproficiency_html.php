<?php
require_once 'iproficiency_dd.php';
class iproficiency_html extends html
{
    function __construct()
    {
        $this->fields        = iproficiency_dd::load_dictionary();
        $this->relations     = iproficiency_dd::load_relationships();
        $this->subclasses    = iproficiency_dd::load_subclass_info();
        $this->table_name    = iproficiency_dd::$table_name;
        $this->readable_name = iproficiency_dd::$readable_name;
    }
}
