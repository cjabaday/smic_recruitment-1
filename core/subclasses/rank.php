<?php
require_once 'rank_dd.php';
class rank extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = rank_dd::load_dictionary();
        $this->relations  = rank_dd::load_relationships();
        $this->subclasses = rank_dd::load_subclass_info();
        $this->table_name = rank_dd::$table_name;
        $this->tables     = rank_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('name, description');
            $this->set_values("?,?");

            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['description']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("name = ?, description = ?");
            $this->set_where("rank_id = ?");

            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['rank_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("rank_id = ?");

            $this->stmt_bind_param($param['rank_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("rank_id = ?");

        $this->stmt_bind_param($param['rank_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_label($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("name = ?");

        $this->stmt_bind_param($param['name']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("rank_id = ? AND (rank_id != ?)");

        $this->stmt_bind_param($param['rank_id']);
        $this->stmt_bind_param($param['rank_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function add_rank(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('rank_id, name');
            $this->set_values("?,?");

            $this->stmt_bind_param($param['rank_id']);
            $this->stmt_bind_param($param['name']);

            $this->stmt_prepare();
            // debug($this->query);
        }
        $this->stmt_execute();
        return $this;
    }
}
