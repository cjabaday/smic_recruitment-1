<?php
require_once 'sst_class.php';
require_once 'classification_dd.php';
class classification_sst extends sst
{
    function __construct()
    {
        $this->fields        = classification_dd::load_dictionary();
        $this->relations     = classification_dd::load_relationships();
        $this->subclasses    = classification_dd::load_subclass_info();
        $this->table_name    = classification_dd::$table_name;
        $this->readable_name = classification_dd::$readable_name;
        parent::__construct();
    }
}
