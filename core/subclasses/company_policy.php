<?php
require_once 'company_policy_dd.php';
class company_policy extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = company_policy_dd::load_dictionary();
        $this->relations  = company_policy_dd::load_relationships();
        $this->subclasses = company_policy_dd::load_subclass_info();
        $this->table_name = company_policy_dd::$table_name;
        $this->tables     = company_policy_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('parent_company_policy_id, policy_code, policy_name, details');
            $this->set_values("?,?,?,?");
            
            $this->stmt_bind_param($param['parent_company_policy_id']);
            $this->stmt_bind_param($param['policy_code']);
            $this->stmt_bind_param($param['policy_name']);
            $this->stmt_bind_param($param['details']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("parent_company_policy_id = ?, policy_code = ?, policy_name = ?, details = ?");
            $this->set_where("company_policy_id = ?");
            
            $this->stmt_bind_param($param['parent_company_policy_id']);
            $this->stmt_bind_param($param['policy_code']);
            $this->stmt_bind_param($param['policy_name']);
            $this->stmt_bind_param($param['details']);
            $this->stmt_bind_param($param['company_policy_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("company_policy_id = ?");
            
            $this->stmt_bind_param($param['company_policy_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("company_policy_id = ?");
        
        $this->stmt_bind_param($param['company_policy_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("company_policy_id = ? AND (company_policy_id != ?)");
        
        $this->stmt_bind_param($param['company_policy_id']);
        $this->stmt_bind_param($param['company_policy_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
