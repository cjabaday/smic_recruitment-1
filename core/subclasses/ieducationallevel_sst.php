<?php
require_once 'sst_class.php';
require_once 'ieducationallevel_dd.php';
class ieducationallevel_sst extends sst
{
    function __construct()
    {
        $this->fields        = ieducationallevel_dd::load_dictionary();
        $this->relations     = ieducationallevel_dd::load_relationships();
        $this->subclasses    = ieducationallevel_dd::load_subclass_info();
        $this->table_name    = ieducationallevel_dd::$table_name;
        $this->readable_name = ieducationallevel_dd::$readable_name;
        parent::__construct();
    }
}
