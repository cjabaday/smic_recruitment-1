<?php
require_once 'temp_user_dd.php';
class temp_user_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'TEMP_USER_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'temp_user_html';
    var $data_subclass = 'temp_user';
    var $result_page = 'reporter_result_temp_user.php';
    var $cancel_page = 'listview_temp_user.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_temp_user.php';

    function __construct()
    {
        $this->fields        = temp_user_dd::load_dictionary();
        $this->relations     = temp_user_dd::load_relationships();
        $this->subclasses    = temp_user_dd::load_subclass_info();
        $this->table_name    = temp_user_dd::$table_name;
        $this->tables        = temp_user_dd::$table_name;
        $this->readable_name = temp_user_dd::$readable_name;
        $this->get_report_fields();
    }
}
