<?php
require_once 'personnel_requisition_dd.php';
class personnel_requisition extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = personnel_requisition_dd::load_dictionary();
        $this->relations  = personnel_requisition_dd::load_relationships();
        $this->subclasses = personnel_requisition_dd::load_subclass_info();
        $this->table_name = personnel_requisition_dd::$table_name;
        $this->tables     = personnel_requisition_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('plantilla_id, branch_id, department_id, company_id, employee_id, requisition_date');
            $this->set_values("?,?,?,?,?,?");
            
            $this->stmt_bind_param($param['plantilla_id']);
            $this->stmt_bind_param($param['branch_id']);
            $this->stmt_bind_param($param['department_id']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['employee_id']);
            $this->stmt_bind_param($param['requisition_date']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("plantilla_id = ?, branch_id = ?, department_id = ?, company_id = ?, employee_id = ?, requisition_date = ?");
            $this->set_where("personnel_requisition_id = ?");
            
            $this->stmt_bind_param($param['plantilla_id']);
            $this->stmt_bind_param($param['branch_id']);
            $this->stmt_bind_param($param['department_id']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['employee_id']);
            $this->stmt_bind_param($param['requisition_date']);
            $this->stmt_bind_param($param['personnel_requisition_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("personnel_requisition_id = ?");
            
            $this->stmt_bind_param($param['personnel_requisition_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("personnel_requisition_id = ?");
        
        $this->stmt_bind_param($param['personnel_requisition_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("personnel_requisition_id = ? AND (personnel_requisition_id != ?)");
        
        $this->stmt_bind_param($param['personnel_requisition_id']);
        $this->stmt_bind_param($param['personnel_requisition_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
