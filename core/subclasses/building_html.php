<?php
require_once 'building_dd.php';
class building_html extends html
{
    function __construct()
    {
        $this->fields        = building_dd::load_dictionary();
        $this->relations     = building_dd::load_relationships();
        $this->subclasses    = building_dd::load_subclass_info();
        $this->table_name    = building_dd::$table_name;
        $this->readable_name = building_dd::$readable_name;
    }
}
