<?php
require_once 'documentation_class.php';
require_once 'resignation_dd.php';
class resignation_doc extends documentation
{
    function __construct()
    {
        $this->fields        = resignation_dd::load_dictionary();
        $this->relations     = resignation_dd::load_relationships();
        $this->subclasses    = resignation_dd::load_subclass_info();
        $this->table_name    = resignation_dd::$table_name;
        $this->readable_name = resignation_dd::$readable_name;
        parent::__construct();
    }
}
