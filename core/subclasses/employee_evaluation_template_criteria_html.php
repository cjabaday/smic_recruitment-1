<?php
require_once 'employee_evaluation_template_criteria_dd.php';
class employee_evaluation_template_criteria_html extends html
{
    function __construct()
    {
        $this->fields        = employee_evaluation_template_criteria_dd::load_dictionary();
        $this->relations     = employee_evaluation_template_criteria_dd::load_relationships();
        $this->subclasses    = employee_evaluation_template_criteria_dd::load_subclass_info();
        $this->table_name    = employee_evaluation_template_criteria_dd::$table_name;
        $this->readable_name = employee_evaluation_template_criteria_dd::$readable_name;
    }
}
