<?php
require_once 'plantilla_dd.php';
class plantilla_html extends html
{
    function __construct()
    {
        $this->fields        = plantilla_dd::load_dictionary();
        $this->relations     = plantilla_dd::load_relationships();
        $this->subclasses    = plantilla_dd::load_subclass_info();
        $this->table_name    = plantilla_dd::$table_name;
        $this->readable_name = plantilla_dd::$readable_name;
    }
}
