<?php
require_once 'resignation_dd.php';
class resignation_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'RESIGNATION_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'resignation_html';
    var $data_subclass = 'resignation';
    var $result_page = 'reporter_result_resignation.php';
    var $cancel_page = 'listview_resignation.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_resignation.php';

    function __construct()
    {
        $this->fields        = resignation_dd::load_dictionary();
        $this->relations     = resignation_dd::load_relationships();
        $this->subclasses    = resignation_dd::load_subclass_info();
        $this->table_name    = resignation_dd::$table_name;
        $this->tables        = resignation_dd::$table_name;
        $this->readable_name = resignation_dd::$readable_name;
        $this->get_report_fields();
    }
}
