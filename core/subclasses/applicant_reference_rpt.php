<?php
require_once 'applicant_reference_dd.php';
class applicant_reference_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_REFERENCE_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_reference_html';
    var $data_subclass = 'applicant_reference';
    var $result_page = 'reporter_result_applicant_reference.php';
    var $cancel_page = 'listview_applicant_reference.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_reference.php';

    function __construct()
    {
        $this->fields        = applicant_reference_dd::load_dictionary();
        $this->relations     = applicant_reference_dd::load_relationships();
        $this->subclasses    = applicant_reference_dd::load_subclass_info();
        $this->table_name    = applicant_reference_dd::$table_name;
        $this->tables        = applicant_reference_dd::$table_name;
        $this->readable_name = applicant_reference_dd::$readable_name;
        $this->get_report_fields();
    }
}
