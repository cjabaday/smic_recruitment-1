<?php
require_once 'documentation_class.php';
require_once 'user_passport_dd.php';
class user_passport_doc extends documentation
{
    function __construct()
    {
        $this->fields        = user_passport_dd::load_dictionary();
        $this->relations     = user_passport_dd::load_relationships();
        $this->subclasses    = user_passport_dd::load_subclass_info();
        $this->table_name    = user_passport_dd::$table_name;
        $this->readable_name = user_passport_dd::$readable_name;
        parent::__construct();
    }
}
