<?php
require_once 'applicant_languages_proficiency_dd.php';
class applicant_languages_proficiency_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_LANGUAGES_PROFICIENCY_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_languages_proficiency_html';
    var $data_subclass = 'applicant_languages_proficiency';
    var $result_page = 'reporter_result_applicant_languages_proficiency.php';
    var $cancel_page = 'listview_applicant_languages_proficiency.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_languages_proficiency.php';

    function __construct()
    {
        $this->fields        = applicant_languages_proficiency_dd::load_dictionary();
        $this->relations     = applicant_languages_proficiency_dd::load_relationships();
        $this->subclasses    = applicant_languages_proficiency_dd::load_subclass_info();
        $this->table_name    = applicant_languages_proficiency_dd::$table_name;
        $this->tables        = applicant_languages_proficiency_dd::$table_name;
        $this->readable_name = applicant_languages_proficiency_dd::$readable_name;
        $this->get_report_fields();
    }
}
