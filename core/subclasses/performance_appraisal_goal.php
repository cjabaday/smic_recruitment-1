<?php
require_once 'performance_appraisal_goal_dd.php';
class performance_appraisal_goal extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = performance_appraisal_goal_dd::load_dictionary();
        $this->relations  = performance_appraisal_goal_dd::load_relationships();
        $this->subclasses = performance_appraisal_goal_dd::load_subclass_info();
        $this->table_name = performance_appraisal_goal_dd::$table_name;
        $this->tables     = performance_appraisal_goal_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('performance_appraisal_id, goal, rating, notes');
            $this->set_values("?,?,?,?");
            
            $this->stmt_bind_param($param['performance_appraisal_id']);
            $this->stmt_bind_param($param['goal']);
            $this->stmt_bind_param($param['rating']);
            $this->stmt_bind_param($param['notes']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("performance_appraisal_id = ?, goal = ?, rating = ?, notes = ?");
            $this->set_where("performance_appraisal_goal_id = ?");
            
            $this->stmt_bind_param($param['performance_appraisal_id']);
            $this->stmt_bind_param($param['goal']);
            $this->stmt_bind_param($param['rating']);
            $this->stmt_bind_param($param['notes']);
            $this->stmt_bind_param($param['performance_appraisal_goal_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("performance_appraisal_goal_id = ?");
            
            $this->stmt_bind_param($param['performance_appraisal_goal_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("performance_appraisal_goal_id = ?");
        
        $this->stmt_bind_param($param['performance_appraisal_goal_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("performance_appraisal_goal_id = ? AND (performance_appraisal_goal_id != ?)");
        
        $this->stmt_bind_param($param['performance_appraisal_goal_id']);
        $this->stmt_bind_param($param['performance_appraisal_goal_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
