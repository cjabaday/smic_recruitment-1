<?php
require_once 'training_dd.php';
class training_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'TRAINING_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'training_html';
    var $data_subclass = 'training';
    var $result_page = 'reporter_result_training.php';
    var $cancel_page = 'listview_training.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_training.php';

    function __construct()
    {
        $this->fields        = training_dd::load_dictionary();
        $this->relations     = training_dd::load_relationships();
        $this->subclasses    = training_dd::load_subclass_info();
        $this->table_name    = training_dd::$table_name;
        $this->tables        = training_dd::$table_name;
        $this->readable_name = training_dd::$readable_name;
        $this->get_report_fields();
    }
}
