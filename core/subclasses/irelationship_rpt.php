<?php
require_once 'irelationship_dd.php';
class irelationship_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'irelationship_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'irelationship_html';
    var $data_subclass = 'irelationship';
    var $result_page = 'reporter_result_irelationship.php';
    var $cancel_page = 'listview_irelationship.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_irelationship.php';

    function __construct()
    {
        $this->fields        = irelationship_dd::load_dictionary();
        $this->relations     = irelationship_dd::load_relationships();
        $this->subclasses    = irelationship_dd::load_subclass_info();
        $this->table_name    = irelationship_dd::$table_name;
        $this->tables        = irelationship_dd::$table_name;
        $this->readable_name = irelationship_dd::$readable_name;
        $this->get_report_fields();
    }
}
