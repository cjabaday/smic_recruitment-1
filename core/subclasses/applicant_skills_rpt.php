<?php
require_once 'applicant_skills_dd.php';
class applicant_skills_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_SKILLS_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_skills_html';
    var $data_subclass = 'applicant_skills';
    var $result_page = 'reporter_result_applicant_skills.php';
    var $cancel_page = 'listview_applicant_skills.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_skills.php';

    function __construct()
    {
        $this->fields        = applicant_skills_dd::load_dictionary();
        $this->relations     = applicant_skills_dd::load_relationships();
        $this->subclasses    = applicant_skills_dd::load_subclass_info();
        $this->table_name    = applicant_skills_dd::$table_name;
        $this->tables        = applicant_skills_dd::$table_name;
        $this->readable_name = applicant_skills_dd::$readable_name;
        $this->get_report_fields();
    }
}
