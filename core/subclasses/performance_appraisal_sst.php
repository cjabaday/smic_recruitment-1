<?php
require_once 'sst_class.php';
require_once 'performance_appraisal_dd.php';
class performance_appraisal_sst extends sst
{
    function __construct()
    {
        $this->fields        = performance_appraisal_dd::load_dictionary();
        $this->relations     = performance_appraisal_dd::load_relationships();
        $this->subclasses    = performance_appraisal_dd::load_subclass_info();
        $this->table_name    = performance_appraisal_dd::$table_name;
        $this->readable_name = performance_appraisal_dd::$readable_name;
        parent::__construct();
    }
}
