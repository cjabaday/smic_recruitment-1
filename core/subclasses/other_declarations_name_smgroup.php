<?php
require_once 'other_declarations_name_smgroup_dd.php';
class other_declarations_name_smgroup extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = other_declarations_name_smgroup_dd::load_dictionary();
        $this->relations  = other_declarations_name_smgroup_dd::load_relationships();
        $this->subclasses = other_declarations_name_smgroup_dd::load_subclass_info();
        $this->table_name = other_declarations_name_smgroup_dd::$table_name;
        $this->tables     = other_declarations_name_smgroup_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('applicant_other_declaration_id, name,  relationship, position, company');
            $this->set_values("?,?,?,?,?");

            $this->stmt_bind_param($param['applicant_other_declaration_id']);
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['relationship']);
            $this->stmt_bind_param($param['position']);
            $this->stmt_bind_param($param['company']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("applicant_other_declaration_id = ?, name = ?, relationship = ?, position = ?, company = ?");
            $this->set_where("other_declarations_name_smgroup_id = ?");

            $this->stmt_bind_param($param['applicant_other_declaration_id']);
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['relationship']);
            $this->stmt_bind_param($param['position']);
            $this->stmt_bind_param($param['company']);
            $this->stmt_bind_param($param['other_declarations_name_smgroup_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("other_declarations_name_smgroup_id = ?");

            $this->stmt_bind_param($param['other_declarations_name_smgroup_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_other_declaration_id = ?");

            $this->stmt_bind_param($param['applicant_other_declaration_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("other_declarations_name_smgroup_id = ?");

        $this->stmt_bind_param($param['other_declarations_name_smgroup_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("other_declarations_name_smgroup_id = ? AND (other_declarations_name_smgroup_id != ?)");

        $this->stmt_bind_param($param['other_declarations_name_smgroup_id']);
        $this->stmt_bind_param($param['other_declarations_name_smgroup_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function select_record($applicant_other_declaration_id)
    {
        $this->set_where('applicant_other_declaration_id = ?');
        $this->stmt_bind_param($applicant_other_declaration_id);
        $this->stmt_fetch('rowdump');

        return $this;
    }
}
