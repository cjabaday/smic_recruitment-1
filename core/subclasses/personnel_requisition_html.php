<?php
require_once 'personnel_requisition_dd.php';
class personnel_requisition_html extends html
{
    function __construct()
    {
        $this->fields        = personnel_requisition_dd::load_dictionary();
        $this->relations     = personnel_requisition_dd::load_relationships();
        $this->subclasses    = personnel_requisition_dd::load_subclass_info();
        $this->table_name    = personnel_requisition_dd::$table_name;
        $this->readable_name = personnel_requisition_dd::$readable_name;
    }
}
