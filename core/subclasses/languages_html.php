<?php
require_once 'languages_dd.php';
class languages_html extends html
{
    function __construct()
    {
        $this->fields        = languages_dd::load_dictionary();
        $this->relations     = languages_dd::load_relationships();
        $this->subclasses    = languages_dd::load_subclass_info();
        $this->table_name    = languages_dd::$table_name;
        $this->readable_name = languages_dd::$readable_name;
    }
}
