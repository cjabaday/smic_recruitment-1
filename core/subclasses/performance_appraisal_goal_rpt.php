<?php
require_once 'performance_appraisal_goal_dd.php';
class performance_appraisal_goal_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'PERFORMANCE_APPRAISAL_GOAL_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'performance_appraisal_goal_html';
    var $data_subclass = 'performance_appraisal_goal';
    var $result_page = 'reporter_result_performance_appraisal_goal.php';
    var $cancel_page = 'listview_performance_appraisal_goal.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_performance_appraisal_goal.php';

    function __construct()
    {
        $this->fields        = performance_appraisal_goal_dd::load_dictionary();
        $this->relations     = performance_appraisal_goal_dd::load_relationships();
        $this->subclasses    = performance_appraisal_goal_dd::load_subclass_info();
        $this->table_name    = performance_appraisal_goal_dd::$table_name;
        $this->tables        = performance_appraisal_goal_dd::$table_name;
        $this->readable_name = performance_appraisal_goal_dd::$readable_name;
        $this->get_report_fields();
    }
}
