<?php
require_once 'sst_class.php';
require_once 'applicant_reference_dd.php';
class applicant_reference_sst extends sst
{
    function __construct()
    {
        $this->fields        = applicant_reference_dd::load_dictionary();
        $this->relations     = applicant_reference_dd::load_relationships();
        $this->subclasses    = applicant_reference_dd::load_subclass_info();
        $this->table_name    = applicant_reference_dd::$table_name;
        $this->readable_name = applicant_reference_dd::$readable_name;
        parent::__construct();
    }
}
