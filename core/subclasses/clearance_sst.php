<?php
require_once 'sst_class.php';
require_once 'clearance_dd.php';
class clearance_sst extends sst
{
    function __construct()
    {
        $this->fields        = clearance_dd::load_dictionary();
        $this->relations     = clearance_dd::load_relationships();
        $this->subclasses    = clearance_dd::load_subclass_info();
        $this->table_name    = clearance_dd::$table_name;
        $this->readable_name = clearance_dd::$readable_name;
        parent::__construct();
    }
}
