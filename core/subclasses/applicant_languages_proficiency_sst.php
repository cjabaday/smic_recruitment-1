<?php
require_once 'sst_class.php';
require_once 'applicant_languages_proficiency_dd.php';
class applicant_languages_proficiency_sst extends sst
{
    function __construct()
    {
        $this->fields        = applicant_languages_proficiency_dd::load_dictionary();
        $this->relations     = applicant_languages_proficiency_dd::load_relationships();
        $this->subclasses    = applicant_languages_proficiency_dd::load_subclass_info();
        $this->table_name    = applicant_languages_proficiency_dd::$table_name;
        $this->readable_name = applicant_languages_proficiency_dd::$readable_name;
        parent::__construct();
    }
}
