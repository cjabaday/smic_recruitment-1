<?php
require_once 'applicant_interview_dd.php';
class applicant_interview_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_INTERVIEW_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_interview_html';
    var $data_subclass = 'applicant_interview';
    var $result_page = 'reporter_result_applicant_interview.php';
    var $cancel_page = 'listview_applicant_interview.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_interview.php';

    function __construct()
    {
        $this->fields        = applicant_interview_dd::load_dictionary();
        $this->relations     = applicant_interview_dd::load_relationships();
        $this->subclasses    = applicant_interview_dd::load_subclass_info();
        $this->table_name    = applicant_interview_dd::$table_name;
        $this->tables        = applicant_interview_dd::$table_name;
        $this->readable_name = applicant_interview_dd::$readable_name;
        $this->get_report_fields();
    }
}
