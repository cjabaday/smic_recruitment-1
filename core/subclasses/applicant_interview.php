<?php
require_once 'applicant_interview_dd.php';
class applicant_interview extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = applicant_interview_dd::load_dictionary();
        $this->relations  = applicant_interview_dd::load_relationships();
        $this->subclasses = applicant_interview_dd::load_subclass_info();
        $this->table_name = applicant_interview_dd::$table_name;
        $this->tables     = applicant_interview_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('applicant_id, interviewer_employee_id, interview_date, remarks');
            $this->set_values("?,?,?,?");
            
            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['interviewer_employee_id']);
            $this->stmt_bind_param($param['interview_date']);
            $this->stmt_bind_param($param['remarks']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("applicant_id = ?, interviewer_employee_id = ?, interview_date = ?, remarks = ?");
            $this->set_where("applicant_interview_id = ?");
            
            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['interviewer_employee_id']);
            $this->stmt_bind_param($param['interview_date']);
            $this->stmt_bind_param($param['remarks']);
            $this->stmt_bind_param($param['applicant_interview_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_interview_id = ?");
            
            $this->stmt_bind_param($param['applicant_interview_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_id = ?");
            
            $this->stmt_bind_param($param['applicant_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_interview_id = ?");
        
        $this->stmt_bind_param($param['applicant_interview_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_interview_id = ? AND (applicant_interview_id != ?)");
        
        $this->stmt_bind_param($param['applicant_interview_id']);
        $this->stmt_bind_param($param['applicant_interview_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
