<?php
require_once 'iprf_staffrequest_dd.php';
class iprf_staffrequest_html extends html
{
    function __construct()
    {
        $this->fields        = iprf_staffrequest_dd::load_dictionary();
        $this->relations     = iprf_staffrequest_dd::load_relationships();
        $this->subclasses    = iprf_staffrequest_dd::load_subclass_info();
        $this->table_name    = iprf_staffrequest_dd::$table_name;
        $this->readable_name = iprf_staffrequest_dd::$readable_name;
    }
}
