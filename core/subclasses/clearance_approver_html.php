<?php
require_once 'clearance_approver_dd.php';
class clearance_approver_html extends html
{
    function __construct()
    {
        $this->fields        = clearance_approver_dd::load_dictionary();
        $this->relations     = clearance_approver_dd::load_relationships();
        $this->subclasses    = clearance_approver_dd::load_subclass_info();
        $this->table_name    = clearance_approver_dd::$table_name;
        $this->readable_name = clearance_approver_dd::$readable_name;
    }
}
