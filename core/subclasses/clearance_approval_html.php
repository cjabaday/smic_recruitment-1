<?php
require_once 'clearance_approval_dd.php';
class clearance_approval_html extends html
{
    function __construct()
    {
        $this->fields        = clearance_approval_dd::load_dictionary();
        $this->relations     = clearance_approval_dd::load_relationships();
        $this->subclasses    = clearance_approval_dd::load_subclass_info();
        $this->table_name    = clearance_approval_dd::$table_name;
        $this->readable_name = clearance_approval_dd::$readable_name;
    }
}
