<?php
require_once 'icivilstatus_dd.php';
class icivilstatus_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'icivilstatus_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'icivilstatus_html';
    var $data_subclass = 'icivilstatus';
    var $result_page = 'reporter_result_icivilstatus.php';
    var $cancel_page = 'listview_icivilstatus.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_icivilstatus.php';

    function __construct()
    {
        $this->fields        = icivilstatus_dd::load_dictionary();
        $this->relations     = icivilstatus_dd::load_relationships();
        $this->subclasses    = icivilstatus_dd::load_subclass_info();
        $this->table_name    = icivilstatus_dd::$table_name;
        $this->tables        = icivilstatus_dd::$table_name;
        $this->readable_name = icivilstatus_dd::$readable_name;
        $this->get_report_fields();
    }
}
