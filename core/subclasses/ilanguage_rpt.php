<?php
require_once 'ilanguage_dd.php';
class ilanguage_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'ilanguage_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'ilanguage_html';
    var $data_subclass = 'ilanguage';
    var $result_page = 'reporter_result_ilanguage.php';
    var $cancel_page = 'listview_ilanguage.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_ilanguage.php';

    function __construct()
    {
        $this->fields        = ilanguage_dd::load_dictionary();
        $this->relations     = ilanguage_dd::load_relationships();
        $this->subclasses    = ilanguage_dd::load_subclass_info();
        $this->table_name    = ilanguage_dd::$table_name;
        $this->tables        = ilanguage_dd::$table_name;
        $this->readable_name = ilanguage_dd::$readable_name;
        $this->get_report_fields();
    }
}
