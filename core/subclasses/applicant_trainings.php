<?php
require_once 'applicant_trainings_dd.php';
class applicant_trainings extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = applicant_trainings_dd::load_dictionary();
        $this->relations  = applicant_trainings_dd::load_relationships();
        $this->subclasses = applicant_trainings_dd::load_subclass_info();
        $this->table_name = applicant_trainings_dd::$table_name;
        $this->tables     = applicant_trainings_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('applicant_id, training_title, training_provider, training_date, venue, no_of_hours, remarks');
            $this->set_values("?,?,?,?,?,?,?");

            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['training_title']);
            $this->stmt_bind_param($param['training_provider']);
            $this->stmt_bind_param($param['training_date']);
            $this->stmt_bind_param($param['venue']);
            $this->stmt_bind_param($param['no_of_hours']);
            $this->stmt_bind_param($param['remarks']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("applicant_id = ?, training_title = ?, training_provider = ?, training_date = ?, venue = ?, no_of_hours = ?, remarks = ?");
            $this->set_where("applicant_training_id = ?");

            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['training_title']);
            $this->stmt_bind_param($param['training_provider']);
            $this->stmt_bind_param($param['training_date']);
            $this->stmt_bind_param($param['venue']);
            $this->stmt_bind_param($param['no_of_hours']);
            $this->stmt_bind_param($param['remarks']);
            $this->stmt_bind_param($param['applicant_training_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_training_id = ?");

            $this->stmt_bind_param($param['applicant_training_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_training_id = ?");

        $this->stmt_bind_param($param['applicant_training_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_training_id = ? AND (applicant_training_id != ?)");

        $this->stmt_bind_param($param['applicant_training_id']);
        $this->stmt_bind_param($param['applicant_training_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function select_record($applicant_id)
    {
        $this->set_where('applicant_id = ?');
        $this->stmt_bind_param($applicant_id);
        $this->stmt_fetch('rowdump');

        return $this;
    }
}
