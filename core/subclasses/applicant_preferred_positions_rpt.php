<?php
require_once 'applicant_preferred_positions_dd.php';
class applicant_preferred_positions_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_PREFERRED_POSITIONS_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_preferred_positions_html';
    var $data_subclass = 'applicant_preferred_positions';
    var $result_page = 'reporter_result_applicant_preferred_positions.php';
    var $cancel_page = 'listview_applicant_preferred_positions.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_preferred_positions.php';

    function __construct()
    {
        $this->fields        = applicant_preferred_positions_dd::load_dictionary();
        $this->relations     = applicant_preferred_positions_dd::load_relationships();
        $this->subclasses    = applicant_preferred_positions_dd::load_subclass_info();
        $this->table_name    = applicant_preferred_positions_dd::$table_name;
        $this->tables        = applicant_preferred_positions_dd::$table_name;
        $this->readable_name = applicant_preferred_positions_dd::$readable_name;
        $this->get_report_fields();
    }
}
