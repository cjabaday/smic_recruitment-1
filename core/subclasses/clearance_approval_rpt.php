<?php
require_once 'clearance_approval_dd.php';
class clearance_approval_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'CLEARANCE_APPROVAL_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'clearance_approval_html';
    var $data_subclass = 'clearance_approval';
    var $result_page = 'reporter_result_clearance_approval.php';
    var $cancel_page = 'listview_clearance_approval.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_clearance_approval.php';

    function __construct()
    {
        $this->fields        = clearance_approval_dd::load_dictionary();
        $this->relations     = clearance_approval_dd::load_relationships();
        $this->subclasses    = clearance_approval_dd::load_subclass_info();
        $this->table_name    = clearance_approval_dd::$table_name;
        $this->tables        = clearance_approval_dd::$table_name;
        $this->readable_name = clearance_approval_dd::$readable_name;
        $this->get_report_fields();
    }
}
