<?php
require_once 'documentation_class.php';
require_once 'forgot_pass_table_dd.php';
class forgot_pass_table_doc extends documentation
{
    function __construct()
    {
        $this->fields        = forgot_pass_table_dd::load_dictionary();
        $this->relations     = forgot_pass_table_dd::load_relationships();
        $this->subclasses    = forgot_pass_table_dd::load_subclass_info();
        $this->table_name    = forgot_pass_table_dd::$table_name;
        $this->readable_name = forgot_pass_table_dd::$readable_name;
        parent::__construct();
    }
}
