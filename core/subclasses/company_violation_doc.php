<?php
require_once 'documentation_class.php';
require_once 'company_violation_dd.php';
class company_violation_doc extends documentation
{
    function __construct()
    {
        $this->fields        = company_violation_dd::load_dictionary();
        $this->relations     = company_violation_dd::load_relationships();
        $this->subclasses    = company_violation_dd::load_subclass_info();
        $this->table_name    = company_violation_dd::$table_name;
        $this->readable_name = company_violation_dd::$readable_name;
        parent::__construct();
    }
}
