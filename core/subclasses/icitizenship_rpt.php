<?php
require_once 'icitizenship_dd.php';
class icitizenship_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'icitizenship_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'icitizenship_html';
    var $data_subclass = 'icitizenship';
    var $result_page = 'reporter_result_icitizenship.php';
    var $cancel_page = 'listview_icitizenship.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_icitizenship.php';

    function __construct()
    {
        $this->fields        = icitizenship_dd::load_dictionary();
        $this->relations     = icitizenship_dd::load_relationships();
        $this->subclasses    = icitizenship_dd::load_subclass_info();
        $this->table_name    = icitizenship_dd::$table_name;
        $this->tables        = icitizenship_dd::$table_name;
        $this->readable_name = icitizenship_dd::$readable_name;
        $this->get_report_fields();
    }
}
