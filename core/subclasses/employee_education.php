<?php
require_once 'employee_education_dd.php';
class employee_education extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = employee_education_dd::load_dictionary();
        $this->relations  = employee_education_dd::load_relationships();
        $this->subclasses = employee_education_dd::load_subclass_info();
        $this->table_name = employee_education_dd::$table_name;
        $this->tables     = employee_education_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('employee_id, level, school, period, awards');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['employee_id']);
            $this->stmt_bind_param($param['level']);
            $this->stmt_bind_param($param['school']);
            $this->stmt_bind_param($param['period']);
            $this->stmt_bind_param($param['awards']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("employee_id = ?, level = ?, school = ?, period = ?, awards = ?");
            $this->set_where("employee_education_id = ?");
            
            $this->stmt_bind_param($param['employee_id']);
            $this->stmt_bind_param($param['level']);
            $this->stmt_bind_param($param['school']);
            $this->stmt_bind_param($param['period']);
            $this->stmt_bind_param($param['awards']);
            $this->stmt_bind_param($param['employee_education_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("employee_education_id = ?");
            
            $this->stmt_bind_param($param['employee_education_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("employee_education_id = ?");
        
        $this->stmt_bind_param($param['employee_education_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("employee_education_id = ? AND (employee_education_id != ?)");
        
        $this->stmt_bind_param($param['employee_education_id']);
        $this->stmt_bind_param($param['employee_education_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
