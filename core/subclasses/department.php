<?php
require_once 'department_dd.php';
class department extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = department_dd::load_dictionary();
        $this->relations  = department_dd::load_relationships();
        $this->subclasses = department_dd::load_subclass_info();
        $this->table_name = department_dd::$table_name;
        $this->tables     = department_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('branch_id, floor_id, name, code, description, head, assistant');
            $this->set_values("?,?,?,?,?,?,?");

            $this->stmt_bind_param($param['branch_id']);
            $this->stmt_bind_param($param['floor_id']);
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['code']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['head']);
            $this->stmt_bind_param($param['assistant']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("branch_id = ?, floor_id = ?, name = ?, code = ?, description = ?, head = ?, assistant = ?");
            $this->set_where("department_id = ?");

            $this->stmt_bind_param($param['branch_id']);
            $this->stmt_bind_param($param['floor_id']);
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['code']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['head']);
            $this->stmt_bind_param($param['assistant']);
            $this->stmt_bind_param($param['department_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("department_id = ?");

            $this->stmt_bind_param($param['department_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("department_id = ?");

        $this->stmt_bind_param($param['department_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("department_id = ? AND (department_id != ?)");

        $this->stmt_bind_param($param['department_id']);
        $this->stmt_bind_param($param['department_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_label($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("name = ?");

        $this->stmt_bind_param($param['name']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function add_department(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('department_id, name');
            $this->set_values("?,?");

            $this->stmt_bind_param($param['department_id']);
            $this->stmt_bind_param($param['name']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }
}
