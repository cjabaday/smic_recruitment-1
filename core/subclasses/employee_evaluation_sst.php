<?php
require_once 'sst_class.php';
require_once 'employee_evaluation_dd.php';
class employee_evaluation_sst extends sst
{
    function __construct()
    {
        $this->fields        = employee_evaluation_dd::load_dictionary();
        $this->relations     = employee_evaluation_dd::load_relationships();
        $this->subclasses    = employee_evaluation_dd::load_subclass_info();
        $this->table_name    = employee_evaluation_dd::$table_name;
        $this->readable_name = employee_evaluation_dd::$readable_name;
        parent::__construct();
    }
}
