<?php
require_once 'sst_class.php';
require_once 'applicant_school_attended_dd.php';
class applicant_school_attended_sst extends sst
{
    function __construct()
    {
        $this->fields        = applicant_school_attended_dd::load_dictionary();
        $this->relations     = applicant_school_attended_dd::load_relationships();
        $this->subclasses    = applicant_school_attended_dd::load_subclass_info();
        $this->table_name    = applicant_school_attended_dd::$table_name;
        $this->readable_name = applicant_school_attended_dd::$readable_name;
        parent::__construct();
    }
}
