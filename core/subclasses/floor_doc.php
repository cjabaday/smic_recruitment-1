<?php
require_once 'documentation_class.php';
require_once 'floor_dd.php';
class floor_doc extends documentation
{
    function __construct()
    {
        $this->fields        = floor_dd::load_dictionary();
        $this->relations     = floor_dd::load_relationships();
        $this->subclasses    = floor_dd::load_subclass_info();
        $this->table_name    = floor_dd::$table_name;
        $this->readable_name = floor_dd::$readable_name;
        parent::__construct();
    }
}
