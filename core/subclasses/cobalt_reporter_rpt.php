<?php
require_once 'cobalt_reporter_dd.php';
class cobalt_reporter_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'COBALT_REPORTER_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'cobalt_reporter_html';
    var $data_subclass = 'cobalt_reporter';
    var $result_page = 'reporter_result_cobalt_reporter.php';
    var $cancel_page = 'listview_cobalt_reporter.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_cobalt_reporter.php';

    function __construct()
    {
        $this->fields        = cobalt_reporter_dd::load_dictionary();
        $this->relations     = cobalt_reporter_dd::load_relationships();
        $this->subclasses    = cobalt_reporter_dd::load_subclass_info();
        $this->table_name    = cobalt_reporter_dd::$table_name;
        $this->tables        = cobalt_reporter_dd::$table_name;
        $this->readable_name = cobalt_reporter_dd::$readable_name;
        $this->get_report_fields();
    }
}
