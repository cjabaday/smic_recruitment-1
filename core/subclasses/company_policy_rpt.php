<?php
require_once 'company_policy_dd.php';
class company_policy_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'COMPANY_POLICY_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'company_policy_html';
    var $data_subclass = 'company_policy';
    var $result_page = 'reporter_result_company_policy.php';
    var $cancel_page = 'listview_company_policy.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_company_policy.php';

    function __construct()
    {
        $this->fields        = company_policy_dd::load_dictionary();
        $this->relations     = company_policy_dd::load_relationships();
        $this->subclasses    = company_policy_dd::load_subclass_info();
        $this->table_name    = company_policy_dd::$table_name;
        $this->tables        = company_policy_dd::$table_name;
        $this->readable_name = company_policy_dd::$readable_name;
        $this->get_report_fields();
    }
}
