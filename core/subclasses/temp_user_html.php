<?php
require_once 'temp_user_dd.php';
class temp_user_html extends html
{
    function __construct()
    {
        $this->fields        = temp_user_dd::load_dictionary();
        $this->relations     = temp_user_dd::load_relationships();
        $this->subclasses    = temp_user_dd::load_subclass_info();
        $this->table_name    = temp_user_dd::$table_name;
        $this->readable_name = temp_user_dd::$readable_name;
    }
}
