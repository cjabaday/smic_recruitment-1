<?php
require_once 'applicant_exam_dd.php';
class applicant_exam_html extends html
{
    function __construct()
    {
        $this->fields        = applicant_exam_dd::load_dictionary();
        $this->relations     = applicant_exam_dd::load_relationships();
        $this->subclasses    = applicant_exam_dd::load_subclass_info();
        $this->table_name    = applicant_exam_dd::$table_name;
        $this->readable_name = applicant_exam_dd::$readable_name;
    }
}
