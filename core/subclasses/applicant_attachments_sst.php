<?php
require_once 'sst_class.php';
require_once 'applicant_attachments_dd.php';
class applicant_attachments_sst extends sst
{
    function __construct()
    {
        $this->fields        = applicant_attachments_dd::load_dictionary();
        $this->relations     = applicant_attachments_dd::load_relationships();
        $this->subclasses    = applicant_attachments_dd::load_subclass_info();
        $this->table_name    = applicant_attachments_dd::$table_name;
        $this->readable_name = applicant_attachments_dd::$readable_name;
        parent::__construct();
    }
}
