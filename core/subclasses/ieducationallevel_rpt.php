<?php
require_once 'ieducationallevel_dd.php';
class ieducationallevel_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'ieducationallevel_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'ieducationallevel_html';
    var $data_subclass = 'ieducationallevel';
    var $result_page = 'reporter_result_ieducationallevel.php';
    var $cancel_page = 'listview_ieducationallevel.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_ieducationallevel.php';

    function __construct()
    {
        $this->fields        = ieducationallevel_dd::load_dictionary();
        $this->relations     = ieducationallevel_dd::load_relationships();
        $this->subclasses    = ieducationallevel_dd::load_subclass_info();
        $this->table_name    = ieducationallevel_dd::$table_name;
        $this->tables        = ieducationallevel_dd::$table_name;
        $this->readable_name = ieducationallevel_dd::$readable_name;
        $this->get_report_fields();
    }
}
