<?php
require_once 'documentation_class.php';
require_once 'hr_program_attendees_dd.php';
class hr_program_attendees_doc extends documentation
{
    function __construct()
    {
        $this->fields        = hr_program_attendees_dd::load_dictionary();
        $this->relations     = hr_program_attendees_dd::load_relationships();
        $this->subclasses    = hr_program_attendees_dd::load_subclass_info();
        $this->table_name    = hr_program_attendees_dd::$table_name;
        $this->readable_name = hr_program_attendees_dd::$readable_name;
        parent::__construct();
    }
}
