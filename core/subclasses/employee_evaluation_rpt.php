<?php
require_once 'employee_evaluation_dd.php';
class employee_evaluation_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'EMPLOYEE_EVALUATION_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'employee_evaluation_html';
    var $data_subclass = 'employee_evaluation';
    var $result_page = 'reporter_result_employee_evaluation.php';
    var $cancel_page = 'listview_employee_evaluation.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_employee_evaluation.php';

    function __construct()
    {
        $this->fields        = employee_evaluation_dd::load_dictionary();
        $this->relations     = employee_evaluation_dd::load_relationships();
        $this->subclasses    = employee_evaluation_dd::load_subclass_info();
        $this->table_name    = employee_evaluation_dd::$table_name;
        $this->tables        = employee_evaluation_dd::$table_name;
        $this->readable_name = employee_evaluation_dd::$readable_name;
        $this->get_report_fields();
    }
}
