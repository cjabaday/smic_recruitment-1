<?php
require_once 'cobalt_reporter_dd.php';
class cobalt_reporter extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = cobalt_reporter_dd::load_dictionary();
        $this->relations  = cobalt_reporter_dd::load_relationships();
        $this->subclasses = cobalt_reporter_dd::load_subclass_info();
        $this->table_name = cobalt_reporter_dd::$table_name;
        $this->tables     = cobalt_reporter_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('module_name, report_name, username, show_field, hidden_field, operator, text_field, sum_field, count_field, group_field1, group_field2, group_field3');
            $this->set_values("?,?,?,?,?,?,?,?,?,?,?,?");
            
            $this->stmt_bind_param($param['module_name']);
            $this->stmt_bind_param($param['report_name']);
            $this->stmt_bind_param($param['username']);
            $this->stmt_bind_param($param['show_field']);
            $this->stmt_bind_param($param['hidden_field']);
            $this->stmt_bind_param($param['operator']);
            $this->stmt_bind_param($param['text_field']);
            $this->stmt_bind_param($param['sum_field']);
            $this->stmt_bind_param($param['count_field']);
            $this->stmt_bind_param($param['group_field1']);
            $this->stmt_bind_param($param['group_field2']);
            $this->stmt_bind_param($param['group_field3']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("module_name = ?, report_name = ?, username = ?, show_field = ?, hidden_field = ?, operator = ?, text_field = ?, sum_field = ?, count_field = ?, group_field1 = ?, group_field2 = ?, group_field3 = ?");
            $this->set_where("module_name = ? AND report_name = ?");
            
            $this->stmt_bind_param($param['module_name']);
            $this->stmt_bind_param($param['report_name']);
            $this->stmt_bind_param($param['username']);
            $this->stmt_bind_param($param['show_field']);
            $this->stmt_bind_param($param['hidden_field']);
            $this->stmt_bind_param($param['operator']);
            $this->stmt_bind_param($param['text_field']);
            $this->stmt_bind_param($param['sum_field']);
            $this->stmt_bind_param($param['count_field']);
            $this->stmt_bind_param($param['group_field1']);
            $this->stmt_bind_param($param['group_field2']);
            $this->stmt_bind_param($param['group_field3']);
            $this->stmt_bind_param($param['orig_module_name']);
            $this->stmt_bind_param($param['orig_report_name']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("module_name = ? AND report_name = ?");
            
            $this->stmt_bind_param($param['module_name']);
            $this->stmt_bind_param($param['report_name']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("module_name = ? AND report_name = ?");
            
            $this->stmt_bind_param($param['module_name']);
            $this->stmt_bind_param($param['report_name']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("module_name = ? AND report_name = ?");
        
        $this->stmt_bind_param($param['module_name']);
        $this->stmt_bind_param($param['report_name']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("module_name = ? AND report_name = ? AND (module_name != ? OR report_name != ?)");
        
        $this->stmt_bind_param($param['module_name']);
        $this->stmt_bind_param($param['report_name']);
        $this->stmt_bind_param($param['orig_module_name']);
        $this->stmt_bind_param($param['orig_report_name']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
