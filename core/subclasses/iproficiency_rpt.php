<?php
require_once 'iproficiency_dd.php';
class iproficiency_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'iproficiency_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'iproficiency_html';
    var $data_subclass = 'iproficiency';
    var $result_page = 'reporter_result_iproficiency.php';
    var $cancel_page = 'listview_iproficiency.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_iproficiency.php';

    function __construct()
    {
        $this->fields        = iproficiency_dd::load_dictionary();
        $this->relations     = iproficiency_dd::load_relationships();
        $this->subclasses    = iproficiency_dd::load_subclass_info();
        $this->table_name    = iproficiency_dd::$table_name;
        $this->tables        = iproficiency_dd::$table_name;
        $this->readable_name = iproficiency_dd::$readable_name;
        $this->get_report_fields();
    }
}
