<?php
require_once 'clearance_dd.php';
class clearance_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'CLEARANCE_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'clearance_html';
    var $data_subclass = 'clearance';
    var $result_page = 'reporter_result_clearance.php';
    var $cancel_page = 'listview_clearance.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_clearance.php';

    function __construct()
    {
        $this->fields        = clearance_dd::load_dictionary();
        $this->relations     = clearance_dd::load_relationships();
        $this->subclasses    = clearance_dd::load_subclass_info();
        $this->table_name    = clearance_dd::$table_name;
        $this->tables        = clearance_dd::$table_name;
        $this->readable_name = clearance_dd::$readable_name;
        $this->get_report_fields();
    }
}
