<?php
require_once 'documentation_class.php';
require_once 'applicant_family_members_dd.php';
class applicant_family_members_doc extends documentation
{
    function __construct()
    {
        $this->fields        = applicant_family_members_dd::load_dictionary();
        $this->relations     = applicant_family_members_dd::load_relationships();
        $this->subclasses    = applicant_family_members_dd::load_subclass_info();
        $this->table_name    = applicant_family_members_dd::$table_name;
        $this->readable_name = applicant_family_members_dd::$readable_name;
        parent::__construct();
    }
}
