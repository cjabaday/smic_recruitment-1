<?php
require_once 'documentation_class.php';
require_once 'applicant_reference_dd.php';
class applicant_reference_doc extends documentation
{
    function __construct()
    {
        $this->fields        = applicant_reference_dd::load_dictionary();
        $this->relations     = applicant_reference_dd::load_relationships();
        $this->subclasses    = applicant_reference_dd::load_subclass_info();
        $this->table_name    = applicant_reference_dd::$table_name;
        $this->readable_name = applicant_reference_dd::$readable_name;
        parent::__construct();
    }
}
