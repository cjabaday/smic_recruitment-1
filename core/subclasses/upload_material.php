<?php
require_once 'upload_material_dd.php';
class upload_material extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = upload_material_dd::load_dictionary();
        $this->relations  = upload_material_dd::load_relationships();
        $this->subclasses = upload_material_dd::load_subclass_info();
        $this->table_name = upload_material_dd::$table_name;
        $this->tables     = upload_material_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('file_name, date_uploaded, active');
            $this->set_values("?,?,?");
            
            $this->stmt_bind_param($param['file_name']);
            $this->stmt_bind_param($param['date_uploaded']);
            $this->stmt_bind_param($param['active']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("file_name = ?, date_uploaded = ?, active = ?");
            $this->set_where("upload_material_id = ?");
            
            $this->stmt_bind_param($param['file_name']);
            $this->stmt_bind_param($param['date_uploaded']);
            $this->stmt_bind_param($param['active']);
            $this->stmt_bind_param($param['upload_material_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("upload_material_id = ?");
            
            $this->stmt_bind_param($param['upload_material_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("upload_material_id = ?");
        
        $this->stmt_bind_param($param['upload_material_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("upload_material_id = ? AND (upload_material_id != ?)");
        
        $this->stmt_bind_param($param['upload_material_id']);
        $this->stmt_bind_param($param['upload_material_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
