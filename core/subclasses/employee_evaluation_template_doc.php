<?php
require_once 'documentation_class.php';
require_once 'employee_evaluation_template_dd.php';
class employee_evaluation_template_doc extends documentation
{
    function __construct()
    {
        $this->fields        = employee_evaluation_template_dd::load_dictionary();
        $this->relations     = employee_evaluation_template_dd::load_relationships();
        $this->subclasses    = employee_evaluation_template_dd::load_subclass_info();
        $this->table_name    = employee_evaluation_template_dd::$table_name;
        $this->readable_name = employee_evaluation_template_dd::$readable_name;
        parent::__construct();
    }
}
