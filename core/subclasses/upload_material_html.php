<?php
require_once 'upload_material_dd.php';
class upload_material_html extends html
{
    function __construct()
    {
        $this->fields        = upload_material_dd::load_dictionary();
        $this->relations     = upload_material_dd::load_relationships();
        $this->subclasses    = upload_material_dd::load_subclass_info();
        $this->table_name    = upload_material_dd::$table_name;
        $this->readable_name = upload_material_dd::$readable_name;
    }
}
