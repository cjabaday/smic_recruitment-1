<?php
require_once 'forgot_pass_table_dd.php';
class forgot_pass_table extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = forgot_pass_table_dd::load_dictionary();
        $this->relations  = forgot_pass_table_dd::load_relationships();
        $this->subclasses = forgot_pass_table_dd::load_subclass_info();
        $this->table_name = forgot_pass_table_dd::$table_name;
        $this->tables     = forgot_pass_table_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('email_address, token, date_applied');
            $this->set_values("?,?,?");
            
            $this->stmt_bind_param($param['email_address']);
            $this->stmt_bind_param($param['token']);
            $this->stmt_bind_param($param['date_applied']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("email_address = ?, token = ?, date_applied = ?");
            $this->set_where("forgot_pass_id = ?");
            
            $this->stmt_bind_param($param['email_address']);
            $this->stmt_bind_param($param['token']);
            $this->stmt_bind_param($param['date_applied']);
            $this->stmt_bind_param($param['forgot_pass_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("forgot_pass_id = ?");
            
            $this->stmt_bind_param($param['forgot_pass_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("forgot_pass_id = ?");
        
        $this->stmt_bind_param($param['forgot_pass_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("forgot_pass_id = ? AND (forgot_pass_id != ?)");
        
        $this->stmt_bind_param($param['forgot_pass_id']);
        $this->stmt_bind_param($param['forgot_pass_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
