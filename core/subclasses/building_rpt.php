<?php
require_once 'building_dd.php';
class building_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'BUILDING_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'building_html';
    var $data_subclass = 'building';
    var $result_page = 'reporter_result_building.php';
    var $cancel_page = 'listview_building.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_building.php';

    function __construct()
    {
        $this->fields        = building_dd::load_dictionary();
        $this->relations     = building_dd::load_relationships();
        $this->subclasses    = building_dd::load_subclass_info();
        $this->table_name    = building_dd::$table_name;
        $this->tables        = building_dd::$table_name;
        $this->readable_name = building_dd::$readable_name;
        $this->get_report_fields();
    }
}
