<?php
require_once 'user_dd.php';
class user extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = user_dd::load_dictionary();
        $this->relations  = user_dd::load_relationships();
        $this->subclasses = user_dd::load_subclass_info();
        $this->table_name = user_dd::$table_name;
        $this->tables     = user_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('username, password, salt, iteration, method, applicant_id, role_id, skin_id, user_level');
            $this->set_values("?,?,?,?,?,?,?,?,?");

            $this->stmt_bind_param($param['username']);
            $this->stmt_bind_param($param['password']);
            $this->stmt_bind_param($param['salt']);
            $this->stmt_bind_param($param['iteration']);
            $this->stmt_bind_param($param['method']);
            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['role_id']);
            $this->stmt_bind_param($param['skin_id']);
            $this->stmt_bind_param($param['user_level']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("username = ?, applicant_id = ?, role_id = ?, skin_id = ?, user_level = ?");
            $this->set_where("username = ?");

            $this->stmt_bind_param($param['username']);
            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['role_id']);
            $this->stmt_bind_param($param['skin_id']);
            $this->stmt_bind_param($param['user_level']);
            $this->stmt_bind_param($param['orig_username']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("username = ?");

            $this->stmt_bind_param($param['username']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("username = ?");

            $this->stmt_bind_param($param['username']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("username = ?");

        $this->stmt_bind_param($param['username']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("username = ? AND (username != ?)");

        $this->stmt_bind_param($param['username']);
        $this->stmt_bind_param($param['orig_username']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_user($username)
    {
        $this->set_query_type('SELECT');
        $this->set_fields("username");
        $this->set_where("username = ?");
        $this->stmt_bind_param($username);
        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows == 1) $this->user_exists = TRUE;
        else $this->user_exists = FALSE;

        return $this;
    }

    function check_level($username)
    {
        $this->set_query_type('SELECT');
        $this->set_fields("user_level");
        $this->set_where("username = ?");
        $this->stmt_bind_param($username);
        $this->stmt_prepare();
        $this->stmt_execute()->stmt_fetch('single')->stmt_close();

        return $this;
    }

    function get_role_users($role_id)
    {
        $this->set_query_type('SELECT');
        $this->set_fields("username");
        $this->set_where("role_id = ?");

        $this->stmt_bind_param($role_id, 'i');

        $this->stmt_prepare();
        $this->stmt_execute()->stmt_fetch()->stmt_close();

        $this->lst_user = '';
        if(isset($this->dump['username']) && is_array($this->dump['username']))
        {
            foreach($this->dump['username'] as $user)
            {
                make_list($this->lst_user, $user);
            }
        }

        return $this;
    }

    function registration($param)
    {

        $this->set_query_type('INSERT');
        $this->set_fields('username, personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified, token');
        $this->set_values("?,?,?,?,?,?,?,?,?,?,?,?");

        $this->stmt_bind_param($param['username']);
        $this->stmt_bind_param($param['personal_email']);
        $this->stmt_bind_param($param['password']);
        $this->stmt_bind_param($param['salt']);
        $this->stmt_bind_param($param['iteration']);
        $this->stmt_bind_param($param['method']);
        $this->stmt_bind_param($param['role_id']);
        $this->stmt_bind_param($param['skin_id']);
        $this->stmt_bind_param($param['user_level']);
        $this->stmt_bind_param($param['applicant_id']);
        $this->stmt_bind_param($param['is_verified']);
        $this->stmt_bind_param($param['token']);

        $this->stmt_prepare();

        $this->stmt_execute();
        return $this;
    }

    function verify_account($id, $token)
    {

        $verification = 'Yes';
        $this->set_query_type('UPDATE');
        $this->set_update("is_verified = ?");
        $this->set_where("applicant_id = ? AND token = ?");

        $this->stmt_bind_param($verification);
        $this->stmt_bind_param($id);
        $this->stmt_bind_param($token);

        $this->stmt_prepare();

        $this->stmt_execute();
        return $this;
    }

    function password_reset($param)
    {

        $this->set_query_type('UPDATE');
        $this->set_update("password = ?, salt = ?, iteration = ?, method = ?");
        $this->set_where("username = ?");

        $this->stmt_bind_param($param['password']);
        $this->stmt_bind_param($param['salt']);
        $this->stmt_bind_param($param['iteration']);
        $this->stmt_bind_param($param['method']);
        $this->stmt_bind_param($param['username']);

        $this->stmt_prepare();

        $this->stmt_execute();
        return $this;
    }
}
