<?php
require_once 'icivilstatus_dd.php';
class icivilstatus extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = icivilstatus_dd::load_dictionary();
        $this->relations  = icivilstatus_dd::load_relationships();
        $this->subclasses = icivilstatus_dd::load_subclass_info();
        $this->table_name = icivilstatus_dd::$table_name;
        $this->tables     = icivilstatus_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('CivilStatusID, CivilStatusCode, CivilStatusDesc, EncodeBy, EncodeDate');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['CivilStatusID']);
            $this->stmt_bind_param($param['CivilStatusCode']);
            $this->stmt_bind_param($param['CivilStatusDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("CivilStatusID = ?, CivilStatusCode = ?, CivilStatusDesc = ?, EncodeBy = ?, EncodeDate = ?");
            $this->set_where("CivilStatusID = ?");
            
            $this->stmt_bind_param($param['CivilStatusID']);
            $this->stmt_bind_param($param['CivilStatusCode']);
            $this->stmt_bind_param($param['CivilStatusDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);
            $this->stmt_bind_param($param['orig_CivilStatusID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("CivilStatusID = ?");
            
            $this->stmt_bind_param($param['CivilStatusID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("CivilStatusID = ?");
            
            $this->stmt_bind_param($param['CivilStatusID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("CivilStatusID = ?");
        
        $this->stmt_bind_param($param['CivilStatusID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("CivilStatusID = ? AND (CivilStatusID != ?)");
        
        $this->stmt_bind_param($param['CivilStatusID']);
        $this->stmt_bind_param($param['orig_CivilStatusID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
