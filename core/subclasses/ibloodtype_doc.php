<?php
require_once 'documentation_class.php';
require_once 'ibloodtype_dd.php';
class ibloodtype_doc extends documentation
{
    function __construct()
    {
        $this->fields        = ibloodtype_dd::load_dictionary();
        $this->relations     = ibloodtype_dd::load_relationships();
        $this->subclasses    = ibloodtype_dd::load_subclass_info();
        $this->table_name    = ibloodtype_dd::$table_name;
        $this->readable_name = ibloodtype_dd::$readable_name;
        parent::__construct();
    }
}
