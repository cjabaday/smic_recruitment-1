<?php
require_once 'action_notice_dd.php';
class action_notice_html extends html
{
    function __construct()
    {
        $this->fields        = action_notice_dd::load_dictionary();
        $this->relations     = action_notice_dd::load_relationships();
        $this->subclasses    = action_notice_dd::load_subclass_info();
        $this->table_name    = action_notice_dd::$table_name;
        $this->readable_name = action_notice_dd::$readable_name;
    }
}
