<?php
require_once 'plantilla_dd.php';
class plantilla extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = plantilla_dd::load_dictionary();
        $this->relations  = plantilla_dd::load_relationships();
        $this->subclasses = plantilla_dd::load_subclass_info();
        $this->table_name = plantilla_dd::$table_name;
        $this->tables     = plantilla_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('parent_plantilla_id, branch_id, department_id, company_id, position_title, job_description, skill_requirements, recruitment_notes');
            $this->set_values("?,?,?,?,?,?,?,?");
            
            $this->stmt_bind_param($param['parent_plantilla_id']);
            $this->stmt_bind_param($param['branch_id']);
            $this->stmt_bind_param($param['department_id']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['position_title']);
            $this->stmt_bind_param($param['job_description']);
            $this->stmt_bind_param($param['skill_requirements']);
            $this->stmt_bind_param($param['recruitment_notes']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("parent_plantilla_id = ?, branch_id = ?, department_id = ?, company_id = ?, position_title = ?, job_description = ?, skill_requirements = ?, recruitment_notes = ?");
            $this->set_where("plantilla_id = ?");
            
            $this->stmt_bind_param($param['parent_plantilla_id']);
            $this->stmt_bind_param($param['branch_id']);
            $this->stmt_bind_param($param['department_id']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['position_title']);
            $this->stmt_bind_param($param['job_description']);
            $this->stmt_bind_param($param['skill_requirements']);
            $this->stmt_bind_param($param['recruitment_notes']);
            $this->stmt_bind_param($param['plantilla_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("plantilla_id = ?");
            
            $this->stmt_bind_param($param['plantilla_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("plantilla_id = ?");
        
        $this->stmt_bind_param($param['plantilla_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("plantilla_id = ? AND (plantilla_id != ?)");
        
        $this->stmt_bind_param($param['plantilla_id']);
        $this->stmt_bind_param($param['plantilla_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
