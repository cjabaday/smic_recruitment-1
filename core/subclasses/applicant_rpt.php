<?php
require_once 'applicant_dd.php';
class applicant_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_html';
    var $data_subclass = 'applicant';
    var $result_page = 'reporter_result_applicant.php';
    var $cancel_page = 'listview_applicant.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant.php';

    function __construct()
    {
        $this->fields        = applicant_dd::load_dictionary();
        $this->relations     = applicant_dd::load_relationships();
        $this->subclasses    = applicant_dd::load_subclass_info();
        $this->table_name    = applicant_dd::$table_name;
        $this->tables        = applicant_dd::$table_name;
        $this->readable_name = applicant_dd::$readable_name;
        $this->get_report_fields();
    }
}
