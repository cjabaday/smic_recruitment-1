<?php
require_once 'person_dd.php';
class person extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = person_dd::load_dictionary();
        $this->relations  = person_dd::load_relationships();
        $this->subclasses = person_dd::load_subclass_info();
        $this->table_name = person_dd::$table_name;
        $this->tables     = person_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('first_name, middle_name, last_name, gender');
            $this->set_values("?,?,?,?");
            
            $this->stmt_bind_param($param['first_name']);
            $this->stmt_bind_param($param['middle_name']);
            $this->stmt_bind_param($param['last_name']);
            $this->stmt_bind_param($param['gender']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("first_name = ?, middle_name = ?, last_name = ?, gender = ?");
            $this->set_where("person_id = ?");
            
            $this->stmt_bind_param($param['first_name']);
            $this->stmt_bind_param($param['middle_name']);
            $this->stmt_bind_param($param['last_name']);
            $this->stmt_bind_param($param['gender']);
            $this->stmt_bind_param($param['person_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("person_id = ?");
            
            $this->stmt_bind_param($param['person_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("person_id = ?");
        
        $this->stmt_bind_param($param['person_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("person_id = ? AND (person_id != ?)");
        
        $this->stmt_bind_param($param['person_id']);
        $this->stmt_bind_param($param['person_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
