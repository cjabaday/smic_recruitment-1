<?php
require_once 'ibloodtype_dd.php';
class ibloodtype_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'ibloodtype_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'ibloodtype_html';
    var $data_subclass = 'ibloodtype';
    var $result_page = 'reporter_result_ibloodtype.php';
    var $cancel_page = 'listview_ibloodtype.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_ibloodtype.php';

    function __construct()
    {
        $this->fields        = ibloodtype_dd::load_dictionary();
        $this->relations     = ibloodtype_dd::load_relationships();
        $this->subclasses    = ibloodtype_dd::load_subclass_info();
        $this->table_name    = ibloodtype_dd::$table_name;
        $this->tables        = ibloodtype_dd::$table_name;
        $this->readable_name = ibloodtype_dd::$readable_name;
        $this->get_report_fields();
    }
}
