<?php
function new_window($location)
{
    echo '<script>' . "\r\n";
    echo 'window.open("' . $location . '")' . "\r\n";
    echo '</script>' . "\r\n";
}

function set_datecontrol_values(&$year, &$month, &$day, $offset=0, $offset_type='m')
{
    $adjusted_date = date("m-d-Y", mktime(0, 0, 0,
                                          $month + $m_offset,
                                          $day + $d_offset,
                                          $year + $y_offset));

    $data = explode('-', $adjusted_date);
    $month = $data[0];
    $day = $data[1];
    $year = $data[2];
}

function log_action_for($action, $module='')
{
    if(isset($_SESSION['user']))
    {
        $username = quote_smart($_SESSION['user']);
    }
    else
    {
        $username = 'Not Logged In';
    }

    if($module=='')
    {
        $module = $_SERVER['SCRIPT_NAME'];
    }

    $date = date("m-d-Y");
    $real_time = date("G:i:s");
    $new_date= explode("-", $date);
    $new_time= explode(":", $real_time);
    $datetime = date('Y-m-d H:i:s');
    $ip_address = get_ip();
    $action = quote_smart($action);

    $data_con = new data_abstraction;
    $data_con->set_query_type('INSERT');
    $data_con->set_table('system_log');
    $data_con->set_fields('ip_address, user, datetime, action, module');
    $data_con->set_values("'$ip_address', '$username', '$datetime', '$action', '$module'");
    $data_con->make_query(TRUE,FALSE);
}
