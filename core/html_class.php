<?php
require 'base_html_class.php';
class html extends base_html
{
    function draw_file_upload($cobalt_text_field_label, $tf_control_name='', $detail_view=FALSE, $draw_table_tags=TRUE, $extra='')
    {

        // debug($detail_view);
        if($tf_control_name=='') $tf_control_name=$cobalt_text_field_label;

        global $$tf_control_name;

        if($draw_table_tags)
        {
            echo '<tr><td class="label">' . $cobalt_text_field_label . ':</td><td>' . "\r\n";
        }

        $value = cobalt_htmlentities($$tf_control_name);

        //this removes the prepended token on the filename, resulting in the original filename as the displayed name
        $display_name = cobalt_htmlentities(substr($value, $this->upload_token_length));

        if($detail_view==FALSE)
        {
            echo '<div class="file_upload">';

            if($this->upload_show_file)
            {
                if($this->upload_enable_link)
                {
                    if($display_name != '')
                    {

                        // add these code for image preview
                        echo '<img src="/' . BASE_DIRECTORY .'/tmp/'. urlencode($value) . '" style="max-height: 100px; max-width: 100px">';
                        //---------------------------------

                        // echo '<a href="/' . BASE_DIRECTORY . '/' . $this->upload_downloader . '?filename=' . urlencode($value) . '">' . $display_name . '</a>&nbsp;';
                    }
                }
                else
                {
                    echo $display_name . '&nbsp;';
                }
            }

            require 'components/get_max_attachment_size.php';
            //echo '<input type="hidden" name="MAX_FILE_SIZE" value="' . $max_attachment_size . '">';

            ++$this->tabindex;
            echo '<input type="file" id="' . $tf_control_name . '" name="' . $tf_control_name . '" tabindex="' . $this->tabindex . '" ' . $extra . '>';
            echo '<input type="hidden" name="existing_' . $tf_control_name . '" value="' . $value . '">';
            echo '</div>';
        }
        else
        {
            if(trim($value)=='')
            {
                echo '<p class="detail_view">[No file uploaded]</p>';
            }
            else
            {
                // add these code for image preview
                echo '<img src="/' . BASE_DIRECTORY .'/tmp/'. urlencode($value) . '" style="max-height: 150px; max-width:150px">';
                echo '<p class="detail_view"><a href="/' . BASE_DIRECTORY . '/' . $this->upload_downloader . '?filename=' . rawurlencode($value) . '">' . $display_name . '</a></p>' . "\r\n";
                //-----------------

            }
        }
        if($draw_table_tags) echo '</td></tr>' . "\r\n";
    }

    function draw_controls_add_line($type='', $title='')
    {
        $this->draw_controls_start($type, $title, FALSE);

        //if no fieldsets defined, then default fieldset is all fields in DD, minus those in $this->exception
        if(empty($this->fieldsets))
        {
            foreach($this->fields as $field_name=>$field_struct)
            {
                if(!in_array($field_name,$this->exception))
                {
                    $this->fieldsets['default'][] = $field_name;
                }
            }
        }

        $arr_drawn_multifields = array();
        $first_field = '';
        foreach($this->fieldsets as $fieldset=>$fields)
        {
            if(substr($fieldset, 0, 6) == '__mf__')
            {
                //This is a multifield, not a regular field
                if(isset($this->bound_objects[$fields]))
                {
                    $subclass = $this->bound_objects[$fields];
                }
                else
                {
                    $subclass = cobalt_load_class($fields . '_html');
                }
                $subclass->detail_view = $this->detail_view;
                $subclass->tabindex = $this->tabindex;
                $subclass->draw_controls_mf_add_line();
                $this->tabindex = $subclass->tabindex;
                $arr_drawn_multifields[] = $fields;
            }
            else
            {
                $anchor_tag = str_replace(' ', '_', $fieldset);
                if($fieldset != 'default' and !is_numeric($fieldset))
                {
                    echo '<fieldset  class="fieldset_group"><legend onClick="toggle_fieldset(\'' . $anchor_tag . '\')"">' . $fieldset . '</legend>' . "\r\n";;
                }
                echo '<table class="input_form" id="' . $anchor_tag . '">' . "\r\n";

                foreach($fields as $field_name)
                {
                    if(!in_array($field_name,$this->exception))
                    {
                        $this->draw_field($field_name, TRUE);

                        if($first_field == '')
                        {
                            $first_field_type = $this->fields[$field_name]['control_type'];

                            if($first_field_type == 'none' OR $first_field_type == 'hidden' OR
                            strpos($this->fields[$field_name]['extra'], 'readonly') !== FALSE)
                            {
                                //ignore
                            }
                            else
                            {
                                if($first_field_type == 'date controls')
                                {
                                    $first_field = $this->fields[$field_name]['date_elements'][1]; //get month field
                                }
                                elseif($first_field_type == 'radio buttons')
                                {
                                    $first_field = $field_name . '[0]';
                                }
                                else
                                {
                                    $first_field = $field_name;
                                }
                            }
                        }
                    }
                }

                echo '</table>' . "\r\n";

                if($fieldset != 'default' and !is_numeric($fieldset))
                {
                    echo '</fieldset>' . "\r\n";;
                }
                echo '<br />';
            }
        }

        //determine if multifields need to be drawn
        foreach($this->relations as $rel_info)
        {
            if($rel_info['type'] == '1-M')
            {
                if(!in_array($rel_info['table'], $arr_drawn_multifields))
                {
                    if(isset($this->bound_objects[$rel_info['table']]))
                    {
                        $subclass = $this->bound_objects[$rel_info['table']];
                    }
                    else
                    {
                        $subclass = cobalt_load_class($rel_info['table'] . '_html');
                    }
                    $subclass->detail_view = $this->detail_view;
                    $subclass->tabindex = $this->tabindex;
                    $subclass->draw_controls_mf_add_line();
                    $this->tabindex = $subclass->tabindex;
                }
            }
        }

        $this->draw_controls_end($first_field, FALSE);

        return $this;
    }

    function draw_controls_mf_add_line($title='')
    {
        if(empty($this->field_from_parent))
        {
            foreach($this->relations as $rel_info)
            {
                if($rel_info['type'] == 'M-1')
                {
                    $this->field_from_parent = $rel_info['link_child'];
                }
            }
        }

        $arr_labels=array();
        $arr_controls=array();
        $arr_parameters=array();
        foreach($this->fields as $field_name=>$field_struct)
        {
            if(empty($this->field_from_parent) || $field_name != $this->field_from_parent)
            {
                $cf_name    = 'cf_' . $this->table_name . '_' . $field_name;
                $size       = $field_struct['size'];
                $extra      = $field_struct['extra'];
                $allow_html = $field_struct['allow_html_tags'];
                switch($field_struct['control_type'])
                {
                    case 'date controls'    :   $arr_labels[] = $field_struct['label'];
                                                $arr_controls[] = 'draw_date_field_mf';
                                                $arr_parameters[] = array('cf_' . $this->table_name . '_' . $field_struct['date_elements'][0],
                                                                          'cf_' . $this->table_name . '_' . $field_struct['date_elements'][1],
                                                                          'cf_' . $this->table_name . '_' . $field_struct['date_elements'][2],
                                                                          $this->year_set,
                                                                          $field_name);
                                                break;

                    case 'time controls'    :   $arr_labels[] = $field_struct['label'];
                                                $arr_controls[] = 'draw_time_field_mf';
                                                $arr_parameters[] = array('cf_' . $this->table_name . '_' . $field_struct['date_elements'][0],
                                                                          'cf_' . $this->table_name . '_' . $field_struct['date_elements'][1],
                                                                          $field_name);
                                                break;

                    case 'radio buttons'    :
                    case 'drop-down list'   :   $arr_labels[] = $field_struct['label'];
                                                if($field_struct['list_type'] == 'predefined')
                                                {
                                                    $arr_controls[] = 'draw_select_field_mf';
                                                }
                                                else
                                                {
                                                    $arr_controls[] = 'draw_select_field_from_query_mf';
                                                }
                                                $arr_parameters[] = array($field_struct['list_settings'], $cf_name, $extra);
                                                break;

                    case 'password'         :   $arr_labels[] = $field_struct['label'];
                                                $arr_controls[] = 'draw_text_field_mf';
                                                if($size > 0)
                                                {
                                                    $extra = ' size="' . $size . '" ' . $extra;
                                                }
                                                if($field_struct['length'] > 0) $extra .= ' maxlength="' . $field_struct['length'] . '" ';
                                                $arr_parameters[] = array($cf_name,'password',$extra, $allow_html);
                                                break;

                    case 'textbox'          :   $arr_labels[] = $field_struct['label'];
                                                $arr_controls[] = 'draw_text_field_mf';
                                                if($size > 0)
                                                {
                                                    $extra = ' size="' . $size . '" ' . $extra;
                                                }
                                                if($field_struct['length'] > 0) $extra .= ' maxlength="' . $field_struct['length'] . '" ';
                                                $arr_parameters[] = array($cf_name,'text',$extra, $allow_html);
                                                break;

                    case 'textarea'         :   $arr_labels[] = $field_struct['label'];
                                                $arr_controls[] = 'draw_text_field_mf';
                                                if($size != '')
                                                {
                                                    $arr_size = explode(';', $size);
                                                    if($arr_size[0] > 0)
                                                    {
                                                        $extra = ' cols="' . $arr_size[0] . '" ' . $extra;
                                                    }
                                                    if(isset($arr_size[1]) && $arr_size[1] > 0)
                                                    {
                                                        $extra = ' rows="' . $arr_size[1] . '" ' . $extra;
                                                    }
                                                }
                                                if($field_struct['length'] > 0) $extra .= ' maxlength="' . $field_struct['length'] . '" ';
                                                $arr_parameters[] = array($cf_name,'textarea',$extra, $allow_html);
                                                break;

                    case 'upload'           :   $arr_labels[] = $field_struct['label'];
                                                $arr_controls[] = 'draw_file_upload_mf';
                                                $arr_parameters[] = array($cf_name, $extra);
                                                break;

                    default                 :   break;

                }
            }
        }

        $multifield_settings = array('field_labels'    => $arr_labels,
                                     'field_controls'  => $arr_controls,
                                     'field_parameters'=> $arr_parameters);

        if(isset($this->mf_label))
        {
            $title = $this->mf_label;
        }
        else
        {
            $title = $this->readable_name;
        }
        $num_var = 'num_' . $this->table_name;
        $count_var = $this->table_name . '_count';
        $button_var = 'btn_mf_' . $this->table_name;
        global $$num_var, $$count_var;

        $this->draw_multifield_auto_add_line($title, $multifield_settings, $num_var, $count_var, $button_var);
    }

    function draw_multifield_auto_add_line($label, $arr_multifield, $num_particulars_var=null, $particulars_count_var=null, $particular_button_var=null, $particular_button_deduct=null)
    {
        // debug();
        if($num_particulars_var==null)
        {
            $num_particulars_var='num_particulars';
        }
        if($particulars_count_var==null)
        {
            $particulars_count_var='particulars_count';
        }
        if($particular_button_var==null)
        {
            $particular_button_var='particular_button';
        }
        if($particular_button_deduct==null)
        {
            $particular_button_deduct='particular_button_deduct';
        }

        global $$num_particulars_var, $$particulars_count_var;

        //Get minimum according to DD
        $minimum=0;
        foreach($this->relations as $rel_info)
        {
            if($rel_info['type'] == 'M-1')
            {
                $minimum= $rel_info['minimum'];
            }
        }

        require_once FULLPATH_BASE . 'javascript/toggle_fieldset.php';
        $anchor_tag = str_replace(' ', '_', $label);
        echo '<fieldset class="fieldset_group">' . "\r\n";
        if(empty($label))
        {
            //no title, no legend
        }
        else
        {
            echo '<legend onClick="toggle_fieldset(\'' . $anchor_tag .'\')">' . $label . '</legend>';
        }

        //if($$num_particulars_var>0) ;
        //else $$num_particulars_var=$$particulars_count_var;
        if(is_numeric($$num_particulars_var))
        {
            $$particulars_count_var = $$num_particulars_var;
        }
        else
        {
            $$num_particulars_var = $$particulars_count_var;
        }

        if($$num_particulars_var<$minimum)
        {
            $$num_particulars_var   = $minimum;
            $$particulars_count_var = $minimum;
        }

        if($this->detail_view==FALSE)
        {
            //if($$num_particulars_var!=0) echo "<input type=hidden name='" . $particulars_count_var . "' value=". $$num_particulars_var . ">\r\n";
            //else  echo "<input type=hidden name='" . $particulars_count_var . "' value=1>\r\n";
            echo "<input type=\"hidden\" id=\"" . $particulars_count_var . "\" name=\"" . $particulars_count_var . "\" value=\"". $$num_particulars_var . "\">\r\n";
        }
        echo '<div id="' . $anchor_tag  . '">';
        echo '<table class="input_form" id = "' .$particulars_count_var . '_tbl" ><tr><td>&nbsp;</td>' . "\r\n";

        //Count how many fields need to be drawn,
        //then loop the <td></td> tags with the corresponding labels.
        $numTDPairs = count($arr_multifield['field_labels']);
        for($a=0;$a<$numTDPairs;++$a)
        {
            echo '<td><p class="multifield_detail_view_label">' . $arr_multifield['field_labels'][$a] . '</p></td>' . "\r\n";
        }
        echo '</tr>' . "\r\n";

        for($a=0;$a<$$num_particulars_var;++$a)
        {
            echo '<tr><td class="label">&nbsp;' . ($a + 1) . '&nbsp; <input type = "button" id= "btn_delete_row" name = "btn_delete_row" onclick="deleteRow(this,\''.$particulars_count_var.'_tbl\',\''.$particulars_count_var.'\')" value = "X"></td>' . "\r\n";

            for($b=0;$b<$numTDPairs;++$b)
            {
                init_var($this->mf_col_align[$b]);
                if($this->mf_col_align[$b] == '') $this->mf_col_align[$b]=='left';
                echo '<td align="' . $this->mf_col_align[$b] . '">';

                if($this->detail_view)
                {
                    echo '<p class="multifield_detail_view">';
                }
                else
                {
                    echo '<p>';
                }

                $this->{$arr_multifield['field_controls'][$b]}($arr_multifield['field_parameters'][$b], $a);
                echo '</p></td>' . "\r\n";

            }

            echo '</tr>' . "\r\n";
        }

        if($$num_particulars_var < 1)
        {
            $colspan = $numTDPairs + 1;
            if(isset($this->mf_label))
            {
                $label = $this->mf_label;
            }
            else
            {
                $label = $this->readable_name;
            }

            echo '<tr><td colspan="' . $colspan . '"><p class="multifield_detail_view">';

            if($this->detail_view)
            {
                echo '[No Data]';
            }
            else
            {
                echo '[Items for ' . $label . ' is set to zero. No data will be submitted for this section]';
            }
            echo '</p></td></tr>';
        }
        echo '</div>';
        echo "</table>\r\n";

        if($this->detail_view==FALSE)
        {

            // echo '<br> Change # of items to:';
            ++$this->tabindex;
            echo '<input type="hidden" size="2" maxlength="2" id="' . $num_particulars_var . '" name="' . $num_particulars_var . '" tabindex="' . $this->tabindex . '" value = " ' . $$particulars_count_var . ' ">';
            ++$this->tabindex;
            echo '<input type="submit" id="' . $particular_button_var . '" name="' . $particular_button_var . '" tabindex="' . $this->tabindex . '" value="+">' . "\r\n";
        }

        echo '</fieldset>' . "\r\n";
        echo '<br>' . "\r\n";

        return $this;
    }
}
