<?php
require_once 'path.php';
init_cobalt('');

//set SOAP Access

$dbh_system_settings = cobalt_load_class('system_settings');
$ws_location = $dbh_system_settings->get('WS Location', FALSE)->dump['value'];
$wsdl = $dbh_system_settings->get('WSDL', FALSE)->dump['value'];



$params = ['soap_version'=>SOAP_1_1,
           'trace'=>1,
           'exceptions'=>1,
           'location'=>$ws_location,
           'uri'=>'https://scholarship.sm-foundation.org/PRF/JobPosting.asmx?wsdl'];
           
           

           

$client = new SoapClient($wsdl,$params);
//end SOAP Access

require_once 'components/format_date.php';

$iprf_staffrequest_applicant_id = $_GET['iprf_staffrequest_applicant_id'];
$iprf_staffrequest_id           = $_GET['iprf_get_id'];
$applicant_id                   = $_GET['applicant_id'];
$owner                          = 'SMIC Recruitment Module'; //this variable is passed to EncodedBy element of
                                                             //applicant's child table

//fetch applicant details
$arr_applicant                       = cobalt_load_component('select_record')
                                       ->inherit($applicant_id,'applicant')
                                       ->go();
//fetch education
$arr_applicant_school_attended       = cobalt_load_component('select_record')
                                       ->inherit($applicant_id,'applicant_school_attended')
                                       ->go();
//fetch skills
$arr_applicant_skills                = cobalt_load_component('select_record')
                                       ->inherit($applicant_id,'applicant_skills')
                                       ->go();
//fetch licenses
$arr_applicant_license               = cobalt_load_component('select_record')
                                       ->inherit($applicant_id,'applicant_license')
                                       ->go();
//fetch family members
$arr_applicant_family_members        = cobalt_load_component('select_record')
                                       ->inherit($applicant_id,'applicant_family_members')
                                       ->go();
//fetch references
$arr_applicant_reference             = cobalt_load_component('select_record')
                                       ->inherit($applicant_id,'applicant_reference')
                                       ->go();
//fetch languages
$arr_applicant_languages_proficiency = cobalt_load_component('select_record')
                                       ->inherit($applicant_id,'applicant_languages_proficiency')
                                       ->go();

//fetch trainings
$arr_applicant_trainings             = cobalt_load_component('select_record')
                                       ->inherit($applicant_id,'applicant_trainings')
                                       ->go();

//fetch other_declarations
$arr_applicant_other_declarations   = cobalt_load_component('select_record')
                                      ->inherit($applicant_id,'applicant_other_declarations')
                                      ->go();

// fetch date applied
$d = cobalt_load_class('iprf_staffrequest_applicants');
$d->check_application_in_staffrequest($applicant_id, $iprf_staffrequest_id);
$date_applied = $d->dump['date_applied'];

// Map applicant data to match fields in iHRIS
$arr_applicant['applicant_id'] = NULL;
$arr_applicant_processed['a'] = $arr_applicant;
$arr_applicant_processed['a']['personnel_requisition_id'] = 5;
$arr_applicant_processed['a']['date_Applied'] = $date_applied;

//send applicant data, this method will return new applicant_id generated from ihris personnel request system
$params_addrec['applicant_id']= [$applicant_id];



$ihris_generated_applicant_id = "";
try {
                
   $ihris_generated_applicant_id = $client->AddRecord_Applicant($params_addrec)->AddRecord_ApplicantResult;
      print_r($ihris_generated_applicant_id);
      print_r($params_addrec);
} catch (SoapFault $e) {
                print_r($e);
    #print($client->__getLastResponse());
}



//send school attended
if(count($arr_applicant_school_attended) > 0)
{
    for($a = 0; $a < count($arr_applicant_school_attended); ++$a)
    {
        extract($arr_applicant_school_attended[$a]);
        $year_from = explode('-',$date_from);
        $year_to = explode('-',$date_to);
        $arr_applicant_school_attended_processed['a'] = [[
                                                        'ApplicantEducationID' => $applicant_school_attended_id,
                                                        'ApplicantID'          => $ihris_generated_applicant_id,
                                                        'School'               => $school_name,
                                                        'SchoolAddress'        => $address,
                                                        'EducationalLevelID'   => $educational_attainment,
                                                        'CourseID'             => $course,
                                                        'YearFrom'             => intval($year_from[0]),
                                                        'YearTo'               => intval($year_to[0]),
                                                        'Awards'               => $awards,
                                                        'EncodeBy'             => $owner,
                                                        'EncodeDate'           => date('Y-m-d'),
                                                        'Course'               => $course,
                                                        'OtherSys_AppId'       => $applicant_id
                                                        ]];
        $client->AddRecord_ApplicantEducation($arr_applicant_school_attended_processed);
    }
}

//send school skills
if(count($arr_applicant_skills) > 0)
{
    for($a = 0; $a < count($arr_applicant_skills); ++$a)
    {
        extract($arr_applicant_skills[$a]);
        // debug($arr_applicant_skills);
        $arr_applicant_skills_processed['a'] = [[
                                                'ApplicantSkillsID' => $applicant_skill_id,
                                                'ApplicantID'       => $ihris_generated_applicant_id,
                                                'Skills'            => $skill,
                                                'YearsOfExperience' => $years_of_experience,
                                                'ProficiencyID'     => $proficiency,
                                                'EncodeBy'          => $owner,
                                                'EncodeDate'        => date('Y-m-d'),
                                                'OtherSys_AppId'       => $applicant_id
                                                ]];
        $client->AddRecord_ApplicantSkills($arr_applicant_skills_processed);
    }
}

//send license
if(count($arr_applicant_license) > 0)
{
    for($a = 0; $a < count($arr_applicant_license); ++$a)
    {
        extract($arr_applicant_license[$a]);
        $arr_applicant_license_processed['a'] = [[
                                                'ApplicantLicenseID' => $applicant_license_id,
                                                'ApplicantID'        => $ihris_generated_applicant_id,
                                                'Type'               => $license,
                                                'LicenseNo'          => $license_number,
                                                'DateExpiry'         => $license_expiry,
                                                'EncodeBy'           => $owner,
                                                'EncodeDate'         => date('Y-m-d'),
                                                'OtherSys_AppId'       => $applicant_id
                                                ]];
        $client->AddRecord_ApplicantLicences($arr_applicant_license_processed);
    }
}

//send family_members

if(count($arr_applicant_family_members) > 0)
{
    for($a = 0; $a < count($arr_applicant_family_members); ++$a)
    {
        extract($arr_applicant_family_members[$a]);
        $arr_applicant_family_members_processed['a'] = [[
                                                        'ApplicantFamilyID' => $applicant_family_member_id,
                                                        'ApplicantID'       => $ihris_generated_applicant_id,
                                                        'LastName'          => '',
                                                        'FirstName'         => '',
                                                        'MiddleName'        => '',
                                                        'Fullname'          => $name,
                                                        'DateBirth'         => $birthday,
                                                        'RelationshipID'    => $relationship,
                                                        'CivilStatusID'     => 0,
                                                        'Contact'           => '',
                                                        'Position'          => '',
                                                        'Company'           => '',
                                                        'CompanyAddress'    => '',
                                                        'EncodeBy'          => $owner,
                                                        'EncodeDate'        => date('Y-m-d'),
                                                        'OtherSys_AppId'       => $applicant_id
                                                        ]];
        $client->AddRecord_ApplicantFamilyMembers($arr_applicant_family_members_processed);
    }
}

//send reference
// debug($arr_applicant_reference);
if(count($arr_applicant_reference) > 0)
{
    for($a = 0; $a < count($arr_applicant_reference); ++$a)
    {
        extract($arr_applicant_reference[$a]);
        $arr_applicant_reference_processed['a'] = [[
                                                    'ApplicantReferenceID' => $applicant_reference_id,
                                                    'ApplicantID'          => $ihris_generated_applicant_id,
                                                    'LastName'             => '',
                                                    'FirstName'            => '',
                                                    'MiddleName'           => '',
                                                    'Fullname'             => $reference_name,
                                                    'Address'              => $reference_address,
                                                    'Contact'              => $reference_contact_number,
                                                    'Position'             => $reference_occupation,
                                                    'YearKnown'            => 0,
                                                    'EncodeBy'             => $owner,
                                                    'EncodeDate'           => date('Y-m-d'),
                                                    'OtherSys_AppId'       => $applicant_id
                                                    ]];
        $client->AddRecord_ApplicantReference($arr_applicant_reference_processed);
    }
}

//send languages_proficiency
if(count($arr_applicant_languages_proficiency) > 0)
{
    for($a = 0; $a < count($arr_applicant_languages_proficiency); ++$a)
    {
        extract($arr_applicant_languages_proficiency[$a]);
        $arr_applicant_languages_proficiency_processed['a'] = [[
                                                    'ApplicantLanguageID'  => $applicant_language_proficiency_id,
                                                    'ApplicantID'          => $ihris_generated_applicant_id,
                                                    'LanguageID'           => $language,
                                                    'SpokenProficiencyID'  => $speaking_proficiency,
                                                    'WrittenProficiencyID' => $writing_proficiency,
                                                    'EncodeBy'             => $owner,
                                                    'EncodeDate'           => date('Y-m-d'),
                                                    'OtherSys_AppId'       => $applicant_id
                                                    ]];
        $client->AddRecord_ApplicantLanguage($arr_applicant_languages_proficiency_processed);
    }
}

//send trainings
if(count($arr_applicant_trainings) > 0)
{
    for($a = 0; $a < count($arr_applicant_trainings); ++$a)
    {
        extract($arr_applicant_trainings[$a]);
        $arr_applicant_trainings_processed['a'] = [[
                                                    'ApplicantTrainingID'  => $applicant_training_id,
                                                    'ApplicantID'          => $ihris_generated_applicant_id,
                                                    'TrainingTitle'        => $training_title,
                                                    'TrainingProvider'     => $training_provider,
                                                    'TrainingDate'         => $training_date,
                                                    'Venue'                => $venue,
                                                    'NoOfHours'            => $no_of_hours,
                                                    'Remarks'              => $remarks,
                                                    'EncodeBy'             => $owner,
                                                    'EncodeDate'           => date('Y-m-d'),
                                                    'OtherSys_AppId'       => $applicant_id
                                                    ]];
        $client->AddRecord_ApplicantTraining($arr_applicant_trainings_processed);
    }
}

//send other other_declarations
if(count($arr_applicant_other_declarations) > 0)
{
    //fetch first record of other declaration only
    if($arr_applicant_other_declarations['crime_convict'] == 'Yes')
    {
        $crime_convict = 1;
    }
    else
    {
        $crime_convict = 0;
    }

    $arr_applicant_other_declarations_processed['a'] = [[
                                                        'ApplicantID' => $ihris_generated_applicant_id,
                                                        'Convicted'   => $crime_convict,
                                                        'DescriptionConvition' => $arr_applicant_other_declarations['details'],
                                                        'OtherSys_AppId'       => $applicant_id
                                                       ]];
    // $client->AddRecord_ApplicantConviction($arr_applicant_school_attended_processed);

    $applicant_other_declaration_id = $arr_applicant_other_declarations['applicant_other_declaration_id'];
    //fetch other_declarations
    $arr_applicant_sm_group_employ = cobalt_load_component('select_record')
                                          ->inherit($applicant_other_declaration_id,'other_declarations_name_smgroup')
                                          ->go();

    // debug($arr_applicant_sm_group_employ);

    if(count($arr_applicant_sm_group_employ) > 0)
    {
        for($a = 0; $a < count($arr_applicant_sm_group_employ); ++$a)
        {
            extract($arr_applicant_sm_group_employ[$a]);

            $arr_applicant_sm_group_employ_processed['a'] = [[
                                                              'ApplicantID' => $ihris_generated_applicant_id,
                                                              'EmploymentRelationshipType' => 2, //employed in any Sm group of companies
                                                              'FullName' => $name,
                                                              'Relationship' => $relationship,
                                                              'Position' => $position,
                                                              'CompanyDepartment' => $company,
                                                              'CreatedBy' => $owner,
                                                              'DateCreated' => date('Y-m-d'),
                                                              'OtherSys_AppId' => $applicant_other_declaration_id
                                                            ]];
            // $client->AddRecord_ApplicantEmployeeRelations($arr_applicant_sm_group_employ_processed);
            // debug($arr_applicant_sm_group_employ_processed['a']);
        }
    }
  //fetch other_declarations
    $arr_applicant_smic_employ   = cobalt_load_component('select_record')
                                        ->inherit($applicant_other_declaration_id,'other_declarations_name_smic_employ')
                                        ->go();

    if(count($arr_applicant_smic_employ) > 0)
    {
        for($a = 0; $a < count($arr_applicant_smic_employ); ++$a)
        {
            extract($arr_applicant_smic_employ[$a]);

            $arr_applicant_smic_employ_processed['a'] = [[
                                                              'ApplicantID' => $ihris_generated_applicant_id,
                                                              'EmploymentRelationshipType' => 1, //employed in any Sm group of companies
                                                              'FullName' => $name,
                                                              'Relationship' => $relationship,
                                                              'Position' => $position,
                                                              'CompanyDepartment' => $company,
                                                              'CreatedBy' => $owner,
                                                              'DateCreated' => date('Y-m-d'),
                                                              'OtherSys_AppId' => $applicant_other_declaration_id
                                                            ]];
            // $client->AddRecord_ApplicantEmployeeRelations($arr_applicant_smic_employ_processed);
            // debug($arr_applicant_smic_employ_processed['a']);
        }
    }
}
//update applicant status

$formatted_time = format_time(date('H:i:s'));
$formatted_date = format_date(date('Y-m-d'));

$dbh = cobalt_load_class('iprf_staffrequest_applicants');
$param = [
          'iprf_staffrequest_applicant_id' => $iprf_staffrequest_applicant_id,
          'updates'                        => "Exported to iHRIS - $formatted_date - $formatted_time"
        ];
        // debug($param);
$dbh->edit_updates($param);

redirect('../modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php?iprf_staffrequest_id='.$iprf_staffrequest_id.'&exported=ok');
